cmake_minimum_required(VERSION 3.20)
# CMake version compatibility
if (POLICY CMP0127)
	# (CMake < 3.22) cmake_dependent_option
	cmake_policy(SET CMP0127 NEW)
endif ()

#[==============================================================================================[
#                                    Basic project defintion                                    #
]==============================================================================================]

list(APPEND CMAKE_MESSAGE_CONTEXT Octopus)
project(Octopus VERSION 15.1
		HOMEPAGE_URL https://www.octopus-code.org
		DESCRIPTION "real-space, real-time, TDDFT code"
		LANGUAGES C Fortran CXX)

# Define language standards
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS ON)
set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED ON)
set(CMAKE_C_EXTENSIONS ON) # enable GNU extensions
# Upstream issue: Fortran standards are not defined in cmake yet

#[==============================================================================================[
#                                            Options                                            #
]==============================================================================================]

include(CMakeDependentOption)
include(FeatureSummary)

option(OCTOPUS_MPI "Octopus: Build with MPI support" OFF)
add_feature_info(OCTOPUS_MPI OCTOPUS_MPI "MPI parallelization support")
option(OCTOPUS_OpenMP "Octopus: Build with OpenMP support" OFF)
add_feature_info(OCTOPUS_OpenMP OCTOPUS_OpenMP "OpenMP parallelization support")
option(OCTOPUS_INSTALL "Octopus: Install project" ${PROJECT_IS_TOP_LEVEL})
option(OCTOPUS_UNIT_TESTS "Octopus: Build with unit-tests" OFF)
option(OCTOPUS_APP_TESTS "Octopus: Build with regression test-suite" ON)
option(OCTOPUS_TESTS_REPORT "Octopus: Export test report" OFF)
# First ON/OFF is the default if the condition is true. Second ON/OFF is used if the condition is false
cmake_dependent_option(OCTOPUS_TESTS_RUN_SERIAL "Octopus: Run all tests as serial" OFF "OCTOPUS_MPI" ON)
mark_as_advanced(OCTOPUS_TESTS_RUN_SERIAL)
option(OCTOPUS_MKL "Octopus: Build with MKL support" OFF)
add_feature_info(OCTOPUS_MKL OCTOPUS_MKL "Intel MKL backend (BLAS/LAPACK/FFTW)")
# Disable FFTW if MKL is enabled, otherwise fallback to FFTW
cmake_dependent_option(OCTOPUS_FFTW "Octopus: Build with FFTW support" ON "NOT OCTOPUS_MKL" OFF)
add_feature_info(OCTOPUS_FFTW OCTOPUS_FFTW "FFTW backend")
# ScaLAPACK is only enabled with MPI support. No fallback logic for this because it can come from MKL
cmake_dependent_option(OCTOPUS_ScaLAPACK "Octopus: Build with ScaLAPACK support" OFF "OCTOPUS_MPI" OFF)
add_feature_info(OCTOPUS_ScaLAPACK OCTOPUS_ScaLAPACK "ScaLAPACK support")
option(OCTOPUS_NATIVE "Octopus: Build for native architecture" OFF)
add_feature_info(OCTOPUS_NATIVE OCTOPUS_NATIVE "Native architecture support")
option(OCTOPUS_CUDA "Octopus: Build with CUDA support" OFF)
add_feature_info(OCTOPUS_CUDA OCTOPUS_CUDA "CUDA graphics driver support")
option(OCTOPUS_HIP "Octopus: Build with HIP support" OFF)
add_feature_info(OCTOPUS_HIP OCTOPUS_HIP "HIP graphics driver support")
option(OCTOPUS_OpenCL "Octopus: Build with OpenCL support" OFF)
add_feature_info(OCTOPUS_OpenCL OCTOPUS_OpenCL "OpenCL graphics driver support")
option(OCTOPUS_DOXYGEN "Octopus: Build with Doxygen support for in-source documentation" OFF)
add_feature_info(OCTOPUS_DOXYGEN OCTOPUS_DOXYGEN "Doxygen source documentation support")
option(OCTOPUS_SHARED_LIBS "Octopus: Build shared libraries" OFF)
add_feature_info(OCTOPUS_SHARED_LIBS OCTOPUS_SHARED_LIBS "Shared library support")
option(OCTOPUS_VERROU "Octopus: Build with Verrou instrumentation" OFF)
add_feature_info(OCTOPUS_VERROU OCTOPUS_VERROU "Floating-point error checker support")
option(OCTOPUS_LIKWID "Octopus: Build with LIKWID instrumentation" OFF)
add_feature_info(OCTOPUS_LIKWID OCTOPUS_LIKWID "Performance analysis support")
option(OCTOPUS_DEBUG "Octopus: Build with debug options" ON)
add_feature_info(OCTOPUS_DEBUG OCTOPUS_DEBUG "Internal debuging and profiling support")

if (NOT OCTOPUS_MKL AND NOT OCTOPUS_FFTW)
	message(FATAL_ERROR "No FFTW vendor enabled. Either MKL or FFTW must be enabled.")
endif ()

#[==============================================================================================[
#                                     Project configuration                                     #
]==============================================================================================]

list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
include(FetchContent)

## Include other cmake modules
include(Octopus)
include(GNUInstallDirs)

## Configure project variables
# Default to Release build if not specified
if (NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE Release)
endif ()
# Whether to build shared or not has to be decided before any targets are created
set(BUILD_SHARED_LIBS ${OCTOPUS_SHARED_LIBS})
set(SPGLIB_SHARED_LIBS ${OCTOPUS_SHARED_LIBS})
# Add custom/third-party cmake modules
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake)
# Build all fortran modules in a common module directory for simpler loading
if (NOT CMAKE_Fortran_MODULE_DIRECTORY)
	set(CMAKE_Fortran_MODULE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/fortran_mods)
endif ()
include_directories(${CMAKE_Fortran_MODULE_DIRECTORY})

# Setup MKL variables
if (OCTOPUS_MKL)
	# Set BLAS/LAPACK to MKL as well
	# Note the user has to set MKL_THREADING_LAYER and MKL_INTERFACE_LAYER accordingly
	set(BLA_VENDOR Intel10_64_dyn CACHE STRING "Octopus: Overload")
	# MKL_THREADING and MKL_MPI determine the libraries returned in MKL::mkl
	# Prevent MKL defaulting to intel_thread for non-threaded builds
	if (NOT OCTOPUS_OpenMP)
		set(MKL_THREADING "sequential" CACHE STRING "Octopus: Overload")
	endif ()
	# Prevent MKL defaulting to intelmpi for non-MPI builds
	if (NOT OCTOPUS_MPI)
		set(MKL_MPI "" CACHE STRING "Octopus: Overload")
	endif ()
	set(MKL_INTERFACE lp64 CACHE STRING "Octopus: Overload")
	set(MKL_LINK dynamic CACHE STRING "Octopus: Overload")
	set(MKL_ARCH intel64 CACHE STRING "Octopus: Overload")
	set(ENABLE_SCALAPACK ${OCTOPUS_ScaLAPACK} CACHE BOOL "Octopus: Overloaded")
	set(MKL_DPCPP_INTERFACE_FULL intel_ilp64 CACHE STRING "Octopus: Overload")
	if (CMAKE_Fortran_COMPILER_ID STREQUAL GNU)
		set(MKL_THREADING gnu_thread CACHE STRING "Octopus: Overloaded")
	endif ()
endif ()

# Compatibilities

#[==============================================================================================[
#                                       External packages                                       #
]==============================================================================================]

set(Octopus_ext_libs)

## Add external packages
# Required libraries
find_package(GSL)
if (OCTOPUS_MPI)
	set(MPI_DETERMINE_LIBRARY_VERSION ON)
	find_package(MPI 3 COMPONENTS Fortran)
	# Further set MKL variables
	if (MPI_Fortran_LIBRARY_VERSION_STRING MATCHES Intel)
		set(MKL_MPI intelmpi CACHE STRING "Octopus: Overloaded")
	elseif (MPI_Fortran_LIBRARY_VERSION_STRING MATCHES "Open MPI")
		set(MKL_MPI openmpi CACHE STRING "Octopus: Overloaded")
	elseif (MPI_Fortran_LIBRARY_VERSION_STRING MATCHES "MPICH")
		set(MKL_MPI mpich CACHE STRING "Octopus: Overloaded")
	else ()
		message(WARNING "Could not detect mpi type from string:\n${MPI_Fortran_LIBRARY_VERSION_STRING}")
	endif ()
	if (NOT MPI_Fortran_HAVE_F08_MODULE)
		message(FATAL_ERROR "MPI must support the mpi_f08 module")
	endif ()
	set_package_properties(MPI PROPERTIES TYPE REQUIRED)
endif ()
if (OCTOPUS_OpenMP)
	find_package(OpenMP COMPONENTS Fortran)
	set_package_properties(OpenMP PROPERTIES TYPE REQUIRED)
endif ()
# find_package(MKL) must not be run if FFTW is requested
if (OCTOPUS_MKL)
	find_package(MKL CONFIG)
	set_package_properties(MKL PROPERTIES TYPE REQUIRED)
elseif (OCTOPUS_FFTW)
	find_package(FFTW)
	set_package_properties(FFTW PROPERTIES TYPE REQUIRED)
endif ()
# Avoid duplicating targets
if (NOT MKL_FOUND)
	find_package(BLAS REQUIRED)
	find_package(LAPACK REQUIRED)
	set_package_properties(BLAS PROPERTIES TYPE REQUIRED)
	set_package_properties(LAPACK PROPERTIES TYPE REQUIRED)
endif ()

if (OCTOPUS_CUDA)
	find_package(CUDAToolkit)
	set(HAVE_CUDA 1)
	set(HAVE_NVTX 1)
	set_package_properties(CUDAToolkit PROPERTIES TYPE REQUIRED)
	if (OCTOPUS_MPI)
		include(cmake/GPUawareMPI.cmake)
	endif ()
        if (CUDAToolkit_VERSION VERSION_GREATER_EQUAL 11.1)
		# CUBIN generation available from CUDA 11.1 onwards
		set(HAVE_CUBIN 1)
	endif ()
endif ()
if (OCTOPUS_HIP)
	find_package(HIP MODULE)
	find_package(hipblas CONFIG)
	find_package(hipfft CONFIG)
	set(HAVE_HIP 1)
	set(HAVE_CUDA 1)
	set_package_properties(HIP PROPERTIES TYPE REQUIRED)
	set_package_properties(hipblas PROPERTIES TYPE REQUIRED)
	set_package_properties(hipfft PROPERTIES TYPE REQUIRED)
	if(HIP_PLATFORM STREQUAL nvidia)
		target_compile_definitions(hip::host INTERFACE __HIP_PLATFORM_NVIDIA__=1)
		find_package(CUDAToolkit REQUIRED)
		target_link_libraries(hip::host INTERFACE CUDA::cudart CUDA::cuda_driver)
		target_link_libraries(roc::hipblas INTERFACE CUDA::cublas)
		target_link_libraries(hip::hipfft INTERFACE CUDA::cufft)
	endif ()
	# NVIDIA backend does not provide hiprtc as a package
	if(HIP_PLATFORM STREQUAL nvidia)
		add_library(hiprtc::hiprtc INTERFACE IMPORTED)
		target_link_libraries(hiprtc::hiprtc INTERFACE CUDA::nvrtc)
	else ()
		find_package(hiprtc CONFIG)
		set_package_properties(hiprtc PROPERTIES TYPE REQUIRED)
	endif ()
	# AMD backend is currently lacking CMake integration for roctx
	if(HIP_PLATFORM STREQUAL amd)
		find_path(ROCTX_INCLUDE_DIR
			NAMES roctracer/roctx.h
			PATHS $ENV{HIP_PATH}
			PATH_SUFFIXES include
			REQUIRED)
		find_library(ROCTX_LIBRARY
			NAMES roctx64
			PATHS $ENV{HIP_PATH} $ENV{HIP_PATH}/lib
			REQUIRED)
		add_library(roc::roctx INTERFACE IMPORTED)
		target_include_directories(roc::roctx INTERFACE ${ROCTX_INCLUDE_DIR})
		target_link_libraries(roc::roctx INTERFACE ${ROCTX_LIBRARY})
	endif ()
	if (OCTOPUS_MPI)
		include(cmake/GPUawareMPI.cmake)
	endif ()
endif ()
if (OCTOPUS_OpenCL)
	find_package(OpenCL)
	find_package(CLBlast CONFIG)
	find_package(clBLAS CONFIG)
	find_package(clFFT CONFIG)
	if(TARGET clFFT)
		set_target_properties(clFFT PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${CLFFT_INCLUDE_DIRS}")
	endif()
	set(HAVE_OPENCL 1)
	if(TARGET clblast)
		set(HAVE_CLBLAST 1)
		set_package_properties(CLBlast PROPERTIES TYPE REQUIRED)
	elseif(TARGET clBLAS)
		set(HAVE_CLBLAS 1)
		set_target_properties(clBLAS PROPERTIES
			INTERFACE_INCLUDE_DIRECTORIES "${CLBLAS_INCLUDE_DIRS}")
		set_package_properties(clBLAS PROPERTIES TYPE REQUIRED)
	else()
		message(FATAL_ERROR "Either clBLAS or CLBlast is required when building Octopus with OpenCL")
	endif()
	set(HAVE_CLFFT 1)
	set_package_properties(OpenCL PROPERTIES TYPE REQUIRED)
	set_package_properties(clFFT PROPERTIES TYPE REQUIRED)
endif ()

# Find netlib ScaLAPACK. Other vendors should also work if it uses the reference cmake project
if (OCTOPUS_ScaLAPACK AND NOT OCTOPUS_MKL)
	find_package(SCALAPACK MODULE)
	set_package_properties(SCALAPACK PROPERTIES TYPE REQUIRED)
endif ()
if (OCTOPUS_ScaLAPACK)
	# If we reach here one of the ScaLAPACK vendors has succeeded
	set(HAVE_SCALAPACK 1)
endif ()

Octopus_FetchContent_Declare(Libxc
		GIT_REPOSITORY https://gitlab.com/LecrisUT/libxc
		GIT_TAG cmake/external-project-6.x
		FIND_PACKAGE_ARGS MODULE COMPONENTS Fortran
		)
Octopus_FetchContent_Declare(Spglib
		GIT_REPOSITORY https://github.com/spglib/spglib
		GIT_TAG v2.1.0
		FIND_PACKAGE_ARGS 2.1.0 EXACT MODULE COMPONENTS Fortran
		)

# Optional dependencies
find_package(netCDF-Fortran MODULE)
find_package(DftbPlus MODULE)
# No proper fallback is designed for CGAL package
find_package(CGAL CONFIG)
if (CGAL_FOUND)
	set(HAVE_CGAL 1)
endif ()
find_package(NLopt MODULE)
find_package(GD MODULE)
find_package(libvdwxc MODULE)
find_package(nfft MODULE)
find_package(PSolver MODULE)
find_package(BerkleyGW MODULE)

if (OCTOPUS_MPI)
	if (OCTOPUS_OpenMP)
		find_package(ELPA MODULE COMPONENTS OpenMP)
	else ()
		find_package(ELPA MODULE)
	endif ()

	find_package(pfft MODULE)
	find_package(pnfft MODULE)

	find_package(METIS MODULE)
	if (METIS_FOUND)
		find_package(ParMETIS MODULE)
	endif ()
endif ()

find_package(SPARSKIT MODULE)
find_package(etsf-io MODULE)

if (OCTOPUS_VERROU)
	find_package(Verrou MODULE REQUIRED)
endif ()
if (OCTOPUS_LIKWID)
	find_package(LIKWID MODULE REQUIRED)
endif ()

# Add all remaining packages
Octopus_FetchContent_MakeAvailable(${Octopus_ext_libs})

# These Libxc commands must come after FetchContent_MakeAvailable, because when using
# DFETCHCONTENT_SOURCE_DIR_LIBXC, FetchContent_MakeAvailable ignores the Before hook
# AND will overwrite any prior definitions of those variables.

# If Libxc is sourced from git submodule, include the Before hook to propagate variables
if (DEFINED FETCHCONTENT_SOURCE_DIR_LIBXC)
	include(${PROJECT_SOURCE_DIR}/cmake/compat/FetchContentLibxc_Before.cmake)
endif ()

# Required when building libxc with Octopus cmake
# See FetchContentLibxc_Before.cmake for definitions
if (DEFINED LIBXC_DISABLE_FXC AND NOT LIBXC_DISABLE_FXC)
	message("Libxc configured to build second-order derivatives, FXC")
    set(HAVE_LIBXC_FXC 1)
endif ()
if (DEFINED LIBXC_DISABLE_KXC AND NOT LIBXC_DISABLE_KXC)
	message("Libxc configured to build third-order derivatives, KXC")
    set(HAVE_LIBXC_KXC 1)
endif ()

message("The following packages have been found with FetchContent:")
foreach(pkg IN LISTS Octopus_ext_libs)
    message("  - ${pkg}")
endforeach()
message(" ")

include(Octopus_PackagesInfo)
# Calling feature_summary twice as workaround for not displaying FATAL_ON_MISSING_REQUIRED_PACKAGES
feature_summary(WHAT ALL
		FILENAME ${CMAKE_CURRENT_BINARY_DIR}/Octopus.info
		DESCRIPTION "Octopus supported libraries"
)
feature_summary(WHAT ALL
		DESCRIPTION "Octopus supported libraries"
		FATAL_ON_MISSING_REQUIRED_PACKAGES
)


#[==============================================================================================[
#                                          Boilerplate                                          #
]==============================================================================================]

if (NOT OCTOPUS_DEBUG)
    add_definitions(-DNDEBUG)
endif ()

find_package(Perl REQUIRED)
include(PerlFindModule)
perl_find_module(NAME "Fcntl qw(:mode :flock)" REQUIRED)
perl_find_module(NAME "File::Basename" REQUIRED)
perl_find_module(NAME "File::Find" REQUIRED)
perl_find_module(NAME "File::Path qw(make_path)" REQUIRED)
perl_find_module(NAME "File::Spec" REQUIRED)
perl_find_module(NAME "File::Temp qw/tempdir/" REQUIRED)
perl_find_module(NAME "Getopt::Std" REQUIRED)
perl_find_module(NAME "Math::Trig" REQUIRED)
perl_find_module(NAME "POSIX qw(ceil)" REQUIRED)
perl_find_module(NAME "Scalar::Util qw(looks_like_number)" REQUIRED)
perl_find_module(NAME "Time::HiRes qw(gettimeofday tv_interval)" REQUIRED)
if(OCTOPUS_TESTS_REPORT)
	perl_find_module(NAME "YAML" REQUIRED)
endif()

# On gfortran, run the `cpp` pre-processor instead so that it doesn't run in traditional mode
# https://stackoverflow.com/a/31753386/22352077
if (CMAKE_Fortran_COMPILER_ID STREQUAL GNU)
	if (NOT CMAKE_GENERATOR MATCHES Ninja)
		message(FATAL_ERROR "GNU preprocessor and non-Ninja generator is not supported"
		)
	endif ()
	find_program (CPP_EXECUTABLE cpp)
	set(CMAKE_Fortran_PREPROCESS_SOURCE
			"${CPP_EXECUTABLE} -ffreestanding -C <DEFINES> <INCLUDES> -E <SOURCE> -o <PREPROCESSED_SOURCE>")
endif ()

## Boilerplate because of custom octopus things
# TODO: Move away from header re-generation of mk_varinfo.pl. Use static lists and dictionaries instead
file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/tmp_generated)
add_custom_target(VarInfo
		COMMAND ${PERL_EXECUTABLE} ${PROJECT_SOURCE_DIR}/scripts/mk_varinfo.pl
		-s ${PROJECT_SOURCE_DIR}
		-b ${PROJECT_BINARY_DIR}/tmp_generated
		COMMENT "Generating variable information")
# Define target ChecksumCopy that copies from ${PROJECT_BINARY_DIR}/tmp_generated to ${PROJECT_BINARY_DIR} only if
# contents changed. This guarantees VarInfo is always called, but dependent targets do not rebuild unnecessarily if
# src/include/options.h and src/include/defaults.h is not *really* changed.
if (CMAKE_VERSION VERSION_LESS 3.26)
	add_custom_target(ChecksumCopy
			COMMAND ${CMAKE_COMMAND}
			-Dchecksum_src=${PROJECT_BINARY_DIR}/tmp_generated
			-Dchecksum_dest=${PROJECT_BINARY_DIR}
			-P ${PROJECT_SOURCE_DIR}/cmake/ChecksumCopy.cmake
			COMMENT "Checking and copying files")
else ()
	add_custom_target(ChecksumCopy
			COMMAND ${CMAKE_COMMAND} -E copy_directory_if_different ${PROJECT_BINARY_DIR}/tmp_generated ${PROJECT_BINARY_DIR}
			COMMENT "Checking and copying files")
endif ()
add_dependencies(ChecksumCopy VarInfo)
add_library(Octopus_base INTERFACE)
add_dependencies(Octopus_base ChecksumCopy)

#[==============================================================================================[
#                                        Main definition                                        #
]==============================================================================================]

# Mimic autotools
include(mock_autotools)

## Main targets
add_library(Octopus_lib)
target_link_libraries(Octopus_lib PRIVATE Octopus_base)
add_executable(Octopus_octopus)

# Setup octopus library
set_target_properties(Octopus_lib PROPERTIES
		VERSION ${PROJECT_VERSION}
		SOVERSION ${PROJECT_VERSION_MAJOR}
		EXPORT_NAME octopus
		OUTPUT_NAME octopus
		)
# Add alias target so that project is compatible with FetchContent
add_library(Octopus::octopus ALIAS Octopus_lib)
# Setup octopus executable
set_target_properties(Octopus_octopus PROPERTIES
		OUTPUT_NAME octopus
		)

# Include necessary non-cmake bundled libs
add_subdirectory(external_libs)

# Argument parser
add_subdirectory(liboct_parser)

# Main project definition in src folder
add_subdirectory(src)

# Auxiliary files
add_subdirectory(share)

# Integration tests. These are run as part of Octopus_lib and must be built
# TODO (Alex). Issue #1194 Move test_integrations/ to the octopus root
# once autotools is removed. It's current in src/ for simplicity.
# add_subdirectory(test_integrations)

# Unit testing
if (OCTOPUS_UNIT_TESTS)
	message("-- Unit tests enabled")
	enable_testing()
	add_subdirectory(unit_tests)
endif()

# Application testsuite
if (OCTOPUS_APP_TESTS)
    enable_testing()
	add_subdirectory(testsuite)
	file(COPY testsuite DESTINATION ${PROJECT_BINARY_DIR}/share
			REGEX ".*\\.(am|in|txt|pl)\$"  EXCLUDE)
endif()

# Doxygen
add_subdirectory(doc/doxygen)

#[==============================================================================================[
#                                       Sanity Checking                                         #
]==============================================================================================]

if(TARGET OpenMP::OpenMP AND NOT OCTOPUS_OpenMP)
	message(FATAL_ERROR "OpenMP was searched for and found even though it's supposed to be disabled")
endif()

if(TARGET MPI::MPI_Fortran AND NOT OCTOPUS_MPI)
	message(FATAL_ERROR "MPI was searched for and found even though it's supposed to be disabled")
endif()

#[==============================================================================================[
#                                       Install or Export                                       #
]==============================================================================================]
# Defined in subdirectories


#[==============================================================================================[
#                                       Packaging                                               #
]==============================================================================================]
# Currently only source

set(CPACK_PACKAGE_NAME "octopus")
set(CPACK_PACKAGE_VENDOR "www.octopus-code.org")
set(CPACK_GENERATOR "")
set(CPACK_ARCHIVE_THREADS 0)
set(CPACK_SOURCE_GENERATOR "TXZ")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-${CMAKE_PROJECT_VERSION_MAJOR}.${CMAKE_PROJECT_VERSION_MINOR}")
set(CPACK_PACKAGE_DIRECTORY ${PROJECT_SOURCE_DIR}/pack CACHE PATH "Directory where CPack will build and deploy packages")

list(APPEND CPACK_SOURCE_IGNORE_FILES   [[/build/]]
                                        "${CPACK_PACKAGE_DIRECTORY}"
                                        "${CPACK_BINARY_DIRECTORY}"
                                        [[/cmake-]]
                                        [[/\\.git]]
                                        [[/\\.gitlab-ci.d]]
                                        [[\\.user$]]
                                        [[\\.gitignore]]
                                        [[\\.codecov\\.yml]]
                                        [[/debian/]]
                                        # Exclude autotools related files
                                        [[PACKAGING]]
                                        [[configure.ac]]
                                        [[Makefile.am]]
                                        [[m4]]
                                    )


include(CPack)
