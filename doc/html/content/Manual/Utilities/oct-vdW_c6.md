---
title: "oct-vdW_c6"
#series: "Manual"
---


### NAME
oct-vdW_c6 - C6 coefficient from a pair of cross_section_tensor files  

### USAGE
oct-vdW_c6 cross_section1 cross_section2

### DESCRIPTION
This script computes C6 coefficient from a pair of cross_section_tensor files, using the Casimir-Polder formula

$$
C\_{6,ij}=\\frac{3}{\\pi}\\int d\\omega \\alpha\_i(\\omega)\\alpha\_j(\\omega) .
$$


---------------------------------------------
