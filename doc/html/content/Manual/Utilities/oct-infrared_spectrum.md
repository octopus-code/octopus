---
title: "oct-infrared_spectrum"
#series: "Manual"
---


### NAME
oct-infrared_spectrum - Infrared spectrum obtained from the dipole moment 

### DESCRIPTION

This utility reads the time-dependent dipole from the {{< file "td.general/multipole" >}} file, applies a sinc enveloppe, removes the DC component and then performs a Fourier transform.

At the end, the code produces {{< file "td.general/infrared" >}}.

This utility honours the variables {{< variable "PropagationSpectrumStartTime" >}}, {{< variable "PropagationSpectrumEndTime" >}}, and {{< variable "PropagationSpectrumMaxEnergy" >}}.

---------------------------------------------
