---
title: "Debugging and profiling"
section: "Manual"
weight: 10
description: " "
---


When implementing new features in {{< octopus >}}, it is often convenient to be able to get debug information or profiling information.
 {{< octopus >}} offers several internal tools to allow for debugging and profiling, which are explained in this page.

#### Parsed variables from an input file
If you look at the file {{< file "exec/parser.log" >}}, it will tell you the value of the variables that you set with the {{< file "inp" >}} file, as well as all the variables which are taking their default value. This can sometimes be helpful in understanding the behavior of the program.
In particular, all the values of the variables are present, but also all the variables that were implicitely parsed (hence using their default values) are stated.

#### Stack trace and verbose mode
The variable {{< variable "Debug" >}} allows for different different level of diagnostic:
 - {{< variable "Debug" >}}={{< code info >}} is a verbose mode that displays extra diagnostic info and a stack trace with any fatal error.
Additionally, a {{< code debug >}} folder is generated, containing extra debug files, like pseudopotential information, LCAO information, ...
 - {{< variable "Debug" >}}={{< code trace >}} provides the full stack strace in case of failing assertion or internal fatal error message.
Note that no information is printed in case of segmentation fault. In order to get a back trace in this case, please use compiler options or a debugger.
 - {{< variable "Debug" >}}={{< code trace_file >}} generate for each MPI task a file with every entry and exit of all routines. This allows for debugging MPI runs but be aware that this slows down signicatively the execution of the code.

Note that options could be combined, like {{< variable "Debug" >}}={{< code info >}}+{{< code trace >}}

### Memory and performance profiling
{{< octopus >}} offers several ways to profile memory. It is possible to get a report of the memory used using {{< variable "ReportMemory" >}}=yes.
Alternatively, one can get {{< variable "ProfilingMode" >}}={{< code prof_memory >}} or {{< variable "ProfilingMode" >}}={{< code prof_memory_full >}} to get memory reports with different level of details.

The elapsed time profiling is obtained by {{< variable "ProfilingMode" >}}={{< code prof_time >}}. This is a lightweight profiler that has no impact on the code performances, and can hence be used to understand better where the time is spent.


### Other options
There exists some more internal variables for advanced debugging and profiling. Please refer to their variable description:
 - {{< variable "ProfilingAllNodes" >}}
 - {{< variable "ReportMemory" >}}
 - {{< variable "MPIDebugHook" >}}
 - {{< variable "MemoryLimit" >}}
 - {{< variable "ProfilingOutputYAML" >}}
 - {{< variable "ProfilingOutputTree" >}}

{{< manual-foot prev="Basics:Troubleshooting" next="Calculations:Ground State" >}}
