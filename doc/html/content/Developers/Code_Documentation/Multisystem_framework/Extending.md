---
Title: Extending the framework
Weight: 10
---



Extending the multisystem framework, in general, consists of several aspects:

* Adding new systems
* Adding new interactions
* Add new couplings (exposed {{<versioned-link "Developers/Code_Documentation/Multisystem_framework/Quantities" "Quantities" >}})

For all of the above, there are certain routines which have to be implemented, and certain flags, which have to be defined.
These will be outlined in this document.


## Creating a new system

### Create the Fortran class:

New systems must be created by extending the 
[`system_t`](https://octopus-code.org/doc/main/doxygen_doc/structsystem__oct__m_1_1system__t.html) class (or a class derived from system_t).

The class should own (by composition) the components and fields needed to describe the system, its (ground) state and its time propagation.

Every new system class must implement the deferred methods of 
[`interaction_partner_t`](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__partner__oct__m_1_1interaction__partner__t.html)
and
[`system_t`](https://octopus-code.org/doc/main/doxygen_doc/structsystem__oct__m_1_1system__t.html) class (or a class derived from system_t).

### Write the constructor:

The 'constructor' must be a function which takes the namespace as argument and returns a pointer to the new object. As constructors are not overloading any deferred methods, they can take additional arguments, if needed.

The constructors must first allocate the new object, and assign the new objects namespace to the one given as argument. These are the minimum requirements to be operable with the system factory.

Here, we also need to define the supported interactions and declare exposed {{<versioned-link "Developers/Code_Documentation/Multisystem_framework/Quantities" "Quantities" >}} (see below).

In general, the constructor is also the right place to query related input variables and set the corresponding variables.

Here we can also define the supported interactions. These are stored in the arrays `supported_interactions` and `supported_interactions_as_partner`. These arrays need to be allocated first.

{{<notice note>}}
They can be allocated with size 0. See note below.
{{</notice>}}

```Fortran
    allocate(this%supported_interactions(0))
    allocate(this%supported_interactions_as_partner(0))
```

We can then extend this list by adding new interactions, as shown below:
```Fortran
   this%supported_interactions = [this%supported_interactions, <NEW_INTERACTION>]
   this%supported_interactions_as_partner = [this%supported_interactions_as_partner, <NEW_INTERACTION>]
```
{{<notice note>}}
From the Fortran 2003 standard, this code automatically re-allocates the arrays with the new required size.
{{</notice>}}


### Define exposed quantities:

New systems will need to interact with other systems. This is done via
[interactions](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__oct__m_1_1interaction__t.html) and so-called couplings.

A number of available {{<versioned-link "Developers/Code_Documentation/Multisystem_framework/Quantities" "Quantities" >}}, which can be used as couplings, are already defined in
{{< source "multisystem/core/quantity.F90" "quantity.F90" >}}:
```Fortran
#include_code_doc quantity
```
If no definition matches your quantity, you can add it to this list.

The parent `class interaction_partner_t` provides an array of type `quantity_t`. For each quantity, the system exposes, we can define their properties (see [`quantity_t`](https://octopus-code.org/doc/main/doxygen_doc/structquantity__oct__m_1_1quantity__t.html)). For instance:
```Fortran
sys%quantities(DIPOLE)%updated_on_demand = .true.
```
indicates, that the dipole moment is used by an interaction, and needs to be updated in a routine `system%update_quantity()` (see below).
Some quantities are needed for the propagation of the system itself (e.g. the position of a classical particle). For such quantities, we need to set `update_on_demand = .false.`.




## Add to system factory:

Octopus uses a so-called factory to create systems at run time. the system factory
defines the allowed systems in
```Fortran
#include_code_doc system_types
```
Thus list needs to be extended to allow for new types.

the systems are created using the `factory%create(namespace, type)` call
{{% expand "Expand for the source of system_factory_create()" %}}
```Fortran
#include_function system_factory_create
```
{{% /expand %}}


### Implement algorithmic steps:

The propagation (and also the SCF algorithm) of the system is performed using abstract
algorithms. These algorithms define algorithmic operations, which the systems might support.

For each system of a calculation, the multisystem framework calls the function `system%do_algorithmic_operation()`, which needs to be implemented by each system.
This function consists of a large `select case` construct, which distinguishes the 
various algorithmic steps, the system can perform.

The function needs to return a logical indicating whether the step was successfully performed, and an array `updated_quantities`, which lists the non-on-demand quantities,
which have been updated in the performed operation.






## Creating a new interaction

### Create the interaction class:

New interactions are derived from the abstract 
[`interaction_t`](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__oct__m_1_1interaction__t.html)
 class.
For specific types of interactions, it might be beneficial to extend one of the derived classes, such as 
[`force_interaction_t`](https://octopus-code.org/doc/main/doxygen_doc/structforce__interaction__oct__m_1_1force__interaction__t.html)
,
[`density_interaction_t`](https://octopus-code.org/doc/main/doxygen_doc/structdensity__interaction__oct__m_1_1density__interaction__t.html)
 or
[`potential_interaction_t`](https://octopus-code.org/doc/main/doxygen_doc/structpotential__interaction__oct__m_1_1potential__interaction__t.html)
.

Interaction classes, in general' keep copies of the partner's couplings, which allows the partners to be updated without invalidating the interaction.

### The constructor

Similar to systems, interactions need a constructor or init function, which is called by the interaction factory. This function must return a pointer to the new object, and take the interaction partner as argument.

Inside the constructor we need to set the label, and assign the partner pointer to the actual interaction partner.  For the `gravity_t` interaction, this reads like

```Fortran
#include_function gravity_constructor
```
Like for systems, new interaction classes also must implement the deferred methods of 
[`interaction_t`](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__oct__m_1_1interaction__t.html).

### Specify couplings:

As seen in the example above, the interactions must declare which couplings they require from the system (on which the interaction has an effect), and the interaction partner (from which the interaction originates). This is done by adding the respective quantity ID's to the arrays:
```Fortran
this%system_quantities
this%couplings_from_partner
```


### Write initialization routines:

Certain properties of the interaction can only be initialized from the system or the partner.

For this, interaction systems and partners provide these deferred methods:

* [`partner%init_interaction_as_partner()`](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__partner__oct__m_1_1interaction__partner__t.html#a90848e5ca1ca15b69b9d76e76b062e24)

In general, this routine should call the init() routine of the supported interactions,
if necessary perform other related tasks, and throw an fatal error if called for an
unsupported interaction.

* [`system%init_interaction()`](https://octopus-code.org/doc/main/doxygen_doc/structsystem__oct__m_1_1system__t.html#a6a97df76e84ee49a6550a39aa5748c44)

In general, interactions need to have data components with dimensions, which depend on the partner, e.g. the spatial dimensions. These quantities should be allocated and initialized in this routine.


### Add to the interaction factory

Similar to the systems, interactions are created by an interaction factory. In order to register a new interaction, we must add it to the interactions enumerator in {{<source "interactions/interactions_enum.F90" "interactions_enum.F90">}}
```Fortran
#include_code_doc interaction_types
```
and to the function `interactions_factory_create()`:
{{% expand "Source of interactions_factory_create" %}}
```Fortran
#include_function interactions_factory_create
```
{{% /expand %}}





## Connect interactions to the systems

Finally, the partners need to copy their couplings to the interaction and systems need to act on the interaction.

* [`partner%copy_quantities_to_interaction`](https://octopus-code.org/doc/main/doxygen_doc/structinteraction__partner__oct__m_1_1interaction__partner__t.html#a9bae5a884caf33cfef7c0c59d4403fbf)

Here, the partners should copy their couplings to the corresponding variables of the interaction.

{{%expand "See, e.g. for classical_particle_t" %}}
```Fortran
#include_subroutine classical_particle_copy_quantities_to_interaction
```
{{% /expand %}}

Systems need to copy the data (e.g. forces) from the interaction. Usually, this is done when needed, as part of an
algorithmic operation.


## Some notes on on_demand quantities

For a more detailled discussion on how quantities are updated, see {{<versioned-link "Developers/Code_Documentation/Multisystem_framework/Quantities" "Quantities" >}}