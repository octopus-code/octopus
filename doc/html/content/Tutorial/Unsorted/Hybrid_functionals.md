---
title: "Hybrid functionals"
series: "Tutorials"
author: "Nicolas Tancogne-Dejean"
theories: ["DFT", "Hartree-Fock"]
system_types: "Bulk"
description: "how to use hybrid functionals."
---


The objective of this tutorial is to give a basic idea of how to perform Hartree-Fock, as well as hybrid functionals 
calculations within the framework of generalized Kohn-Sham calculation.

##  Input   
As always, we will start with a simple input file. In this example, we will look at the example of solid argon, treated using the HSE06 functional.

{{< code-block >}}
#include_input doc/tutorials/other/hybrids/1.gks/inp
{{< /code-block >}}

Let us look at the different input variable. Most of them have already been described in the Periodic System tutorials.
What we have added here are:
* {{< variable "XCFunctional" >}} = hyb_gga_xc_hse06: This specifies that we are using the HSE06 exchange-correlation functional.
This implies that the code will work in the generalized Kohn-Sham framework, which allows for nonlocal operators. 
If we would like to work in the usual Kohn-Sham framework, we would need to use the Optimized Effective Potential (OEP) method.
* {{< variable "AdaptivelyCompressedExchange" >}} = yes: This activates the use of the Adaptively Compressed Exchange (ACE) 
operator proposed in [^footnote-1], which makes the evaluation of the exchange operator much faster.
This is activated by default starting from Octopus 16.0.

##  Output   

Now run Octopus using the above input file. Here are some important things to note from the output. 
First of all, we see that the theory level has been automatically switched to <tt>generalized_kohn_sham</tt>, 
and that the code is rightly using the requested HSE06 functional.

{{< code-block >}}
#include_file doc/tutorials/other/hybrids/1.gks/Theory_level.txt
{{< /code-block >}}

The rest of the output is quite standard.
As one main effect of using the HSE06 functional is to open the bandgap of solid argon.
Indeed, looking at the <tt>static/info</tt> file, we see that we obtain a bandgap of roughly 10.3 eV:

{{< code-block >}}
#include_file doc/tutorials/other/hybrids/1.gks/gap.txt
{{< /code-block >}}
whereas a similar PBE calculation produces a bandgap of only 8.6eV.

##  Hartree-Fock  

If we want to perform not an hybrid-functional calculation but an Hartree-Fock calculation, we simply need to replace the definition of the functional by the line
{{< code-block >}}
  {{< variable "TheoryLevel" >}} = hartree_fock
{{< /code-block >}}

{{<notice note>}}
Note that this result is not yet converged with respect to the k-points. 
{{</notice>}}

The ACE operator should still be used here to obtain the best performance.
Using the same input file but for Hartree-Fock, we obtain the HF bandgap of solid argon, which is roughly of 15.9 eV:
{{< code-block >}}
#include_file doc/tutorials/other/hybrids/2.hf/gap.txt
{{< /code-block >}}



##  Using the ACE operator for TD calculations  

Here we want to mention an important aspect of the ACE operator for TD simulations.
By construction, the ACE operator is only defined at convergence of an SCF loop. For TD calculations, this implies that one should perform a self-consistency at every step of a TD calculation.
At the moment, this is only implemented for the ETRS propagator, and we strongly recommend to use the following lines for a TD calculation using the ACE operator ({{< variable "AdaptivelyCompressedExchange" >}} = yes):
{{< code-block >}} 
  {{< variable "TDPropagator" >}} = etrs
  {{< variable "TDStepsWithSelfConsistency" >}} = all_steps
{{< /code-block >}}

##  References  

[^footnote-1]: {{< article title="Adaptively Compressed Exchange Operator" authors="Lin Lin" journal="J. Chem. Theory Comput." volume="12" pages="2242–2249" year="2016" doi="10.1021/acs.jctc.6b00092" >}}

