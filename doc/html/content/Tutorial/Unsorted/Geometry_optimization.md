---
title: "Geometry optimization"
tags: ["Tutorial", "Beginner", "Geometry Optimization", "Molecule", "Pseudopotentials", "DFT", "Total Energy", "Forces"]
series: "Tutorials"
difficulties: "Beginner"
theories: "DFT"
calculation_modes: "Geometry optimization"
species_types: "Pseudopotentials"
system_types: "Molecule"
features: ["Total energy", "Forces"]
description: "Determining the geometry of methane."
---


In this tutorial we will show how to perform geometry optimizations using {{< octopus >}}.

## Methane molecule

###  Manual optimization
We will start with the methane molecule. Before using any sophisticated algorithm for the geometry optimization, it is quite instructive to do it first "by hand". Of course, this is only practical for systems with very few degrees of freedom, but this is the case for methane, as we only need to optimize the CH bond-length. We will use the following files.

####  {{< file inp >}} file

{{< code-block >}}
#include_input /doc/tutorials/other/geometry_optimization/1.methane/1.script/inp
{{< /code-block >}}

This input file is similar to ones used in other tutorials. We have seen in the {{< tutorial "Basics:Total energy convergence" "Total_energy_convergence" >}} tutorial that for these values of the spacing and radius the total energy was converged to within 0.1 eV.

####  {{< file ch.sh >}} script
You can run the code by hand, changing the bond-length for each calculation, but you can also use the following script:

```bash
#include_file /doc/tutorials/other/geometry_optimization/1.methane/1.script/ch.sh
```

#include_eps /doc/tutorials/other/geometry_optimization/1.methane/1.script/Methane_Energy_vs_CH.eps caption="Energy versus CH bond length for methane"

####  Results

Once you run the calculations, you should obtain values very similar to the following ones:

{{< code-block >}}
#include_file /doc/tutorials/other/geometry_optimization/1.methane/1.script/ch.dat
{{< /code-block >}}

You can see on the right how this curve looks like when plotted. By simple inspection we realize that the equilibrium CH distance that we obtain is around 1.1 Å. This should be compared to the experimental value of 1.094 Å. From this data can you calculate the frequency of the vibrational breathing mode of methane?

### FIRE

Since changing the bond-lengths by hand is not exactly the simplest method to perform the geometry optimization, we will now use automated algorithms for doing precisely that. Several of these algorithms are available in {{< octopus >}}. The default and recommend one is the the FIRE method[^footnote-1]
.
{{< octopus >}} can also use the [GSL optimization routines](https://www.gnu.org/software/gsl/manual/html_node/Multidimensional-Minimization.html), so all the methods provided by this library are available.

#### Input

First we will run a simple geometry optimization using the following input file:

{{< code-block >}}
#include_input /doc/tutorials/other/geometry_optimization/1.methane/2.fire/inp
{{< /code-block >}}

In this case we are using default values for all the input variables related to the geometry optimization. Compared to the input file above, we have changed the {{< variable "CalculationMode" >}} to {{< code go >}}. Because the default minimum box of spheres centered on the atoms has some caveats when performing a geometry optimization (see bellow for more details), we also changed the box shape to a single sphere with a 5 Å radius. Like always, convergence of the final results should be checked with respect to the mesh parameters. In this case you can trust us that a radius of 5 Å is sufficient.

The geometry in the input file will be used as a starting point. Note that although a single variable CH was used to specify the structure, the coordinates will be optimized independently.

#### Output

When running {{< octopus >}} in geometry optimization mode the output will be a bit different from the normal ground-state mode. Since each minimization step requires several SCF cycles, only one line of information for each SCF iteration will printed:

```
#include_file doc/tutorials/other/geometry_optimization/1.methane/2.fire/Iteration_1.txt
```

At the end of each minimization step the code will print the total energy, the maximum force acting on the ions and the maximum displacement of the ions from the previous geometry to the next one. {{< octopus >}} will also print the current geometry to a file named {{< file "go.XXXX.xyz" >}} (XXXX is the minimization iteration number) in a directory named {{< file "geom" >}}. In the working directory there is also a file named {{< file "work-geom.xyz" >}} that accumulates the geometries of each SCF calculation. The results are also summarized in {{< file "geom/optimization.log" >}}. The most recent individual geometry is in {{< file "last.xyz" >}}.

After some minimization steps the code will exit.

```
#include_file doc/tutorials/other/geometry_optimization/1.methane/2.fire/Iteration_last.txt
```

If the minimization was successful a file named {{< file "min.xyz" >}} will be written in the working directory containing the minimized geometry. In this case it should look like this:

```
#include_file doc/tutorials/other/geometry_optimization/1.methane/2.fire/min.xyz
```

Has the symmetry been retained? What is the equilibrium bond length determined? Check the forces in {{< file "static/info" >}}. You can also visualize the optimization process as an animation by loading {{< file "work-geom.xyz" >}} as an XYZ file into XCrySDen.

## Sodium trimer

Let's now try something different: a small sodium cluster. This time we will use an alternate method related to conjugate gradients.

#### Input

Here is the input file to use

{{< code-block >}}
#include_input doc/tutorials/other/geometry_optimization/2.sodium_trimer/1.cg_bfgs/inp
{{< /code-block >}}

and the corresponding {{< file "inp.xyz" >}} file

```
#include_file doc/tutorials/other/geometry_optimization/2.sodium_trimer/1.cg_bfgs/inp.xyz
```

In this case the starting geometry is a random geometry.

#### Output
After some time the code should stop successfully. The information about the last minimization iteration should look like this:

```
#include_file doc/tutorials/other/geometry_optimization/2.sodium_trimer/1.cg_bfgs/Iteration_last.txt
```

You might have noticed that in this case the code might perform several SCF cycles for each minimization iteration. This is normal, as the minimization algorithm used here requires the evaluation of the energy and forces at several geometries per minimization iteration. The minimized geometry can be found in the {{< file "min.xyz" >}} file:

```
#include_file doc/tutorials/other/geometry_optimization/2.sodium_trimer/1.cg_bfgs/inp.xyz
```

#### Caveats of using a minimum box

For this example we are using the minimum box ({{< code-inline >}}{{< variable "BoxShape" >}} = minimum{{< /code-inline >}}). This box shape is the one used by default, as it is usually the most efficient one, but it requires some care when using it for a geometry optimization. The reason is that the box is generated at the beginning of the calculation and is not updated when the atoms move. This means that at the end of the optimization, the centers of the spheres composing the box will not coincide anymore with the position of the atoms. This is a problem if the atoms move enough such that their initial and final positions differ significantly. If this is the case, the best is to restart the calculation using the previously minimized geometry as the starting geometry, so that the box is constructed again consistently with the atoms. In this example, this can be done by copying the contents of the {{< file "min.xyz" >}} file {{< file "inp.xyz" >}} and running {{< octopus >}} again without changing input file.

This time the calculation should converge after only one iteration:

```
#include_file doc/tutorials/other/geometry_optimization/2.sodium_trimer/2.restart/Iteration_last.txt
```


You can now compare the old {{< file "min.xyz" >}} file with the new one:

```
#include_file doc/tutorials/other/geometry_optimization/2.sodium_trimer/2.restart/inp.xyz
```

Although the differences are not too large, there are also not negligible.

#### Further improving the geometry
Imagine that you now want to improve the geometry obtained and reduce the value of the forces acting on the ions. The stopping criterion is controlled by the {{< variable "GOTolerance" >}} variable (and {{< variable "GOMinimumMove" >}}). Its default value is 0.051 eV/Å (0.001 H/b), so let's set it to 0.01 eV/Å:

{{< code-block >}}
#include_input doc/tutorials/other/geometry_optimization/2.sodium_trimer/3.restart/inp
{{< /code-block >}}

Copy the contents of file {{< file "min.xyz" >}} to file {{< file "inp.xyz" >}} and rerun the code. This time, after some minimization steps {{< octopus >}} should return the following error message:

{{< code-block >}}
 **************************** FATAL ERROR *****************************
 *** Fatal Error (description follows)
 *--------------------------------------------------------------------
 * Error occurred during the GSL minimization procedure:
 * iteration is not making progress towards solution
 **********************************************************************
{{< /code-block >}}

This means the minimization algorithm was unable to improve the solutions up to the desired accuracy. Unfortunately there are not many things that can be done in this case. The most simple one is just to restart the calculation starting from the last geometry (use the last {{< file "go.XXXX.xyz" >}} file) and see if the code is able to improve further the geometry.











---------------------------------------------
[^footnote-1]: {{< article title="Structural relaxation made simple" authors="Erik Bitzek, Pekka Koskinen, Franz Gähler, Michael Moseler, and Peter Gumbsch" journal="Phys. Rev. Lett." volume="97" pages="170201" year="2006" url="https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.97.170201" doi="10.1103/PhysRevLett.97.170201" link="APS" >}}
