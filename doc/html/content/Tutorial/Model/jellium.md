---
title: "Jellium and jellium slabs"
weight: 3
series: "Tutorials"
tutorials: "Model Systems"
difficulties: "Beginner"
difficulties_weight: 2
calculation_modes: ["Ground state"]
system_types: "Model"
species_types: "User-defined species"
description: "Getting the electron density of a homogeneous electron gas, and of a jellium slab."
---

In this tutorial, we will compute the ground state of a uniform electron gas, as well as the one of a jellium slab, which is a good model for metallic surfaces.

### Jellium

Let us start by performing a calculation for a uniform electrom gas, or jellium. In this tutorial, we choose as an example a Wigner-Seitz radius $r_s=5.0$.
We then create a cubic box with the number of electron corresponding to the density $n$ given by

$$
\frac{1}{n} = \frac{4}{3}\pi r_s^3
$$

For doing so, we define a constant potential in the box using the {{< variable "Species" >}} block, as done in the input file below
{{< code-block >}}
#include_input doc/tutorials/model_systems/jellium/1.bulk/inp
{{< /code-block >}}

### Jellium slab

In order to describe a jellium slab, we need to use the special {{< variable "Species" >}} call "species_jellium_slab".
This species has two relevant parameters, one is the thickness of the slab, and one is the number of electrons.
This is specified as follow:
{{< code-block >}}
%Species
"jellium" | species_jellium_slab | thickness | d0 | valence | N_electrons
%
{{< /code-block >}}

where "d_0" and  "N_electrons" are values defined in the input file.
As the homogeneous electrons gas are usually parametrized by the Wigner-Seitz radius $r_s$, the number of electrons in the slab can be defined as

$$
N_e = A d_0 / (4 \pi r_s^3 / 3)
$$

where $A$ is the area of the slab included in the simulation box.

The full input file then reads

{{< code-block >}}
#include_input doc/tutorials/model_systems/jellium/2.slab/inp
{{< /code-block >}}

Here, we used a unit cell of $1.6\text{\AA}\times1.6\text{\AA}$ in-plane size, and a size along the perpendicular direction of $25 \text{\AA} $. We then a jellium slab of thickness $16 \text{\AA} $, centered here around $z=0$, with $r_s=5$, which correspond to 0.5279 electrons in the system.
Because of the fractional occupations, and because we describe a metallic surface, we employed here a small {{< variable "Smearing" >}} of 10 meV, with a Fermi-Dirac distribution.

After running this input file, one can plot the density of the jellium slab.
{{< figure src="/images/jellium_slab_density.png" width="500px" caption="Fig. Electron density of the jellium slab, compared to the profile of the uniform background density (black lines)."  >}}

Clear Friedel oscillations of the electron density near the surface are observed, and the result can be compared to the result obtained by Lang and Kohn, see Fig. 2 in Ref.[^footnote-1].

This plot is generated from the script:
```bash
#include_file doc/tutorials/model_systems/jellium/2.slab/plot.gp
```

##  References
<references/>

[^footnote-1]: {{< article title="Theory of Metal Surfaces: Charge Density and Surface Energy" authors="N. D. Lang and W. Kohn" journal="Phys. Rev. B" volume="1" pages="4555" year="1970" doi="10.1103/PhysRevB.1.4555" >}}

