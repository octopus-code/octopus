---
title: "CECAM workshop April 2024"
weight: 80
description: "Basic introduction to run Octopus"
---

This is a series of linked tutorials that was given at the CECAM school 
[Ab Initio Quantum Electrodynamics for Quantum Materials Engineering](https://www.cecam.org/workshop-details/ab-initio-quantum-electrodynamics-for-quantum-materials-engineering-1294).

In this school, a new way of running the tutorials, was used. The tutorials were presented in form or Jupyter notebooks, which can be run through a binder using cloud computing resources of the 
[MPCDF](https://www.mpcdf.mpg.de/).

The tutorials in this school focussed on the interaction of matter and light. In particular the binder contains the tutorials:

* Octopus basics (Jupyter notebook version of {{< tutorial "Basics" "Octopus Basics" >}} )
* Cavity-modified band structure using a QEDFT electron-photon exchange functional
* QED linear response:
  * Optical spectra from real-time QEDFT
  * Optical spectra from the electron-photon Casida equation
  * Optical spectra from the electron-photon Sternheimer equation
* Maxwell TDDFT
  * Space- and time-dependent external source
  * Tutorial on full minimal coupling within the self-consistent Maxwell-TDDFT framework

The Jupyter notebooks can be found in this [repository](https://gitlab.com/mpsd-training/workshops/cecam-qed-school-2024-participants), however, the equations are not properly rendered when viewed in gitlab. 
To see them correctly, it is recommended to use the binder, where the notebooks can be executed.

To launch an instance, click on the following badge:
[![Binder](https://mybinder.org/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fmpsd.training%2Fworkshops%2Fcecam-qed-school-2024-participants.git/HEAD)
