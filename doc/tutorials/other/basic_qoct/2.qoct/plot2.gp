set xlabel "Iteration number"
set ylabel "Function value"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "convergence.eps"
set key bottom right

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5


plot "opt-control/convergence" u 1:2 t 'J' w lp linecolor rgb "black", '' u 1:3 t 'J1' w lp linecolor rgb "red"
