#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

cp inp_gs inp

mpirun -n 4 octopus |tee log


cp inp_resp inp

mpirun -n 4 octopus |tee log

$HELPER_DIR/extract_generic.sh log "Polarizabilities" "SCF Residual" > Response.txt
$HELPER_DIR/extract_generic.sh log "x-direction and frequency 0.1500" "LR SCF Iteration:   2"  | head -n -1 > Response-x.txt

cp inp_gs inp_resp tutorial.sh *.txt em_resp/freq_0.0000/alpha $OCTOPUS_TOP/doc/tutorials/other/sternheimer_lr/
