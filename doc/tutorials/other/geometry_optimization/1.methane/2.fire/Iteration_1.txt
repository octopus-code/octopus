Info: Starting SCF iteration.
iter    1 : etot -2.27991992E+02 : abs_dens 2.03E+00 : etime     0.7s
iter    2 : etot -2.27947938E+02 : abs_dens 1.81E-01 : etime     0.1s
iter    3 : etot -2.21131250E+02 : abs_dens 4.01E-01 : etime     0.4s
iter    4 : etot -2.17934989E+02 : abs_dens 2.16E-01 : etime     0.2s
iter    5 : etot -2.18667820E+02 : abs_dens 6.44E-02 : etime     0.2s
iter    6 : etot -2.18394307E+02 : abs_dens 1.23E-02 : etime     0.2s
iter    7 : etot -2.18301945E+02 : abs_dens 2.80E-03 : etime     0.3s
iter    8 : etot -2.18295247E+02 : abs_dens 2.24E-04 : etime     0.2s
iter    9 : etot -2.18290443E+02 : abs_dens 1.19E-04 : etime     0.1s
iter   10 : etot -2.18286567E+02 : abs_dens 1.00E-04 : etime     0.2s
iter   11 : etot -2.18286878E+02 : abs_dens 8.22E-06 : etime     0.1s
iter   12 : etot -2.18286749E+02 : abs_dens 8.95E-06 : etime     0.2s
iter   13 : etot -2.18286927E+02 : abs_dens 2.06E-06 : etime     0.1s
iter   14 : etot -2.18286925E+02 : abs_dens 8.01E-07 : etime     0.1s

             Info: Writing states. 2024/10/18 at 10:41:24


        Info: Finished writing states. 2024/10/18 at 10:41:24

Info: SCF converged in   14 iterations

Info: Number of matrix-vector products:        350
Info: Starting SCF iteration.
