#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

cp inp_go inp

mpirun -n 4 octopus |tee log

cp inp_gs inp

mpirun -n 4 octopus |tee log


cp inp_vib inp

mpirun -n 4 octopus |tee log

cp inp_go inp_gs inp_vib tutorial.sh *.txt vib_modes/normal_frequencies_lr $OCTOPUS_TOP/doc/tutorials/other/vibrational_modes/
