set pm3d
set view map

set t postscript enhanced color font "Monospace-Bold,20" landscape size 11,8.5
set output "ARPES_PATH.eps"

set xlabel "k (1/Bohr)"
set ylabel "kinetic energy (eV)"

set cbrange [0:]

unset surface
unset key

set pm3d map
sp 'PES_ARPES.path' u 1:4:5
