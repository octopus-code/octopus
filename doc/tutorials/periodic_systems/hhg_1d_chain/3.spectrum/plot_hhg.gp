set xlabel "Energy [harmonic order]"
set ylabel "log(Harmonic yield) [a.u.]"
set t postscript enhanced color font "Monospace-Bold,25" landscape size 11,8.5
set output "HHG_1d_chain_hhg.eps"

set rmargin 4.5
set lmargin 10.5
set tmargin 3.2
set bmargin 5.5

w0 = 0.055123955

set xtics 1

plot 'hs-mult.x_nk24' u ($1/w0):(log($2))w l lw 3 t 'nk 25',\
     'hs-mult.x'  u ($1/w0):(log($2))w l lw 3 t 'nk 50'
