#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

cp inp_gs inp

mpirun -n 4 octopus |tee log

cp inp_referemce inp

mpirun -n 4 octopus |tee log


mv td.general td.general_ref

./script.pl

./script_gather.pl


gnuplot plot.gp

cp inp_gs inp_reference inp_ref *.pl plot.gp *.eps $OCTOPUS_TOP/doc/tutorials/periodic_systems/transient_abs/