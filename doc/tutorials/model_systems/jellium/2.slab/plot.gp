set xlabel "z [Bohr]"
set ylabel "Density n(z)"
set t pngcairo font "Monospace-Bold,20" size 800, 600

set output "jellium_slab_density.png"

rs = 5.0
rho = 1/(4.0/3.0*pi*(rs**3))
Lz = 30.2356/2

set arrow nohead from -Lz,0 to -Lz,rho
set arrow nohead from Lz,0 to Lz,rho
set arrow nohead from -Lz,rho to Lz,rho

plot 'static/density.x=0,y=0' w l notitle

