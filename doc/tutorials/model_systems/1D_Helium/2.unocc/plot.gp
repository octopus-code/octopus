set xlabel "x1"
set ylabel "x2"
set t postscript enhanced color font "Monospace-Bold,20" landscape size 11,8.5
set key top left

set hidden3d
set pm3d
set contour
set ticslevel 0
unset key
unset surface

set output "Wfs-st00001.eps"
splot 'static/wf-st00001.z=0' using 1:2:3

set output "Wfs-st00002.eps"
splot 'static/wf-st00002.z=0' using 1:2:3

set output "Wfs-st00003.eps"
splot 'static/wf-st00003.z=0' using 1:2:3
