#ifndef CONFIG_H
#define CONFIG_H

// Windows does not support _Thread_local. Use appropriate aliases
// Reference: https://stackoverflow.com/a/18298965
#ifndef TLS_SPECIFIER
#if __STDC_VERSION__ >= 201112 && !defined __STDC_NO_THREADS__
#define TLS_SPECIFIER _Thread_local
#elif defined _MSC_VER
#define TLS_SPECIFIER __declspec(thread)
#elif defined __GNUC__
#define TLS_SPECIFIER __thread
#else
#error "Cannot define thread_local"
#endif
#endif

#endif
