!! Copyright (C) 2024 Cristian Le, Alex Buccheri

!> @brief Register tests by providing a test suite instance containing all tests associated
!!  with an Octopus module.
!!
!! For example:
!! ```
!! call execute_app(test_list([testsuite_oct_module, testsuite_oct_module2]))
!! ```
!! where a test_suite instance contains the module name, and an array of test_case
!! instances for routines in that module (one test_case per unit test):
!! ```
!! testsuite_sort = suite("basic/sort", test_list([&
!!                             test_case("test_sort", test_sort)&
!!                       ]))
!!```
!! Note: this routine does not return but stops the program with the right exit code.
program test_suite
  use fortuno_interface_m

  ! Test drivers
  use driver_basic_oct_m
  use driver_basis_set_oct_m
  use driver_boxes_oct_m
  use driver_classical_oct_m
  use driver_communication_oct_m
  use driver_electrons_oct_m
  use driver_grid_oct_m
  use driver_hamiltonian_oct_m
  use driver_interactions_oct_m
  use driver_ions_oct_m
  use driver_math_oct_m
  use driver_maxwell_oct_m
  use driver_mesh_oct_m
  use driver_opt_control_oct_m
  use driver_output_oct_m
  use driver_photons_oct_m
  use driver_poisson_oct_m
  use driver_scf_oct_m
  use driver_species_oct_m
  use driver_states_oct_m
  use driver_sternheimer_oct_m
  use driver_td_oct_m

  implicit none

  ! Note, execute_cmd_app is responsible for initialising and finalising the MPI env
  ! when compiled with MPI support
  call execute_cmd_app(test_list([&
          testsuite_basic(), &
                  testsuite_basis_set(), &
                  testsuite_boxes(), &
                  testsuite_classical(), &
                  testsuite_communication(), &
                  testsuite_electrons(), &
                  testsuite_grid(), &
                  testsuite_hamiltonian(), &
                  testsuite_interactions(), &
                  testsuite_ions(), &
                  testsuite_math(), &
                  testsuite_maxwell(), &
                  testsuite_mesh(), &
                  testsuite_opt_control(), &
                  testsuite_output(), &
                  testsuite_photons(), &
                  testsuite_poisson(), &
                  testsuite_scf(), &
                  testsuite_species(), &
                  testsuite_states(), &
                  testsuite_sternheimer(), &
                  testsuite_td()&
          ]))

end program test_suite
