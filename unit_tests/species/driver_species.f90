 !> @brief Exposes all unit tests associated with the species folder to fortuno cmd line app
module driver_species_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_species

contains

    function testsuite_species() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_species

end module driver_species_oct_m