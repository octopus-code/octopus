 !> @brief Exposes all unit tests associated with the multisystem folder to fortuno cmd line app
module driver_multisystem_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_multisystem

contains

    function testsuite_multisystem() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_multisystem

end module driver_multisystem_oct_m