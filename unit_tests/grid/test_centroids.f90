module testsuite_centroids_oct_m
    use, intrinsic :: iso_fortran_env, only: real64, int64
    use fortuno_interface_m

    use electron_space_oct_m,  only: electron_space_t
    use grid_oct_m,            only: grid_t, grid_end
    use ions_oct_m,            only: ions_t
    use mesh_oct_m,            only: mesh_x_global
    use mpi_oct_m,             only: mpi_world
    use multicomm_oct_m,       only: multicomm_t, multicomm_end
    use namespace_oct_m,       only: namespace_t
    use parser_oct_m,          only: parser_initialize_symbol_table, parse_end, parse_variable, parse_input_string

    use mock_oct_m
    use setup_teardown_oct_m

    use centroids_oct_m,       only: centroids_t

    implicit none
    private
    public :: testsuite_centroids

    character(len=22) :: module_name = "testsuite_centroids"
    type(namespace_t) :: namespace
    character(len=:), allocatable  :: parser_log

contains

    !> @brief Returns a suite instance, wrapped as test_item
    function testsuite_centroids() result(res)
        type(test_item), allocatable :: res

        res = suite("grid/centroids", test_list([&
                        test_case("test_centroids_class", test_centroids_class) &
                ]))

    end function testsuite_centroids


    !> @brief Helper routine to find vectors of a 2D array in a larger array 2D
    !!
    !! Suboptimal implementation, but fine for testing purposes
    integer function n_matches(a, b)
        real(real64),  intent(in) :: a(:, :), b(:, :)

        integer                   :: i, j, cnt, ndim
        real(real64)              :: diff
        real(real64), parameter   :: thres = 1.0e-8_real64 !< Threshold for two vectors being the same
        real(real64), allocatable :: v_a(:)

        ndim = size(a, 1)
        call check(size(b, 1) == ndim, msg="First dim of two arrays being compared must be consistent")
        allocate(v_a(ndim))

        cnt = 0
        do i = 1, size(a, 2)
            v_a = a(:, i)
            do j = 1, size(b, 2)
                diff = sum(abs(v_a(:) - b(:, j))) / real(ndim, real64)
                if (diff < thres) then
                    cnt = cnt + 1
                    exit
                endif
            enddo
        enddo
        n_matches = cnt

    end function n_matches


    !> @brief Test methods of the centroids class
    subroutine test_centroids_class()
        ! Space
        type(electron_space_t)    :: space
        integer, parameter        :: ndim = 3
        integer, parameter        :: nperiodic = 0
        integer, parameter        :: nspin = 1
        ! Grid
        type(ions_t), pointer     :: ion
        type(grid_t)              :: gr
        type(multicomm_t)         :: mc
        real(real64)              :: spacings(ndim), lsize(ndim)
        ! Centroids
        integer, parameter        :: n_centroids_global = 12               !< Total number of centroids
        real(real64)              :: c_positions(ndim, n_centroids_global) !< All centroid positions across the system
        type(centroids_t)         :: centroids
        real(real64), allocatable :: positions(:, :)                       !< Centroid positions on local communicator/domain
        integer(int64)            :: ipg                                   !< Global grid index
        integer                   :: ic

        if (.not. allocated(parser_log)) call test_module_setup(module_name, namespace, parser_log)
        call parser_initialize_symbol_table(parser_log)

        ! Construct a grid
        call mock_space(namespace, ndim, nperiodic, nspin, space)
        ion => mock_dummy_ion(namespace)

        spacings = [0.25, 0.25, 0.25]
        lsize = [1.0, 1.0, 1.0]
        call mock_rectangular_grid(namespace, space, ion, spacings, lsize, mc, gr)

        ! centroids declared row-wise, but will be stored columnwise upon reshaping
        ! Points have been selected by running with 4 processes, and arbitrarily choosing
        ! 3 points per process (domain)
        c_positions = reshape(&
            [-0.75_real64,  0.25_real64, -0.75_real64, &
              0.75_real64,  0.75_real64, -0.75_real64, &  
              0.50_real64,  0.00_real64,  0.00_real64, &
             -1.00_real64, -1.00_real64, -1.00_real64, &
             -1.00_real64, -0.75_real64, -0.25_real64, &   
              0.00_real64, -1.00_real64,  0.00_real64, &
              1.00_real64,  0.50_real64,  0.00_real64, &
              0.25_real64,  0.25_real64,  0.25_real64, &  
              1.00_real64,  1.00_real64,  0.75_real64, & 
              1.00_real64, -0.50_real64,  0.25_real64, &    
             -0.75_real64, -1.00_real64,  0.50_real64, & 
              0.50_real64, -0.25_real64,  1.00_real64  &
             ], shape=[ndim, n_centroids_global])

        call centroids%init(gr, c_positions)

        call check(centroids%npoints_global() == n_centroids_global, &
            & "Global number of centroids should be consistent, independent of N MPI processes")

        ! Centroids per domain varies with the number of domains used.
        ! Note, these checks could fail if the 'MeshPartition = rcb' default for N MPI <= 4 
        ! changes, but setting this default is done in the code, not from docs, and the option
        ! is not saved, so it is impossible to assert in a test. 
        if (gr%mpi_grp%size == 1) then
            call check(centroids%npoints() == n_centroids_global)

        elseif (gr%mpi_grp%size == 2) then
            if (gr%mpi_grp%rank == 0) call check(centroids%npoints() == 6)
            if (gr%mpi_grp%rank == 1) call check(centroids%npoints() == 6)

        elseif (gr%mpi_grp%size == 3) then
            if (gr%mpi_grp%rank == 0) call check(centroids%npoints() == 3)
            if (gr%mpi_grp%rank == 1) call check(centroids%npoints() == 5)
            if (gr%mpi_grp%rank == 2) call check(centroids%npoints() == 4)

        elseif (gr%mpi_grp%size == 4) then
            if (gr%mpi_grp%rank == 0) call check(centroids%npoints() == 3)
            if (gr%mpi_grp%rank == 1) call check(centroids%npoints() == 3)
            if (gr%mpi_grp%rank == 2) call check(centroids%npoints() == 3)
            if (gr%mpi_grp%rank == 3) call check(centroids%npoints() == 3)
        endif

        ! Get centroid positions local to this domain/process
        allocate(positions(ndim, centroids%npoints()))
        do ic = 1, centroids%npoints()
            positions(:, ic) = centroids%get_local_position(gr, ic)
        enddo
        
        call check(n_matches(positions, c_positions) == centroids%npoints(), &
            msg='Expect all centroid positions local to this domain to present' // &
            & ' in the global array of centroid positions')

        ! Test mapping from ic -> icg -> ipg results in positions that are consistent
        ! with those used to initialise the object
        positions = 0.0_real64
        do ic = 1, centroids%npoints()
            ipg = centroids%to_global_mesh_index(ic)
            positions(:, ic) = mesh_x_global(gr, ipg)
        enddo
        
        call check(n_matches(positions, c_positions) == centroids%npoints(), &
            msg='Expect local centroid index to correctly map to th global grid index, '//&
            & 'and give consistent centroid positions')

    end subroutine test_centroids_class


end module testsuite_centroids_oct_m
