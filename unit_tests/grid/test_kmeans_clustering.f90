!! Copyright (C) 2024 Alex Buccheri

!> Unit test routines from the `kmeans_clustering` module
!!
!! @note Setup and teardown.
!! The `test_module_setup` needs to be called once in the execution of this test module.
!! To achieve this, it is guarded with an if-statement, and present in each test case.
!! Being present in all test cases is not required when running the unit tests with ctest, 
!! however it is necessary if one wishes to directly execute the test binary and select a 
!! specific test case to run, using fortuno''s CLI:
!! ```
!!  cd <build>/test
!!  mpirun -np 2 test_suite grid/kmeans_clustering/test_weighted_kmeans_in_2d
!! ```
!! Running specific test cases is particularly useful for debugging.
!!
!! The teardown/finalise should be the final test_case of the test_suite instance.
!! See `function testsuite_kmeans_clustering()`.
!! Correct freeing of resources is not so important if running a single test case for
!! debugging, and there''s no clear mechnanism to determine if finalising should be called
!! in this instance.
!
module testsuite_kmeans_clustering_oct_m
    use, intrinsic :: iso_fortran_env, only: real64, int64
    use fortuno_interface_m

    use cube_oct_m,            only: cube_t, cube_init, cube_end, cube_init_cube_map
    use cube_function_oct_m,   only: cube_function_t, dcube_function_alloc_RS, dcube_function_free_RS, dmesh_to_cube
    use electron_space_oct_m,  only: electron_space_t
    use global_oct_m,          only: M_Pi
    use grid_oct_m,            only: grid_t, grid_end
    use ions_oct_m,            only: ions_t
    use mpi_oct_m,             only: mpi_world
    use multicomm_oct_m,       only: multicomm_t, multicomm_end
    use namespace_oct_m,       only: namespace_t
    use parser_oct_m,          only: parser_initialize_symbol_table, parse_end

    use setup_teardown_oct_m
    use mock_oct_m
    use kmeans_clustering_oct_m

    implicit none
    private
    public :: testsuite_kmeans_clustering

    character(len=22) :: module_name = "test_kmeans_clustering"
    type(namespace_t) :: namespace
    character(len=:), allocatable  :: parser_log

contains

    !> Returns a suite instance, wrapped as test_item
    function testsuite_kmeans_clustering() result(res)
        type(test_item), allocatable :: res

        res = suite("grid/kmeans_clustering", test_list([&
                        test_case("test_assign_points_to_centroids_finite_bc", test_assign_points_to_centroids_finite_bc), &
                        test_case("test_update_centroids", test_update_centroids), &
                        test_case("test_weighted_kmeans_in_2d", test_weighted_kmeans_in_2d) &
                ]))

    end function testsuite_kmeans_clustering


    !> @brief Test assignment of grid points to clusters, for a 2D regular grid
    !! with finite BCs.
    !! Only tests serial, not domain decomposition.
    subroutine test_assign_points_to_centroids_finite_bc()
        ! Space
        type(electron_space_t)    :: space
        integer, parameter        :: ndim = 2
        integer, parameter        :: nperiodic = 0
        integer, parameter        :: nspin = 1
        ! Grid
        type(ions_t), pointer     :: ion
        type(grid_t)              :: gr
        type(multicomm_t)         :: mc
        real(real64)              :: spacing(ndim), lsize(ndim)
        ! Centroids
        integer,      allocatable :: ip_to_ic(:)      !< map grid indices to centroid indices
        real(real64), allocatable :: centroids(:, :)

        if (.not. allocated(parser_log)) call test_module_setup(module_name, namespace, parser_log)

        call parser_initialize_symbol_table(parser_log)

        call mock_space(namespace, ndim, nperiodic, nspin, space)
        ion => mock_dummy_ion(namespace)

        spacing = [0.5, 0.5]
        lsize = [1.0, 1.0]
        call mock_rectangular_grid(namespace, space, ion, spacing, lsize, mc, gr)

        allocate(centroids(space%dim, 2))
        centroids = reshape([0.0_real64, -0.5_real64,  &
                             0.0_real64,  0.5_real64], & 
                             shape=[space%dim, 2])

        allocate(ip_to_ic(gr%np))
        call assign_points_to_centroids_finite_bc(gr, centroids, ip_to_ic)

        ! All points along y=0 (5 points) are equidistant to the two centroids.
        ! Because the algorithm updates when current distance < stored minimum, 
        ! equidistant points get assigned to the the lowest-indexed centroid.
        call check(gr%np == 25, msg="Total number of grid points")
        call check(all(ip_to_ic(1:15) == 1), msg="Grid indices assigned to centroid 1")
        call check(all(ip_to_ic(16:25) == 2), msg="Grid indices assigned to centroid 2")

        deallocate(ion)
        call grid_end(gr)
        call multicomm_end(mc)
        call parse_end()

    end subroutine test_assign_points_to_centroids_finite_bc


    !> @brief Test updating centroids for a 2D regular grid, with finite BCs
    !! Only tests serial, not domain decomposition.
    subroutine test_update_centroids()
        ! Space
        type(electron_space_t)    :: space
        integer, parameter        :: ndim = 2
        integer, parameter        :: nperiodic = 0
        integer, parameter        :: nspin = 1
        ! Grid
        integer                   :: ip
        type(ions_t), pointer     :: ion
        type(grid_t)              :: gr
        type(multicomm_t)         :: mc
        real(real64)              :: spacing(ndim), lsize(ndim)
        ! Centroids
        integer                   :: ref_ip_to_ic(25)
        integer,      allocatable :: ip_to_ic(:)      !< map grid indices to centroid indices
        real(real64), allocatable :: ref_centroids(:, :), centroids(:, :), uniform_weighting(:)
        
        if (.not. allocated(parser_log)) call test_module_setup(module_name, namespace, parser_log)

        call parser_initialize_symbol_table(parser_log)

        call mock_space(namespace, ndim, nperiodic, nspin, space)
        ion => mock_dummy_ion(namespace)

        spacing = [0.5, 0.5]
        lsize = [1.0, 1.0]
        call mock_rectangular_grid(namespace, space, ion, spacing, lsize, mc, gr)
        call check(gr%np == 25, msg="Total number of grid points")

        allocate(centroids(space%dim, 2), ref_centroids(space%dim, 2))
        
        ! Initial centroids on the 2D grid
        ! .  .  .  .  .
        ! .  .  .  .  .
        ! .  o  .  o  .
        ! .  .  .  .  .
        ! .  .  .  .  .
        centroids = reshape([-0.5_real64,  0.0_real64, &
                              0.5_real64,  0.0_real64], &          
                            shape=[space%dim, 2])

        ref_centroids = reshape([-0.5_real64,  0.0_real64, &
                                  0.75_real64,  0.0_real64], &          
                          shape=[space%dim, 2])            

        allocate(ip_to_ic(gr%np))
        call assign_points_to_centroids_finite_bc(gr, centroids, ip_to_ic)

        ! Point assignment
        ! centroid 1  centroid 2
        ! -1 0.5 0      0.5 1
        ! ---------------------
        !  .  .  .       .  .
        !  .  .  .       .  .
        !  .  o  .       o  .
        !  .  .  .       .  .
        !  .  .  .       .  .
        ref_ip_to_ic = [1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2]
        call check(all(ref_ip_to_ic == ref_ip_to_ic), msg='Assignment of grid indices to centroids')

        ! Ensures that the weight has no contribution 
        allocate(uniform_weighting(gr%np))
        do ip = 1, gr%np
            uniform_weighting(ip) = 1._real64 / real(gr%np, real64)
        enddo

        call update_centroids(gr, uniform_weighting, ip_to_ic, centroids)

        call check(count(ip_to_ic == 1) == 15, "Points assigned to centroid 1")
        call check(all_close(centroids(:, 1), [-0.5_real64,  0.0_real64]), msg = 'Centroid 1 unchanged')

        ! As less points have been assigned to centroid 2, for (x >= 0.5, y), its centre
        ! has now changed to half way between 0.5 and Lsize(1)
        call check(count(ip_to_ic == 2) == 10, "Points assigned to centroid 2")
        call check(all_close(centroids(:, 2), [0.75_real64,  0.0_real64]), msg = 'Centroid 2 changed')

        deallocate(ion)
        call grid_end(gr)
        call multicomm_end(mc)
        call parse_end()

    end subroutine test_update_centroids


    !> @brief Evaluate a Gaussian function on the point of the mesh.
    real(real64) function gaussian_function(x, mean, sigma)
        real(real64),  intent(in)  :: x(:)           !< Point on mesh
        real(real64),  intent(in)  :: mean(:)        !< Centre of the Gaussian
        real(real64),  intent(in)  :: sigma(:)       !< Width of the Gaussian

        real(real64) :: n_dim, n, norm, exponent

        n_dim = real(size(x), real64)
        n =  0.5_real64 * n_dim
        norm = 1._real64  / (product(sigma) * (2._real64 * M_Pi)**n)
        exponent = sum((x - mean)**2 / sigma**2)
        gaussian_function =  norm * exp(-0.5_real64 * exponent)

    end function gaussian_function


    !> @brief Test applicationf of weighted k-means on 2D regular grid, 
    !! with a weight function defined by gaussians - one in each quadrant of the grid,
    !! for finite BCs.
    !!
    !! The centroids should iteratively converge to the weight function maxima.
    !! Tested up to 4 MPI processes.
    !! When running without domain decomposition, one can set visualise = .true. and plot the result.
    !! @note
    !! Declaring the centroid coordinates with or without `_real64` can introduce enough numerical
    !! difference such that some of the final centroids shift. This is assumed to be due to the 
    !! discretisation of the final points, in conjunction with a relatively coarse grid.
    subroutine test_weighted_kmeans_in_2d()
        ! Space
        type(electron_space_t)    :: space
        integer, parameter :: ndim = 2
        integer, parameter :: nperiodic = 0
        integer, parameter :: nspin = 1
        ! Grid
        type(ions_t), pointer     :: ion
        type(grid_t)              :: gr
        type(multicomm_t)         :: mc
        real(real64)              :: spacing(3), lsize(3)
        integer                   :: np
        ! Gaussian
        integer, parameter        :: n_gauss = 4
        real(real64)              :: mean(ndim, n_gauss), sigma(ndim)
        real(real64), allocatable :: total_gaussian(:)
        ! Centroids
        integer                   :: n_centroid
        real(real64), allocatable :: centroids(:, :), ref_centroids(:, :)
        ! Output
        type(cube_t)              :: cube
        type(cube_function_t)     :: total_gaussian_cf, cube_to_ip
        real(real64), allocatable :: index_map(:)
        logical                   :: visualise = .false.

        integer      :: ip, i, ix, iy, nx
        real(real64) :: mean_i(ndim), x(ndim), average_centre(ndim), gauss
        character(:), allocatable :: inp_str_grid

        if (.not. allocated(parser_log)) call test_module_setup(module_name, namespace, parser_log)
        call parser_initialize_symbol_table(parser_log)

        ! Grid settings
        spacing = [0.05, 0.05, 0.05]
        lsize = [1.0, 1.0, 1.0]

        call mock_space(namespace, ndim, nperiodic, nspin, space)
        ion => mock_dummy_ion(namespace)

        inp_str_grid = &
        '%Spacing'                   // nl // &
        block_line_str(spacing)      // nl // &
        '%'                          // nl // &
        'BoxShape = parallelepiped'  // nl // &
        '%Lsize'                     // nl // &
        block_line_str(lsize)        // nl // &
        '%'

        ! Use domain parallelisation if testing with more than one process
        if (mpi_world%size > 1) then
            inp_str_grid = &
            'ParDomains = auto'          // nl // &
            'RestartWrite = false'       // nl // &
            'MeshPartition = rcb'        // nl // inp_str_grid
        endif

        call mock_grid_given_input(namespace, space, ion, mc, inp_str_grid, gr)

        ! Gaussians centred in each quadrant of the grid
        ! total_gaussian defines the weight function
        sigma = [0.15_real64, 0.15_real64]
        mean = reshape([-0.5_real64,  0.5_real64, &
                         0.5_real64, -0.5_real64, &
                         0.5_real64,  0.5_real64, &
                        -0.5_real64, -0.5_real64], [ndim, n_gauss])

        np = gr%np
        allocate(total_gaussian(np))

        do ip = 1, np
            x = gr%x(ip, :)
            gauss = gaussian_function(x, mean(:, 1), sigma)
            total_gaussian(ip) = gauss
        enddo

        do i = 2, n_gauss
            mean_i = mean(:, i)
            do ip = 1, np
                x = gr%x(ip, :)
                gauss = gaussian_function(x, mean_i, sigma)
                total_gaussian(ip) = total_gaussian(ip) + gauss
            enddo
        enddo

        ! Assign initial centroids
        n_centroid = 25
        allocate(centroids(ndim, n_centroid))

        ! Points chosen randomly, from these global indices of the grid
        ! indices = [2, 14, 102, 153, 217, 236, 319, 342, 401, 425, 512, 526, 610, &
        !           687, 703, 795, 840, 876, 923, 961, 1012, 1058, 1109, 1145, 1165]
        centroids = reshape([&
        -0.950_real64, -1.000_real64, &   
        -0.350_real64, -1.000_real64, &    
        -0.050_real64, -0.900_real64, &    
         0.450_real64, -0.850_real64, &    
        -0.450_real64, -0.750_real64, &    
         0.500_real64, -0.750_real64, &    
         0.550_real64, -0.650_real64, &    
        -0.350_real64, -0.600_real64, &    
         0.550_real64, -0.550_real64, &    
        -0.300_real64, -0.500_real64, &    
        -0.050_real64, -0.400_real64, &    
         0.650_real64, -0.400_real64, &    
         0.750_real64, -0.300_real64, &    
         0.500_real64, -0.200_real64, &    
        -0.750_real64, -0.150_real64, &    
        -0.250_real64, -0.050_real64, &
        -0.050_real64,  0.000_real64, &    
        -0.300_real64,  0.050_real64, &
         0.000_real64,  0.100_real64, &    
        -0.150_real64,  0.150_real64, &    
         0.350_real64,  0.200_real64, &    
         0.600_real64,  0.250_real64, &    
        -0.950_real64,  0.350_real64, &    
         0.850_real64,  0.350_real64, &    
        -0.200_real64,  0.400_real64], &
        shape=[ndim, n_centroid])

        ! Output initial centroids
        if (visualise) then
            do i = 1, n_centroid
                write(101, *) centroids(:, i), 0._real64
            enddo
        endif

        call weighted_kmeans(space, gr, total_gaussian, centroids, discretize=.true.)
    
        ref_centroids = reshape([&
        -0.750_real64, -0.550_real64,  &
        -0.400_real64, -0.750_real64,  &
        -0.250_real64, -0.550_real64,  &
         0.450_real64, -0.800_real64,  &
        -0.600_real64, -0.650_real64,  &
         0.400_real64, -0.600_real64,  &
         0.650_real64, -0.650_real64,  &
        -0.450_real64, -0.550_real64,  &
         0.450_real64, -0.450_real64,  &
        -0.350_real64, -0.400_real64,  &
         0.250_real64, -0.550_real64,  &
         0.600_real64, -0.450_real64,  &
         0.750_real64, -0.400_real64,  &
         0.550_real64, -0.300_real64,  &
        -0.600_real64, -0.400_real64,  &
        -0.500_real64, -0.250_real64,  &
         0.300_real64, -0.300_real64,  &
        -0.550_real64,  0.350_real64,  &
         0.350_real64,  0.350_real64,  &
        -0.350_real64,  0.400_real64,  &
         0.350_real64,  0.600_real64,  &
         0.600_real64,  0.400_real64,  &
        -0.650_real64,  0.550_real64,  &
         0.600_real64,  0.650_real64,  &
        -0.450_real64,  0.650_real64], &
        shape=[ndim, n_centroid])

        call check(all_close(centroids, ref_centroids),  msg="Compare final centroids to tabulated reference")

        ! centroids should be approximately centred around the four gaussian peaks
        ! where the function is at maxima, as specified by the means above.
        ! Note, the distribution of the final centoids is not even due to the 
        ! choice of centroid initial guess.
        average_centre = sum(centroids(:, [18, 20, 23, 25]), dim=2) / 4._real64
        call check(all_close(average_centre, mean(:, 1), atol=0.02_real64),  msg="Top left Gaussian func peak")

        average_centre = sum(centroids(:, [4, 6, 7, 9, 11, 12, 13, 14]), dim=2) / 8._real64
        call check(all_close(average_centre, mean(:, 2), atol=0.026_real64),  msg="Bottom right Gaussian func peak")

        average_centre = sum(centroids(:, [19, 21, 22, 24]), dim=2) / 4._real64
        call check(all_close(average_centre, mean(:, 3), atol=0.026_real64),  msg="Top right Gaussian func peak")

        average_centre = sum(centroids(:, [1, 2, 3, 5, 8, 10, 15, 16]), dim=2) / 8._real64
        call check(all_close(average_centre, mean(:, 4), atol=0.02_real64),  msg="Bottom left Gaussian func peak")

        if (visualise) then
            ! Output Gaussians for gnuplot
            call cube_init(cube, gr%idx%ll, namespace, space, gr%spacing, gr%coord_system)
            call cube_init_cube_map(cube, gr)
            call check(.not. cube%parallel_in_domains,  &
                msg="Index mapping used in this test will not work with domain decomposition")

            ! Mapping of mesh indices to cube indices
            index_map = [(real(i, real64), i=1, gr%np)]
            call dcube_function_alloc_RS(cube, cube_to_ip)
            call dmesh_to_cube(gr, index_map, cube, cube_to_ip)

            call check(size(cube_to_ip%drs, 1) == 41, msg='sqrt(np)')
            call check(size(cube_to_ip%drs, 2) == 41, msg='sqrt(np)')
            call check(size(cube_to_ip%drs, 3) == 1, msg='Mesh has no thickness')

            ! Map gaussian from mesh to cube
            call dcube_function_alloc_RS(cube, total_gaussian_cf)
            call dmesh_to_cube(gr, total_gaussian, cube, total_gaussian_cf)

            ! Visualise with gnuplot as "splot 'fort.100' u 1:2:3 w lp"
            nx = size(cube_to_ip%drs, 1)
            do iy = 1 , size(cube_to_ip%drs, 2)
                do ix = 1, nx
                    ip = int(cube_to_ip%dRS(ix, iy, 1))
                    write(100, *) gr%x(ip, :), total_gaussian_cf%dRS(ix, iy, 1)
                enddo
                    write(100, *)
            enddo

            ! Output final centroids for plotting over Gaussians
            do i = 1, n_centroid
                write(102, *) centroids(:, i), 0._real64
            enddo

            call dcube_function_free_RS(cube, cube_to_ip)
            call dcube_function_free_RS(cube, total_gaussian_cf)
            call cube_end(cube)

            ! Plot script:
            !splot 'fort.100' u 1:2:3 w pm3d title 'Gaussians on mesh'
            !replot 'fort.101' u 1:2:3 w p ps 4 lw 2 title'Initial centroids'
            !replot 'fort.102' u 1:2:3 w p ps 4 lw 2 title'Final centroids'
        endif
    
        ! Free data
        deallocate(ion)
        call grid_end(gr)
        call multicomm_end(mc)
        ! Free the symbol table
        call parse_end()

    end subroutine test_weighted_kmeans_in_2d

end module testsuite_kmeans_clustering_oct_m
