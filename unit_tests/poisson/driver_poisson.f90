 !> @brief Exposes all unit tests associated with the poisson folder to fortuno cmd line app
module driver_poisson_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_poisson

contains

    function testsuite_poisson() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_poisson

end module driver_poisson_oct_m