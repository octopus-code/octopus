 !> @brief Exposes all unit tests associated with the states folder to fortuno cmd line app
module driver_states_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_states

contains

    function testsuite_states() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_states

end module driver_states_oct_m