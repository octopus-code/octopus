!! Copyright (C) 2024 Alex Buccheri

!> Unit test routines from the `mesh` module
module testsuite_mesh_oct_m
    use, intrinsic :: iso_fortran_env, only: real64, int64
    use fortuno_interface_m

    use electron_space_oct_m,  only: electron_space_t
    use grid_oct_m,            only: grid_t, grid_end
    use ions_oct_m,            only: ions_t
    use mpi_oct_m,             only: mpi_world
    use multicomm_oct_m,       only: multicomm_t, multicomm_end
    use namespace_oct_m,       only: namespace_t
    use parser_oct_m,          only: parser_initialize_symbol_table, parse_end

    use setup_teardown_oct_m
    use mock_oct_m
    use mesh_oct_m

    implicit none
    private
    public :: testsuite_mesh_funcs

    character(len=22) :: module_name = "test_mesh"
    type(namespace_t) :: namespace
    character(len=:), allocatable  :: parser_log

contains

    !> Returns a suite instance, wrapped as test_item
    function testsuite_mesh_funcs() result(res)
        type(test_item), allocatable :: res

        res = suite("grid/mesh", test_list([&
                test_case("test_mesh_discretize_values_to_mesh", test_mesh_discretize_values_to_mesh) &
                ]))

    end function testsuite_mesh_funcs

    !> @brief Test discretization of values to discrete points on the mesh.
    !! SERIAL ONLY.
    !! 
    !! @note
    !! To test when using domain decomposition, the checks on gr%np and gr%x would need to be
    !! guarded with if statements (single process) or replaced with something more general
    !! as all mesh/grid quantities are process-local.
    !! Assignment of grid points with gr_indices would also have to be replaced with hard-coded
    !! grid points.
    !! Alternatively, one could rewrite the test such that only the final values are checked,
    !! by confirming each one exists in gr%x.
    subroutine test_mesh_discretize_values_to_mesh()
        type(electron_space_t)    :: space
        type(ions_t), pointer     :: ion
        type(grid_t)              :: gr
        type(multicomm_t)         :: mc
        real(real64)              :: spacing(3), lsize(3)
        integer                   :: n_values, i
        real(real64), allocatable :: values(:, :), mesh_points(:, :)
        integer,      allocatable :: gr_indices(:)
        real(real64) :: noise

        if (.not. allocated(parser_log)) call test_module_setup(module_name, namespace, parser_log)
        call parser_initialize_symbol_table(parser_log)

        ! Space
        space = electron_space_t(namespace)
        call check(space%ispin == 1, msg='N spin channels')
        call check(space%dim == 3, msg='Default to 3D system')
        call check(space%periodic_dim == 0, msg='Periodic dimensions - defaults to finite system')

        ! Grid
        spacing = [0.5, 0.5, 0.5]
        lsize = [1.0, 1.0, 1.0]
        ion => mock_dummy_ion(namespace)

        call mock_rectangular_grid(namespace, space, ion, spacing, lsize, mc, gr)

        ! Test will break if parallel in domains, for reasons documented above
        if (gr%parallel_in_domains) then
            deallocate(ion)
            call grid_end(gr)
            call multicomm_end(mc)
            call parse_end()
            return
        endif

        call check(gr%np == 125, msg='5^3 i.e. 5 points per dimension -1.0, -0.50, 0., 0.50, 1.0')
        call check(all_close(gr%x(1, :), [-1._real64, -1._real64, -1._real64]), &
            msg='First grid point')
        call check(all_close(gr%x(gr%np, :), [1._real64, 1._real64, 1._real64]), &
            msg='Last grid point')

        ! Set values equal to some arbitrary grid points plus fixed noise
        n_values = 5
        allocate(values(space%dim, n_values), mesh_points(space%dim, n_values))
        allocate(gr_indices(n_values))
        gr_indices = [1, 24, 43, 89, 111]
        noise = 0.01_real64

        do i = 1, n_values
            mesh_points(:, i) = gr%x(gr_indices(i), :)
            values(:, i) = mesh_points(:, i) + noise
        enddo

        call mesh_discretize_values_to_mesh(gr, values)
        call check(all_close(mesh_points, values), msg='Compare values to discrete grid points')

        ! Free data
        deallocate(ion)
        call grid_end(gr)
        call multicomm_end(mc)
        ! Free the symbol table
        call parse_end()

    end subroutine test_mesh_discretize_values_to_mesh


end module testsuite_mesh_oct_m
