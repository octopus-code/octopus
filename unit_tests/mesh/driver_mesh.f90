!> @brief Exposes all unit tests associated with the mesh functions
module driver_mesh_oct_m
    use fortuno_interface_m, only: test_list

    use testsuite_mesh_oct_m

    implicit none
    private
    public :: testsuite_mesh

contains

    function testsuite_mesh() result(tests)
        type(test_list) :: tests

        tests = test_list([&
                testsuite_mesh_funcs()&
                ])

    end function testsuite_mesh

end module driver_mesh_oct_m