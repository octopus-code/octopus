 !> @brief Exposes all unit tests associated with the box folder to fortuno cmd line app
module driver_boxes_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_boxes

contains

    function testsuite_boxes() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_boxes

end module driver_boxes_oct_m
