 !> @brief Exposes all unit tests associated with the output folder to fortuno cmd line app
module driver_output_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_output

contains

    function testsuite_output() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_output

end module driver_output_oct_m