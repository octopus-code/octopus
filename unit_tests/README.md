# Octopus Unit Testing with Fortuno

<!-- TOC -->

- [Octopus Unit Testing with Fortuno](#octopus-unit-testing-with-fortuno)
  - [Introduction](#introduction)
  - [Test Structure and Registering a Unit Test](#test-structure-and-registering-a-unit-test)
    - [Test Module](#test-module)
      - [Inclusion in CMakeLists](#inclusion-in-cmakelists)
    - [Test Driver Module](#test-driver-module)
  - [Parsing Inputs](#parsing-inputs)

<!-- TOC -->

## Introduction

Octopus unit testing is based on the [Fortuno](https://github.com/fortuno-repos/fortuno) test framework, which is 
automatically fetched and installed by cmake at build time. Note that Fortuno requires gfortran >= 12.0.0 to be
compiled. 

Unit tests are disabled by default, but can be enabled with the `-DOCTOPUS_UNIT_TESTS=On` configure option.

Unit tests can be run via `ctest`:

```shell
# List all available tests (including regression)
ctest --test-dir ./build -N
# Run all unit tests via labels
ctest --test-dir ./build -L unit_tests
# Execute the specific test `basic/sort`
ctest --test-dir ./build -R basic/sort
```

where `./build` should be replaced with the build directory name.`ctest` will not write to stdout by default. 
This can be changed with the `--verbose` flag. 

The unit tests can also equivalently be run by directly executing the test binary:

```shell
# List available tests 
./build/unit_tests/test-suite --list
# Execute the test `basic/sort`
./build/unit_tests/test-suite basic/sort
```

where `--list` lists all registered tests.

We also note that Octopus has an existing test driver file, `src/main/test.F90`, in which many tests are already
present. These tests require the presence of an `inp` file, and assertions must be done via the testsuite perl app.
As such, we recommend this approach for regression, integration and performance testing of run modes, rather than 
unit tests of single subroutines. `src/main/test.F90` also allows for repeating tests with different inputs, which 
is useful for inspecting scaling behavior.

## Test Structure and Registering a Unit Test

The structure of `unit_tests` mirrors that of the `src`. Each subfolder of the `src` should have the same named folder in
`unit_tests`. Within this folder, one should find tests for each module, following the convention `test_<module-name>.f90`,
and a `driver_<subfolder-name>.f90` to register these tests with fortuno. For example:

```console
$ tree ./
./
├── src
│   └── basic
│       └── sort.F90        # Octopus module
└── unit_tests
    ├── basic
    │   └── driver_basic.F90   # Module to register the tests from each test_module in unit_tests/basic
    │   └── test_sort.F90      # Module to test routines in src/basic/sort.f90
    └── fortuno_app.F90        # Collate subroutines from each driver file in the subfolders of unit_tests/ 
```

### Test Module

Test modules contain the routines that perform the unit tests, however they also require some boilerplate to
register each unit test as a "suite". A test module should always contain the boilerplate:

```fortran
module testsuite_MODULE_oct_m
  use fortuno_interface_m ! Octopus wrapper, exposing fortuno routines
  use my_oct_m    ! Module containing routines to test
  implicit none

  private
  public :: testsuite_MODULE  ! Function to expose to unit_tests/driver_SUBFOLDER.f90
    
contains

   !> @brief Returns a suite instance, wrapped as test_item
  function testsuite_MODULE() result(res)
    type(test_item), allocatable :: res
    res = suite("SUBFOLDER/MODULE",&  ! Specifies the unique test name, registered by fortuno and ctest
              test_list([&
                test_case("test_name", test_subroutine)&  ! TODO Add test cases to array arg of test_list
              ])
            )
  end function testsuite_MODULE
    
  subroutine test_subroutine()
    ! Test a routine from my_oct_m   
  end subroutine test_subroutine
    
end module testsuite_MODULE_oct_m
```

where `SUBFOLDER` and `MODULE` are replaced with the appropriate names. 

#### Inclusion in CMakeLists

If the test file is new, it should also be added to the `CMakeLists.txt`, in its respective folder:

```cmake
target_sources(Octopus_test_suite PRIVATE
    driver_subfolder.f90  # Driver to register module tests, and expose to fortuno
    test_module.f90       # New test module, defined in unit_tests/subfolder/test_module.f90
)
foreach (test IN ITEMS
    <subfolder/module>  # Test name, as defined in the test_suite("subfolder/module", items) registration of test_module.f90
)
```

### Test Driver Module

If `test_<module-name>.f90` is a new file, one also needs to add a couple of lines to the respective test driver,
located in the same folder, in order to expose this module's unit tests to fortuno. For example:

```fortran
module driver_SUBFOLDER_oct_m
    use fortuno_interface_m, only: test_list

    ! Test modules for modules defined in the SUBFOLDER folder
    use testsuite_MODULE1_oct_m
    use testsuite_MODULE2_oct_m

    implicit none
    private
    public :: testsuite_SUBFOLDER ! Exposed function, used by unit_tests/fortuno_app.f90
    !                               to register all tests from SUBFOLDER

contains

    function testsuite_SUBFOLDER() result(tests)
        type(test_list) :: tests

        tests = test_list([&
            testsuite_MODULE1(),&
            testsuite_MODULE2()&
        ])

    end function testsuite_SUBFOLDER

end module driver_SUBFOLDER_oct_m
```

Full, working examples of unit tests can be found in the `basic/` and `math/` folders.

## Parsing Inputs

Many Octopus classes contain a call to access a single instance of the parsed Octopus `inp`ut file. Developers can 
populate the input symbol table by defining the relevant inputs in a fortran character string, and parsing within a unit
test. This is demonstrated in [basic/test_parser.f90](basic/test_parser.f90).
