 !> @brief Exposes all unit tests associated with the interactions folder to fortuno cmd line app
module driver_interactions_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_interactions

contains

    function testsuite_interactions() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_interactions

end module driver_interactions_oct_m