!> @brief Setups and teardowns for unit testing Octopus.
module setup_teardown_oct_m
    use fortuno_interface_m,   only: global_comm

    use calc_mode_par_oct_m,   only: calc_mode_par
    use global_oct_m,          only: global_init, conf, M_Pi
    use io_oct_m,              only: io_init, io_mkdir
    use loct_oct_m,            only: loct_mkdir
    use messages_oct_m,        only: messages_init, messages_end
    use namespace_oct_m,       only: namespace_t
    use parser_oct_m,          only: parser_initialize_symbol_table, parse_end
    use unit_system_oct_m,     only: unit_system_init

    implicit none
    private

    public :: test_module_setup, test_module_teardown

contains

    !> @brief All setup required to initialise files and singletons required by Octopus
    !!
    !! namespace is only intent(out) because the compiler will complain at the declaration:
    !! ``` 
    !!  character(len=22) :: module_name = "test_kmeans_clustering"
    !!  type(namespace_t) :: namespace = namespace_t(module_name)
    !! ```
    subroutine test_module_setup(module_name, namespace, parser_log)
        character(len=*),              intent(in ) :: module_name !< Test module name
        type(namespace_t),             intent(out) :: namespace   !< namespace for unit test 
        character(len=:), allocatable, intent(out) :: parser_log  !< Oct parser log file name

        character(len=:), allocatable  :: logs_root       !< Root directory for placing unit test parser logs

        ! Initialise global conf and mpi_world singleton objects
        call global_init(communicator=global_comm())
        logs_root = trim(conf%build_dir) // '/unit_tests/unit_test_logs'
        parser_log = logs_root // '/' // trim(module_name) // '.log'
        call loct_mkdir(logs_root)

        ! Initialise global variables for use with io, without depending on input
        ! parser initialisation
        call io_init(defaults=.true.)

        ! Octopus wants to write log info to the namespace, hence give a valid path. 
        ! This gets written to `./conf%build_dir/unit_tests/<folder>/module_name`
        namespace = namespace_t(trim(module_name))
        call io_mkdir('.', namespace)

        ! Calls below depend on initialisation of the parser symbol table
        call parser_initialize_symbol_table(parser_log)

        ! Messages are used within numerous routines, so the singleton must be initialised
        ! Messages also writes files to the same directory as the parser log
        call messages_init(output_dir=logs_root)

        ! After main initialises, one also has to pay attention to initialisations in run
        call unit_system_init(namespace)

    end subroutine test_module_setup


    !> @brief All finalisation required by Octopus
    !! To be called once, at the end of the test module
    !!
    !! @note
    !! This does not want to finalize MPI, hence there is 
    !! no call to `mpi_mod_end()`
    subroutine test_module_teardown()

        call messages_end()
        call parse_end()

    end subroutine test_module_teardown


end module setup_teardown_oct_m
