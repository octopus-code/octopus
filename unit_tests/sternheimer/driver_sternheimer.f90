 !> @brief Exposes all unit tests associated with the sternheimer folder to fortuno cmd line app
module driver_sternheimer_oct_m
    use fortuno_interface_m, only: test_list

    implicit none
    private
    public :: testsuite_sternheimer

contains

    function testsuite_sternheimer() result(tests)
        type(test_list) :: tests

        tests = test_list()

    end function testsuite_sternheimer

end module driver_sternheimer_oct_m