#[==============================================================================================[
#                                  Libxc compatibility wrapper                                  #
]==============================================================================================]

# Upstream issues:
# https://gitlab.com/libxc/libxc/-/merge_requests/610
# https://gitlab.com/libxc/libxc/-/merge_requests/604

#[===[.md
# FindLibxc

Libxc compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

list(APPEND CMAKE_MESSAGE_CONTEXT FindLibxc)
include(Octopus)

# Libxc package files are broken on Debian (see: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1084870)
# Since this is causes an error in the processing of the CMake files themselves, configuration fails
# (as opposed to the package simply not being found). This is a workaround to avoid even going in that
# direction if we know it will cause an error (pkgConfig and building from source still considered)

set(LIBXC_SKIP_FIND_PACKAGE OFF)

# Do not second guess explicitly provided libxc
if(NOT Libxc_ROOT)

    find_path(LIBXC_CMAKE_CONFIG_PATH
              NAMES "LibxcConfig.cmake" "libxc-config.cmake"
              NO_CACHE
              PATHS
                    "/usr/local"
                    "/usr"
                    "/usr/share/cmake/Libxc"
                    "/usr/local/opt"
                    "/opt/homebrew/opt"
              PATH_SUFFIXES
                    "libxc" )

    if(LIBXC_CMAKE_CONFIG_PATH)

        find_file(LIBXC_CMAKE_FORTRAN_TARGETS_FILE
                  NAMES "LibxcTargets-Fortran.cmake"
                  NO_CACHE
                  HINTS
                        "${LIBXC_CMAKE_CONFIG_PATH}")

        # We found the overall configuration file but are missing the Fortran targets file
        # This is the specific issue we are working around
        if(NOT LIBXC_CMAKE_FORTRAN_TARGETS_FILE)
            set(LIBXC_SKIP_FIND_PACKAGE ON)
        endif()
    endif()

endif()

# Try each Libxc major version before trying pkg-config
# We need to do this to ensure the targets are not populated
# https://gitlab.kitware.com/cmake/cmake/-/issues/25527#note_1479281
# https://discourse.cmake.org/t/write-basic-package-version-file-and-version-range/10021

Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        # Skip libxc 5.0.0 specifically
        VERSIONS 7.0 6.0 5.1
        NAMES Libxc libxc
        PKG_MODULE_NAMES libxcf03
        SKIP_FIND_PACKAGE ${LIBXC_SKIP_FIND_PACKAGE} )

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(Libxc::xcf03 ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
    # For use with try_run, which cannot use an ALIAS
    set(LIBXC_TRY_RUN_TARGET PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
    
elseif (NOT TARGET Libxc::xcf03 AND TARGET xcf03)
    add_library(Libxc::xcf03 ALIAS xcf03)
    # For use with try_run, which cannot use an ALIAS
    set(LIBXC_TRY_RUN_TARGET xcf03)
endif ()

# Check if the found libXC package support FXC and KXC, respectively
if (CMAKE_VERSION LESS 3.25)
    try_run(libxc_has_fxc_run libxc_has_fxc_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
            SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libxc_fxc.F90
            LINK_LIBRARIES LIBXC_TRY_RUN_TARGET
    )
    try_run(libxc_has_kxc_run libxc_has_kxc_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
        SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libxc_kxc.F90
        LINK_LIBRARIES LIBXC_TRY_RUN_TARGET
    )
else ()
    # TODO: Move to SOURCE_FROM_VAR when moving to 3.25
    try_run(libxc_has_fxc_run libxc_has_fxc_compile
            SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libxc_fxc.F90
            LINK_LIBRARIES LIBXC_TRY_RUN_TARGET
            NO_CACHE
    )
    try_run(libxc_has_kxc_run libxc_has_kxc_compile
            SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_libxc_kxc.F90
            LINK_LIBRARIES LIBXC_TRY_RUN_TARGET
            NO_CACHE
    )
endif ()

# Set preprocessor variables used by Octopus
if (libxc_has_fxc_run EQUAL 0)
    message("Detected LibXC is compiled with support for second-order derivatives, FXC")
    set(HAVE_LIBXC_FXC 1)
endif ()
if (libxc_has_kxc_run EQUAL 0)
    message("Detected LibXC is compiled with support for third-order derivatives, KXC")
    set(HAVE_LIBXC_KXC 1)
endif ()

set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://gitlab.com/libxc/libxc
        DESCRIPTION "Library of exchange-correlation functionals for density-functional theory"
)

# Note that `VERISION_GREATER`, etc. treat `x` as `x.0.0`, `x.y` as `x.y.0`, etc.
if (Libxc_VERSION VERSION_GREATER_EQUAL 8)
    message(WARNING "Libxc version ${Libxc_VERSION} is not officially supported. Use it at your own risk.\n
	Please report any issues in the build and execution to the official repository so we can support it as quickly as possible.")
endif ()

list(POP_BACK CMAKE_MESSAGE_CONTEXT)
