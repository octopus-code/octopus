# _ci_intel:

Packages that need FFTW and are incompatible with MKL's default FFT are not available in the toolchains:
- nfft
- pfft
- pnfft
- libvdwxc
