#[==============================================================================================[
#                                  ELPA compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindELPA

ELPA compatibility module for Octopus

This file is specifically tuned for Octopus usage.

]===]

list(APPEND CMAKE_MESSAGE_CONTEXT FindELPA)
include(Octopus)

if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
	set(_required_arg REQUIRED)
endif ()

# OPTIONAL_COMPONENTS OpenMP doesn't make sense, because both the application
# and ELPA must be compiled with or without OpenMP.
if (OpenMP IN_LIST ${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS AND NOT ${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_OpenMP)
	message(FATAL_ERROR "${CMAKE_FIND_PACKAGE_NAME} does not support OPTIONAL_COMPONENTS OpenMP")
endif ()

if (NOT OCTOPUS_ScaLAPACK)
	if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
		message(FATAL_ERROR "ELPA is marked as REQUIRED but ScaLAPACK is not enabled")
	else ()
		message(STATUS "Skipping ELPA because ScaLAPACK is not enabled")
	endif ()
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()

# If OpenMP is requested look for `elpa_openmp`.
if (OpenMP IN_LIST ${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS)
	pkg_check_modules(${CMAKE_FIND_PACKAGE_NAME}
			${_required_arg}
			${${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY}
			IMPORTED_TARGET
			elpa_openmp)
	set(_elpa_target OpenMP)
	set(_elpa_name elpa_openmp)
else ()
	pkg_check_modules(${CMAKE_FIND_PACKAGE_NAME}
			${_required_arg}
			${${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY}
			IMPORTED_TARGET
			elpa)
	set(_elpa_target elpa)
	set(_elpa_name elpa)
endif ()

if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(_elpa_fcflags)
	pkg_get_variable(_elpa_fcflags ${_elpa_name} fcflags)
	if (_elpa_fcflags)
		target_compile_options(PkgConfig::${CMAKE_FIND_PACKAGE_NAME} INTERFACE
				$<$<COMPILE_LANGUAGE:Fortran>:${_elpa_fcflags}>)
	endif ()
	get_target_property(link_libs PkgConfig::${CMAKE_FIND_PACKAGE_NAME} INTERFACE_LINK_LIBRARIES)
	# Workaround for ELPA broken pkg-config: https://gitlab.mpcdf.mpg.de/elpa/elpa/-/issues/111
	# Remove everything from the LINK_LIBRARIES list that does not contain "elpa"
	list(FILTER link_libs INCLUDE REGEX ".*elpa.*")
	set_target_properties(PkgConfig::${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
			INTERFACE_LINK_LIBRARIES "${link_libs}"
	)
	add_library(ELPA::${_elpa_target} ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()

if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(HAVE_ELPA 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
		URL https://gitlab.mpcdf.mpg.de/elpa/elpa
		DESCRIPTION "Eigenvalue Solvers for Petaflop Apllications (ELPA)"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
