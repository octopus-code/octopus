list(APPEND CMAKE_MESSAGE_CONTEXT GPUawareMPI)
find_package(MPI QUIET COMPONENTS C)
try_compile(gpu_aware_mpi_compile ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/CMakeTmp
	SOURCES ${CMAKE_CURRENT_LIST_DIR}/test_gpu_aware_mpi.c
	COMPILE_DEFINITIONS -DHAVE_HIP=${HAVE_HIP} -DHAVE_CUDA=${HAVE_CUDA}
	LINK_LIBRARIES MPI::MPI_C)
if (gpu_aware_mpi_compile)
	message(STATUS "Found GPU-aware MPI: detected in <mpi-ext.h>")
	set(HAVE_CUDA_MPI 1)
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()
message(STATUS "Failed to compile test_gpu_aware_mpi.c, trying ucx_info")

# Check if we can find ucx_info
find_program(UCX_INFO NAMES ucx_info)
if (NOT UCX_INFO)
	message(STATUS "Failed to find ucx_info")
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()

# Run ucx_info -b and check return code
execute_process(
	COMMAND ${UCX_INFO} -b
	RESULT_VARIABLE UCX_INFO_RESULT
	OUTPUT_VARIABLE UCX_INFO_OUTPUT)
if(NOT UCX_INFO_RESULT EQUAL 0)
	message(STATUS "Failed to execute ucx_info")
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()

# Extract the line with UCX_CONFIGURE_FLAGS from the output
string(REGEX MATCH "UCX_CONFIGURE_FLAGS[^\n]*" UCX_CONFIGURE_FLAGS ${UCX_INFO_OUTPUT})
if(NOT UCX_CONFIGURE_FLAGS)
	message(STATUS "Failed to extract UCX_CONFIGURE_FLAGS from ucx_info")
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()

# Check if UCX_CONFIGURE_FLAGS contains --with-rocm or --with-cuda
if (HIP_PLATFORM STREQUAL amd)
	string(REGEX MATCH "--with-rocm" UCX_CONFIGURE_WITH ${UCX_CONFIGURE_FLAGS})
else ()
	string(REGEX MATCH "--with-cuda" UCX_CONFIGURE_WITH ${UCX_CONFIGURE_FLAGS})
endif ()
if (UCX_CONFIGURE_WITH LESS 0)
	message(STATUS "No --with-cuda or --with-rocm detected in UCX_CONFIGURE_FLAGS")
	list(POP_BACK CMAKE_MESSAGE_CONTEXT)
	return()
endif ()

# If we got this far, then ucx_info was configured --with-rocm or --with-cuda
message(STATUS "Found GPU-aware MPI: ucx_info has ${UCX_CONFIGURE_WITH}")
set(HAVE_CUDA_MPI 1)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
