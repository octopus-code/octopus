# TODO: Remove non-namespaced options when upstream accepts it
set(ENABLE_FORTRAN ON CACHE BOOL "Octopus: Overloaded" FORCE)
set(BUILD_TESTING OFF CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_ENABLE_FORTRAN ON CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_BUILD_TESTING OFF CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_IGNORE_DEPRECATED ON CACHE BOOL "Octopus: Overloaded" FORCE)
set(LIBXC_DISABLE_VXC OFF CACHE BOOL "Octopus: Overloaded. Compile first derivative code" FORCE)
set(LIBXC_DISABLE_FXC OFF CACHE BOOL "Octopus: Overloaded. Compile second derivative code" FORCE)
set(LIBXC_DISABLE_KXC OFF CACHE BOOL "Octopus: Overloaded. Compile third derivative code" FORCE)
# Not used by Octopus
set(LIBXC_DISABLE_LXC ON CACHE BOOL "Octopus: Overloaded. Compile fourth derivative code" FORCE)
