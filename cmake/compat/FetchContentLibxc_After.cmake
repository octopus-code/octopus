# Alex. FetchContent_GetProperties(Libxc) does not *appear* to set Libxc_POPULATED
# so use GLOB instead.
if(DEFINED FETCHCONTENT_SOURCE_DIR_LIBXC)
	file(GLOB third_party_libxc_files "${FETCHCONTENT_SOURCE_DIR_LIBXC}/*")
	list(LENGTH third_party_libxc_files n_libxc_files)
	if (${n_libxc_files} EQUAL 0)
		message(FATAL_ERROR "The Libxc submodule directory is present but not populated. 
Run `git submodule update --init --recursive` in Octopus's root.")
	endif()
endif()	

if (NOT TARGET Libxc::xcf03)
	add_library(Libxc::xcf03 ALIAS xcf03)
endif ()

# FetchContent does not define target include directories
if (CMAKE_DISABLE_FIND_PACKAGE_Libxc)
	target_include_directories(xcf03 INTERFACE
		"$<BUILD_INTERFACE:${Libxc_SOURCE_DIR}/src>"
	)
endif()

set_package_properties(Libxc PROPERTIES
		URL https://gitlab.com/libxc/libxc
		DESCRIPTION "Library of exchange-correlation functionals for density-functional theory."
)

set_property(GLOBAL APPEND PROPERTY PACKAGES_FOUND Libxc)
get_property(_packages_not_found GLOBAL PROPERTY PACKAGES_NOT_FOUND)
list(REMOVE_ITEM _packages_not_found Libxc)
set_property(GLOBAL PROPERTY PACKAGES_NOT_FOUND ${_packages_not_found})
