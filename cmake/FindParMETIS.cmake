#[==============================================================================================[
#                                ParMETIS compatibility wrapper                                #
]==============================================================================================]

#[===[.md
# FindParMETIS

ParMETIS compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET METIS::parmetis)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT FindParMETIS)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES ParMETIS parmetis
        PKG_MODULE_NAMES parmetis)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(METIS::parmetis ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(ParMETIS_LIBRARY
            NAMES parmetis
    )
    mark_as_advanced(ParMETIS_LIBRARY)
    find_path(ParMETIS_INCLUDE_DIR
            NAMES parmetis.h
            PATH_SUFFIXES metis
    )
    mark_as_advanced(ParMETIS_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS ParMETIS_LIBRARY ParMETIS_INCLUDE_DIR
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${ParMETIS_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${ParMETIS_LIBRARY})
        add_library(METIS::parmetis UNKNOWN IMPORTED)
        set_target_properties(METIS::parmetis PROPERTIES
                IMPORTED_LOCATION ${ParMETIS_LIBRARY}
                IMPORTED_LINK_INTERFACE_LANGUAGES C
        )
        target_include_directories(METIS::parmetis INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
        target_link_libraries(METIS::parmetis INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_PARMETIS 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/KarypisLab/ParMETIS
        DESCRIPTION "ParMETIS - Parallel Graph Partitioning and Fill-reducing Matrix Ordering"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
