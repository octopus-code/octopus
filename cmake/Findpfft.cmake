#[==============================================================================================[
#                                  pfft compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# Findpfft

pfft compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET pfft::pfft)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findpfft)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        HAVE_FALLBACK
        NAMES pfft
        PKG_MODULE_NAMES pfft)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(pfft::pfft ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
# Temporary workarounds for non-packages
if (NOT ${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    find_library(pfft_LIBRARY
            NAMES pfft
    )
    mark_as_advanced(pfft_LIBRARY)
    find_path(pfft_INCLUDE_DIR
            NAMES pfft.f03
            PATH_SUFFIXES pfft
    )
    mark_as_advanced(pfft_INCLUDE_DIR)
    find_package_handle_standard_args(${CMAKE_FIND_PACKAGE_NAME}
            REQUIRED_VARS pfft_LIBRARY pfft_INCLUDE_DIR
    )
    if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
        set(${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS ${pfft_INCLUDE_DIR})
        set(${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES ${pfft_LIBRARY})
        add_library(pfft::pfft UNKNOWN IMPORTED)
        set_target_properties(pfft::pfft PROPERTIES
                IMPORTED_LOCATION ${pfft_LIBRARY}
        )
        target_include_directories(pfft::pfft INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_INCLUDE_DIRS})
        target_link_libraries(pfft::pfft INTERFACE ${${CMAKE_FIND_PACKAGE_NAME}_LIBRARIES})
    endif()
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_PFFT 1)
endif ()

# pfft requires FFTW with MPI support but this dependency is not handled by pfft's build system.
# See https://github.com/mpip/pfft/issues/40. As such, Octopus must explicitly provide FFTW::DoubleMPI
# Cannot link to FFTW-MPI when using MKL. See https://gitlab.com/octopus-code/octopus/-/issues/1167
if (pfft_FOUND)
    if (NOT TARGET FFTW::DoubleMPI)
        message(FATAL_ERROR "pfft was found, but FFTW3 with MPI support, which is required by pfft, is not available.")
    endif ()
endif ()

set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/mpip/pfft
        DESCRIPTION "PFFT is a software library for computing massively parallel, fast Fourier transformations on distributed memory architectures"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
