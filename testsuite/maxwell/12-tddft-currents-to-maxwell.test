# -*- coding: utf-8 mode: shell-script -*-

Test       : Maxwell-TDDFT dynamics at different coupling levels
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : Yes

#This test checks the propagation of electromagnetic fields given by the TDDFT currents of kicked benzene

# ground state
ExtraFile  : 12-tddft-currents-to-maxwell.00-benzene.xyz
Input      : 12-tddft-currents-to-maxwell.01-benzene-gs.inp
Precision: 4e-08
match ; SCF convergence ; GREPCOUNT(benzene/static/info, 'SCF converged') ; 1
Precision: 1e-04
match ;     Initial energy     ; GREPFIELD(benzene/static/info, 'Total       =', 3) ; -37.44576068

# tddft-maxwell dynamics, dipole in length gauge
Input      : 12-tddft-currents-to-maxwell.02-benzene-mxll-td-length-gauge.inp

Precision: 3.74e-13
match ;  Benzene Energy [step  0]  ; LINEFIELD(benzene/td.general/energy, -21, 3) ; -37.44578880864112
match ;  Benzene Energy [step 20]  ; LINEFIELD(benzene/td.general/energy, -1, 3) ; -37.4456586018051

Precision: 2.00e-14
match ; Benzene Multipoles [step  0] ; LINEFIELD(benzene/td.general/multipoles, -21, 4) ; 0.0
match ; Benzene Multipoles [step 20]  ; LINEFIELD(benzene/td.general/multipoles, -1, 4) ; -0.020945152833762942

Precision: 1e-12
match ;  Tot. Maxwell energy [step 0]  ; LINEFIELD(maxwell/td.general/maxwell_energy, 6, 3) ; 0.0
match ;    Tot. Maxwell energy [step 300]   ; LINEFIELD(maxwell/td.general/maxwell_energy, 306, 3) ; 1.8252201808122854e-06

Precision: 1e-7
match ;    Ex  (x=  0.76,y=  0,z=0) [step 400]   ; LINEFIELD(maxwell/output_iter/td.0000400/e_field-x\.y=0\,z=0, 19, 2) ; 7.89158873438839E-05
Precision: 2e-17
match ;    By  (x=  0,y=  0,z=3.02) [step 400]   ; LINEFIELD(maxwell/output_iter/td.0000400/b_field-y\.x=0\,y=0, 20, 2) ; -4.20000942740198e-08

Precision: 1e-10
match ;  Benzene Dipole Ex Field from Maxwell [step 13]   ; LINEFIELD(benzene/td.general/maxwell_dipole_field, 20, 3) ; -1.3845786664804949e-05

# tddft-maxwell dynamics, dipole in velocity gauge
Input      : 12-tddft-currents-to-maxwell.03-benzene-mxll-td-veloc-gauge.inp

Precision: 2.54e-14
match ;  Benzene Multipoles [step  0]  ; LINEFIELD(benzene/td.general/multipoles, -21, 4) ; 0.0
Precision: 3.74e-12
match ;     Benzene Energy [step  0]     ; LINEFIELD(benzene/td.general/energy, -21, 3) ; -37.44578880864112
match ;     Benzene Energy [step 20]     ; LINEFIELD(benzene/td.general/energy, -1, 3) ; -37.44565861350341
Precision: 2.0e-14
match ;    Benzene Multipoles [step 20]     ; LINEFIELD(benzene/td.general/multipoles, -1, 4) ; -0.02094502658485187
Precision: 1.00e-12
match ;     Tot. Maxwell energy [step 0]     ; LINEFIELD(maxwell/td.general/maxwell_energy, 6, 3) ; 0.0
match ;     Tot. Maxwell energy [step 300]     ; LINEFIELD(maxwell/td.general/maxwell_energy, 306, 3) ; 1.8251988163130319e-06
Precision: 1.e-14
match ;       Ex  (x=  0.76,y=  0,z=0) [step 400]      ; LINEFIELD(maxwell/output_iter/td.0000400/e_field-x\.y=0\,z=0, 19, 2) ; 7.89220185345642E-05
Precision: 8.48e-12
match ;       By  (x=  0,y=  0,z=3.02) [step 400]      ; LINEFIELD(maxwell/output_iter/td.0000400/b_field-y\.x=0\,y=0, 20, 2) ; -4.19204058973226E-08
Precision: 1.00e-12
match ;   Dipolar field [step  20]  ; LINEFIELD(benzene/td.general/maxwell_dipole_field, -1, 3) ; 9.9539974645246381e-08

# tddft-maxwell dynamics, full minimal coupling
Input      : 12-tddft-currents-to-maxwell.04-benzene-mxll-td-full-min-coup.inp

Precision: 3.74e-13
match ;   Benzene Energy [step  0]   ; LINEFIELD(benzene/td.general/energy, -21, 3) ; -37.44578880864112
match ;   Benzene Energy [step 20]   ; LINEFIELD(benzene/td.general/energy, -1, 3) ; -37.445658587165468

Precision: 2.54e-14
match ;  Benzene Multipoles [step  0] ; LINEFIELD(benzene/td.general/multipoles, -21, 4) ; 0.0
match ;  Benzene Multipoles [step 20]   ; LINEFIELD(benzene/td.general/multipoles, -1, 4) ; -2.0946234710390102e-02

Precision: 1.00e-12
match ;   Tot. Maxwell energy [step 0]   ; LINEFIELD(maxwell/td.general/maxwell_energy, 6, 3) ; 0.0
match ;    Tot. Maxwell energy [step 300]   ; LINEFIELD(maxwell/td.general/maxwell_energy, 306, 3) ; 1.8255478826055845e-06

Precision: 5e-07
match ;     Ex  (x=  0.76,y=  0,z=0) [step 400]     ; LINEFIELD(maxwell/output_iter/td.0000400/e_field-x\.y=0\,z=0, 19, 2) ; 7.89098648744605E-05
Precision: 2e-10
match ;    By  (x=  0,y=  0,z=3.02) [step 400]    ; LINEFIELD(maxwell/output_iter/td.0000400/b_field-y\.x=0\,y=0, 20, 2) ; -4.18808529368566E-08
Precision: 2e-10
match ;    Diamagnetic current  (x=-0.38, y=  0,z=0) [step 20]    ; LINEFIELD(benzene/output_iter/td.0000020/current_dia-x.z\=0, 470, 3) ; 2.29218571171827E-08
Precision: 2e-10
match ;    Total current  (x=-0.38, y=  0,z=0) [step 20]    ; LINEFIELD(benzene/output_iter/td.0000020/current-x.z\=0, 470, 3) ; 9.83482848244905E-005

# external source-tddft dynamics, dipole in velocity gauge
Input      : 12-tddft-currents-to-maxwell.05-benzene-extsource-td-veloc-gauge.inp
Precision: 1e-4
match ;  Benzene Energy [step  0]  ; LINEFIELD(benzene/td.general/energy, -21, 3) ; -3.7445782357444699e+01
match ;  Benzene Energy [step 20]  ; LINEFIELD(benzene/td.general/energy, -1, 3) ; -3.7445292890781460e+01

Precision: 1e-10
match ; Benzene Multipoles [step  0] ; LINEFIELD(benzene/td.general/multipoles, -21, 4) ; 0.0
Precision: 1e-07
match ; Benzene Multipoles [step 20]  ; LINEFIELD(benzene/td.general/multipoles, -1, 4) ; -9.5204920166063031e-04

# maxwell-tddft dynamics with dipole and quadrupole interaction
Input      : 12-tddft-currents-to-maxwell.06-benzene-dipole-and-quadrupole.inp

Precision: 1e-04
match ;   Benzene Energy [step  0]   ; LINEFIELD(benzene/td.general/energy, -21, 3) ; -37.44578235744467
Precision: 3e-3
match ;   Benzene Energy [step 20]   ; LINEFIELD(benzene/td.general/energy, -1, 3) ; -37.4434318288578

Precision: 1e-10
match ;  Benzene Multipoles [step  0]  ; LINEFIELD(benzene/td.general/multipoles, -21, 4) ; 0.0
Precision: 1e-06
match ;  Benzene Multipoles [step 20]   ; LINEFIELD(benzene/td.general/multipoles, -1, 4) ; 0.090862714250860691

Precision: 1e-8
match ;  Maxwell dipole field [step  10]  ; LINEFIELD(benzene/td.general/multipoles, -11, 4) ; 1.9994170595845105e-02