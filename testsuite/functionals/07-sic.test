# -*- coding: utf-8 mode: shell-script -*-

Test       : LDA+SIC/OEP-KLI Functional
Program    : octopus
TestGroups : long-run, functionals
Enabled    : Yes

# This test performs the calculation of an argon atom making
# use of the local-density approximation (LDA) plus the self-interaction
# correction (SIC), treating this latter term with the Krieger-Li-Iafrate
# approximation (KLI) to the optimized effective potential method (OEP).
# This is the functional used in:
# [X.-M. Tong and S.-I Chu, Phys. Rev. A 55, 3406 (1997)],
# to the best of my knowledge for the first time.
#
# The calculation reproduces the result given for Ar in Table IV of this paper.
# It is the value of the energy of the HOMO (a.k.a. ionization potential): 0.549Ha.
#
# The number is missed by 0.006Ha = 0.16eV. The reason is probably that we
# use a LDA pseudopotential, whereas the results in the paper are all-electron.

Input     : 07-sic.01-gs.inp

match ;  SCF convergence     ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 3.97e-05
match ;         Total energy               ; GREPFIELD(static/info, 'Total       =', 3) ; -20.79877188
Precision: 1.00e-04
match ;         Ion-ion energy             ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 1.08e-05
match ;         Eigenvalues sum            ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.35839379
Precision: 2.17e-05
match ;         Hartree energy             ; GREPFIELD(static/info, 'Hartree     =', 3) ; 18.0376534
Precision: 7.45e-06
match ;         Int[n*v_xc]                ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -5.880564305
Precision: 3.19e-07
match ;         Exchange energy            ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.28328899
Precision: 1.00e-04
match ;         Correlation energy         ; GREPFIELD(static/info, 'Correlation =', 3) ; 0.0
Precision: 1.19e-04
match ;         Kinetic energy             ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 7.808949565
Precision: 1.81e-04
match ;         External energy            ; GREPFIELD(static/info, 'External    =', 3) ; -43.362085379999996
Precision: 5.25e-06
match ;        Eigenvalue 1              ; GREPFIELD(static/info, '1   --', 3) ; -1.050882
Precision: 2.71e-05
match ;        Eigenvalue 2              ; GREPFIELD(static/info, '2   --', 3) ; -0.542777
Precision: 2.71e-05
match ;        Eigenvalue 3              ; GREPFIELD(static/info, '3   --', 3) ; -0.542772
Precision: 2.71e-05
match ;        Eigenvalue 4              ; GREPFIELD(static/info, '4   --', 3) ; -0.542767

#The following test only runs in serial at the moment
Processors : 1
Input     : 07-sic.02-scdm.inp

Precision: 2.71e-05
match ; SCF convergence    ; GREPCOUNT(static/info, 'SCF converged') ; 1

Precision: 4.60e-05
match ;     Total energy           ; GREPFIELD(static/info, 'Total       =', 3) ; -20.9298725
Precision: 1.00e-04
match ;     Ion-ion energy         ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.0
Precision: 2.50e-05
match ;     Eigenvalues sum        ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.49222164
Precision: 1.75e-05
match ;     Hartree energy         ; GREPFIELD(static/info, 'Hartree     =', 3) ; 18.1836418
Precision: 1.88e-05
match ;     Int[n*v_xc]            ; GREPFIELD(static/info, 'Int\[n\*v_xc\] =', 3) ; -6.19209947
Precision: 8.70e-06
match ;     Exchange energy        ; GREPFIELD(static/info, 'Exchange    =', 3) ; -3.446109045
Precision: 1.00e-04
match ;     Correlation energy     ; GREPFIELD(static/info, 'Correlation =', 3) ; 0.0
Precision: 1.06e-04
match ;     Kinetic energy         ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 7.9187255599999995
Precision: 1.63e-04
match ;     External energy        ; GREPFIELD(static/info, 'External    =', 3) ; -43.586130960000006

Precision: 5.35e-05
match ;     Eigenvalue 1           ; GREPFIELD(static/info, '1   --', 3) ; -1.06952
Precision: 2.81e-05
match ;     Eigenvalue 2           ; GREPFIELD(static/info, '2   --', 3) ; -0.561086
Precision: 2.80e-05
match ;     Eigenvalue 3           ; GREPFIELD(static/info, '3   --', 3) ; -0.560688
Precision: 2.77e-05
match ;     Eigenvalue 4           ; GREPFIELD(static/info, '4   --', 3) ; -0.554821
