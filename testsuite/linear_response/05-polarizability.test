# -*- coding: utf-8 mode: shell-script -*-

Test       : Sodium static polarizability
Program    : octopus
TestGroups : long-run, linear_response
Enabled    : Yes


Input: 05-polarizability.01-gs.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 8.25e-07
match ;  Total energy    ; GREPFIELD(static/info, 'Total       =', 3) ; -11.40216086
# due to the perversity of Methfessel-Paxton, occupations can be outside [0,2] and entropy can be negative
Precision: 8.80e-07
match ;  Free energy     ; GREPFIELD(static/info, 'Free        =', 3) ; -11.32495793
Precision: 1.59e-05
match ;  Eigenvalue      ; GREPFIELD(static/info, '1   --', 3) ; -3.178126
Precision: 1.02e-05
match ;  Occupation      ; GREPFIELD(static/info, '1   --', 4) ; 2.032756
Precision: 9.19e-06
match ;  Eigenvalue      ; GREPFIELD(static/info, '2   --', 3) ; -1.837042
Precision: 1.64e-16
match ;  Occupation      ; GREPFIELD(static/info, '2   --', 4) ; -0.032739000000000004
Precision: 1.36e-05
match ;  Fermi energy    ; GREPFIELD(static/info, 'Fermi energy =', 4) ; -2.712718

Input: 05-polarizability.02-sternheimer.inp
if(available libxc_fxc); then
Precision: 2.43e-05
  match ;  Polarizability xx  ; LINEFIELD(em_resp/freq_0.0000/alpha, 2, 1) ; 48.678087
Precision: 1.35e-05
  match ;  Polarizability yy  ; LINEFIELD(em_resp/freq_0.0000/alpha, 3, 2) ; 27.011462
Precision: 1.35e-05
  match ;  Polarizability zz  ; LINEFIELD(em_resp/freq_0.0000/alpha, 4, 3) ; 27.011462
else
  match ; Error no libxc_fxc ; GREPCOUNT(err, 'not compiled with the fxc support') ; 1
endif

Input: 05-polarizability.03-finite-diff.inp
Precision: 2.43e-05
match ;   Polarizability xx   ; LINEFIELD(em_resp_fd/alpha, 2, 1) ; 48.678821
Precision: 8.25e-06
match ;   Polarizability yy   ; LINEFIELD(em_resp_fd/alpha, 3, 2) ; 27.012186500000002
Precision: 8.25e-06
match ;   Polarizability zz   ; LINEFIELD(em_resp_fd/alpha, 4, 3) ; 27.0121875

Precision: 1.15e-05
match ;   Born charge xx   ; LINEFIELD(em_resp_fd/born_charges, 3, 1) ; 0.002293
Precision: 2.41e-11
match ;   Born charge yy   ; LINEFIELD(em_resp_fd/born_charges, 4, 2) ; 0.0048270000000000006
Precision: 2.41e-11
match ;   Born charge zz   ; LINEFIELD(em_resp_fd/born_charges, 5, 3) ; 0.0048270000000000006
# the born charges should all be zero by symmetry, if the calculation were converged
Precision: 3.98e-17
match ;   Born charge diff  ; GREPFIELD(em_resp_fd/born_charges, "Discrepancy", 3, 4) ; 0.003982
# These values should all be zero by symmetry.
Precision: 8.88e-06
match ;   beta xxx   ; GREPFIELD(em_resp_fd/beta, "beta xxx", 3) ; 0.0036106848
Precision: 7.01e-06
match ;   beta zxy   ; GREPFIELD(em_resp_fd/beta, "beta zxy", 3) ; 0.004141856300000001
