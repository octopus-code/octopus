# -*- coding: utf-8 mode: shell-script -*-

Test       : GGA+U for (AF) bulk NiO crystal with noncollinear spins
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes

Processors : 2

Input      : 07-noncollinear.01-U5-gs.inp

Precision: 1e-08
match ;   SCF convergence   ; GREPCOUNT(static/info, 'SCF converged') ; 1.0
match ;   Total k-points     ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 4.0
match ;   Reduced k-points   ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 2.0

Precision: 6.04e-08
match ;          Total energy                ; GREPFIELD(static/info, 'Total       =', 3) ; -120.89410739
Precision: 9.00e-13
match ;          Ion-ion energy              ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -89.95635275
Precision: 5.52e-14
match ;          Eigenvalues sum             ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.51767229
Precision: 1.71e-12
match ;          Hartree energy              ; GREPFIELD(static/info, 'Hartree     =', 3) ; 34.13301329
Precision: 1.98e-13
match ;          Exchange energy             ; GREPFIELD(static/info, 'Exchange    =', 3) ; -19.819106140000002
Precision: 2.20e-08
match ;          Correlation energy          ; GREPFIELD(static/info, 'Correlation =', 3) ; -2.00218391
Precision: 8.55e-08
match ;          Kinetic energy              ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 170.94478574
Precision: 1.07e-07
match ;          External energy             ; GREPFIELD(static/info, 'External    =', 3) ; -214.30567101
Precision: 5.57e-08
match ;          Hubbard energy              ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.11143156
Precision: 1.00e-04
match ;          k-point 1 (x)          ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
Precision: 1.00e-04
match ;          k-point 1 (y)          ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
Precision: 1.00e-04
match ;          k-point 1 (z)          ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 2.78e-04
match ;          Eigenvalue  1          ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.55535
Precision: 2.41e-05
match ;          Eigenvalue  8          ; GREPFIELD(static/info, '#k =       1', 3, 8) ; -0.482612
Precision: 1.55e-05
match ;          Eigenvalue 16          ; GREPFIELD(static/info, '#k =       1', 3, 16) ; -0.310485
Precision: 3.29e-16
match ;          Eigenvalue 17          ; GREPFIELD(static/info, '#k =       1', 3, 17) ; -0.03293

Precision: 1.00e-04
match ;         Total Magnetic Moment x         ; GREPFIELD(static/info, 'mx = ', 3) ; 0.0
match ;         Total Magnetic Moment y         ; GREPFIELD(static/info, 'my = ', 3) ; 0.0
match ;         Total Magnetic Moment z         ; GREPFIELD(static/info, 'mz = ', 3) ; 0.0
Precision: 9.06e-05
match ;         Local Magnetic Moment (Ni1)         ; GREPFIELD(static/info, '1        Ni', 3) ; 1.8123900000000002
Precision: 9.06e-05
match ;         Local Magnetic Moment (Ni2)         ; GREPFIELD(static/info, '2        Ni', 3) ; -1.8123900000000002
Precision: 2.20e-06
match ;         Local Magnetic Moment (O1)         ; GREPFIELD(static/info, '3         O', 3) ; 0.0
match ;         Local Magnetic Moment (O2)         ; GREPFIELD(static/info, '4         O', 3) ; 0.0
Precision: 3.01e-07
match ;         Occupation Ni2 up-down 3d4         ; LINEFIELD(static/occ_matrices, -2, 7) ; 0.00060196
match ;         Occupation Ni2 up-down 3d5         ; LINEFIELD(static/occ_matrices, -1, 9) ; -0.46237429

Precision: 6.62e-12
match ;           Force 1 (x)                  ; GREPFIELD(static/info, 'Forces on the ions', 3, 2) ; 6.04431235e-13
Precision: 6.62e-12
match ;           Force 1 (y)                  ; GREPFIELD(static/info, 'Forces on the ions', 4, 2) ; 6.027895e-13
Precision: 1.17e-10
match ;           Force 1 (z)                  ; GREPFIELD(static/info, 'Forces on the ions', 5, 2) ; 1.2808135000000002e-13
Precision: 6.62e-12
match ;           Force 2 (x)                  ; GREPFIELD(static/info, 'Forces on the ions', 3, 3) ; 6.026180899999999e-13
Precision: 6.62e-12
match ;           Force 2 (y)                  ; GREPFIELD(static/info, 'Forces on the ions', 4, 3) ; 6.04842545e-13
Precision: 1.17e-10
match ;           Force 2 (z)                  ; GREPFIELD(static/info, 'Forces on the ions', 5, 3) ; 1.401213e-13
Precision: 2.12e-07
match ;           Force 3 (x)                  ; GREPFIELD(static/info, 'Forces on the ions', 3, 4) ; 1.22984922e-07
match ;           Force 3 (y)                  ; GREPFIELD(static/info, 'Forces on the ions', 4, 4) ; 1.22984921e-07
match ;           Force 3 (z)                  ; GREPFIELD(static/info, 'Forces on the ions', 5, 4) ; -1.22967572e-07
match ;           Force 4 (x)                  ; GREPFIELD(static/info, 'Forces on the ions', 3, 5) ; -1.2298709e-07
match ;           Force 4 (y)                  ; GREPFIELD(static/info, 'Forces on the ions', 4, 5) ; -1.2298709e-07
match ;           Force 4 (z)                  ; GREPFIELD(static/info, 'Forces on the ions', 5, 5) ; 1.22981505e-07

Input      : 07-noncollinear.02-acbn0.inp

Precision: 1e-8
match ;  SCF convergence  ; GREPCOUNT(static/info, 'SCF converged') ; 1.0

match ;  Total k-points    ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 4.0
match ;  Reduced k-points  ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 2.0

Precision: 9.95e-05
match ;          Total energy                 ; GREPFIELD(static/info, 'Total       =', 3) ; -120.7949443
Precision: 9.00e-13
match ;          Ion-ion energy               ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -89.95635275
Precision: 1.26e-04
match ;          Eigenvalues sum              ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -7.238978865
Precision: 1.07e-05
match ;          Hartree energy               ; GREPFIELD(static/info, 'Hartree     =', 3) ; 34.10201033
Precision: 2.80e-06
match ;          Exchange energy              ; GREPFIELD(static/info, 'Exchange    =', 3) ; -19.82098157
Precision: 1.76e-07
match ;          Correlation energy           ; GREPFIELD(static/info, 'Correlation =', 3) ; -1.99995766
Precision: 3.96e-05
match ;          Kinetic energy               ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 170.718696905
Precision: 3.87e-05
match ;          External energy              ; GREPFIELD(static/info, 'External    =', 3) ; -214.03169500
Precision: 1.91e-06
match ;          Hubbard energy               ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.19331302

Precision: 1.00e-04
match ;          Total Magnetic Moment x          ; GREPFIELD(static/info, 'mx = ', 3) ; 0.0
match ;          Total Magnetic Moment y          ; GREPFIELD(static/info, 'my = ', 3) ; 0.0
match ;          Total Magnetic Moment z          ; GREPFIELD(static/info, 'mz = ', 3) ; 0.0
Precision: 1.65e-06
match ;          Local Magnetic Moment (Ni1)          ; GREPFIELD(static/info, '1        Ni', 3) ; 1.8507775000000002
match ;          Local Magnetic Moment (Ni2)          ; GREPFIELD(static/info, '2        Ni', 3) ; -1.8507775000000002
Precision: 2.20e-06
match ;          Local Magnetic Moment (O1)           ; GREPFIELD(static/info, '3         O', 3) ; 1.0000000000000002e-06
match ;          Local Magnetic Moment (O2)           ; GREPFIELD(static/info, '4         O', 3) ; 1.0000000000000002e-06

Precision: 3.05e-07
match ;        Occupation Ni2 up-down 3d4        ; LINEFIELD(static/occ_matrices, -2, 7) ; 0.00061063
Precision: 2.36e-06
match ;        Occupation Ni2 up-down 3d5        ; LINEFIELD(static/occ_matrices, -1, 9) ; -0.4716783

Precision: 4.00e-05
match ;          Eigenvalue  1          ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.66387
match ;          Eigenvalue  8          ; GREPFIELD(static/info, '#k =       1', 3, 8) ; -0.58951
match ;          Eigenvalue 16          ; GREPFIELD(static/info, '#k =       1', 3, 16) ; -0.30908
match ;          Eigenvalue 17          ; GREPFIELD(static/info, '#k =       1', 3, 17) ; -0.0325355
