# -*- coding: utf-8 mode: shell-script -*-

Test       : TiO2 and matrix elements
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : Yes

Input      : 18-TiO2.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ;  Total k-points    ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 8.0
match ;  Reduced k-points  ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 6.0
match ;  Space group         ; GREPFIELD(out, 'Space group', 4) ; 136.0
match ;  No. of symmetries   ; GREPFIELD(out, 'symmetries that can be used', 5) ; 8.0

Precision: 1.57e-06
match ;        Total energy              ; GREPFIELD(static/info, 'Total       =', 3) ; -184.8031278
Precision: 5.94e-08
match ;        Ion-ion energy            ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -118.71359251
Precision: 3.36e-07
match ;        Eigenvalues sum           ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -27.92210072
Precision: 1.50e-06
match ;         Hartree energy             ; GREPFIELD(static/info, 'Hartree     =', 3) ; 42.44778686
Precision: 1.58e-06
match ;        Exchange energy           ; GREPFIELD(static/info, 'Exchange    =', 3) ; -31.645265900000002
Precision: 1.13e-07
match ;        Correlation energy        ; GREPFIELD(static/info, 'Correlation =', 3) ; -2.2617048200000003
Precision: 4.43e-07
match ;        Kinetic energy            ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 88.62086204
Precision: 1.96e-06
match ;         External energy            ; GREPFIELD(static/info, 'External    =', 3) ; -163.25121256

Precision: 2.08e-03
match ;    Direct gap    ; GREPFIELD(static/info, 'Direct gap', 7, 0) ; 0.0416
Precision: 2.08e-03
match ;    Indirect gap    ; GREPFIELD(static/info, 'Indirect gap', 10, 0) ; 0.0416

Precision: 4.31e-08
match ;      Two-body (vvvv) Re      ; LINEFIELD(static/output_me_two_body, 2, 9) ; 0.062175814498219996
Precision: 1.00e-04
match ;      Two-body (vvvv) Im      ; LINEFIELD(static/output_me_two_body, 2, 10) ; 0.0
Precision: 2.44e-07
match ;      Two-body (cccc) Re      ; LINEFIELD(static/output_me_two_body, 1887, 9) ; 1.278353329882
Precision: 1.00e-08
match ;      Two-body (cccc) Im      ; LINEFIELD(static/output_me_two_body, 1887, 10) ; 0.0
Precision: 1.00e-08
match ;      Two-body (vvcc) Re      ; LINEFIELD(static/output_me_two_body, 14, 9) ; 0.0
Precision: 1.00e-08
match ;      Two-body (vvcc) Re      ; LINEFIELD(static/output_me_two_body, 14, 10) ; 0.0

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.01e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -2.029017
Precision: 1.01e-05
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -2.0199759999999998
Precision: 5.87e-04
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -1.1749
Precision: 5.83e-06
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; -1.166679

Input      : 18-TiO2.02-gs_kerker.inp

Precision: 5.83e-06
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ;  Total k-points    ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 8.0
match ;  Reduced k-points  ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 6.0
match ;  Space group         ; GREPFIELD(out, 'Space group', 4) ; 136.0
match ;  No. of symmetries   ; GREPFIELD(out, 'symmetries that can be used', 5) ; 8.0

Precision: 1.10e-07
match ;  Total energy        ; GREPFIELD(static/info, 'Total       =', 3) ; -184.02166672
Precision: 1.19e-13
match ;  Ion-ion energy      ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -118.71359251
Precision: 1.76e-07
match ;  Eigenvalues sum     ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -27.96739887
Precision: 2.09e-07
match ;  Hartree energy      ; GREPFIELD(static/info, 'Hartree     =', 3) ; 41.72762507
Precision: 1.57e-07
match ;  Exchange energy     ; GREPFIELD(static/info, 'Exchange    =', 3) ; -31.32403024
Precision: 1.13e-07
match ;  Correlation energy  ; GREPFIELD(static/info, 'Correlation =', 3) ; -2.26689777
Precision: 4.33e-07
match ;  Kinetic energy      ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 86.55732751000001
Precision: 8.00e+01
match ;  External energy     ; GREPFIELD(static/info, 'External    =', 3) ; -160.0

Precision: 7.70e-06
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.00e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -2.003791
Precision: 9.97e-06
match ;    Eigenvalue  2    ; GREPFIELD(static/info, '#k =       1', 3, 2) ; -1.994046
Precision: 6.07e-06
match ;    Eigenvalue  4    ; GREPFIELD(static/info, '#k =       1', 3, 4) ; -1.213804
Precision: 6.04e-06
match ;    Eigenvalue  5    ; GREPFIELD(static/info, '#k =       1', 3, 5) ; -1.208434

Precision: 5.06e-17
match ;  Direct gap  ; GREPFIELD(static/info, 'Direct gap', 7, 0) ; 0.0524
Precision: 1.02e-03
match ;  Indirect gap  ; GREPFIELD(static/info, 'Indirect gap', 10, 0) ; 0.0204
