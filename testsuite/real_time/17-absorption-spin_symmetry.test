# -*- coding: utf-8 mode: shell-script -*-

Test       : Absorption spectrum with spin-symmetry
Program    : octopus
TestGroups : short-run, real_time
Enabled    : Yes

# ground state
Processors : 1
Input      : 17-absorption-spin_symmetry.01-gs.inp
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 5.65e-07
match ;  Initial energy  ; GREPFIELD(static/info, 'Total       =', 3) ; -11.2990742

Processors : 4
Input      : 17-absorption-spin_symmetry.02-td.inp
Precision: 1.13e-13
match ;  Energy [step   1]  ; LINEFIELD(td.general/energy, -101, 3) ; -11.29907419575246
Precision: 1.13e-13
match ;  Energy [step  25]  ; LINEFIELD(td.general/energy, -76, 3) ; -11.29755022040344
Precision: 1.32e-13
match ;  Energy [step  50]  ; LINEFIELD(td.general/energy, -51, 3) ; -11.297550175449219
Precision: 1.18e-13
match ;  Energy [step  75]  ; LINEFIELD(td.general/energy, -26, 3) ; -11.29755014228773
Precision: 1.26e-13
match ;  Energy [step 100]  ; LINEFIELD(td.general/energy, -1, 3) ; -11.29755010654612

Util       : oct-propagation_spectrum
Input      : 17-absorption-spin_symmetry.03-spectrum.inp

Precision: 1.00e-02
match ;   Energy      1   ; LINEFIELD(cross_section_tensor, -91, 1) ; 1.0
Precision: 2.18e-08
match ;   Sigma       1   ; LINEFIELD(cross_section_tensor, -91, 2) ; 0.043600505000000005
Precision: 2.21e-08
match ;   Anisotropy  1   ; LINEFIELD(cross_section_tensor, -91, 3) ; 0.044254822
Precision: 2.00e-02
match ;   Energy      2   ; LINEFIELD(cross_section_tensor, -81, 1) ; 2.0
Precision: 7.98e-08
match ;   Sigma       2   ; LINEFIELD(cross_section_tensor, -81, 2) ; 0.15951824
Precision: 8.08e-08
match ;   Anisotropy  2   ; LINEFIELD(cross_section_tensor, -81, 3) ; 0.16152759
Precision: 3.00e-02
match ;   Energy      3   ; LINEFIELD(cross_section_tensor, -71, 1) ; 3.0
Precision: 1.54e-08
match ;   Sigma       3   ; LINEFIELD(cross_section_tensor, -71, 2) ; 0.30850056
Precision: 1.56e-07
match ;   Anisotropy  3   ; LINEFIELD(cross_section_tensor, -71, 3) ; 0.31108321
Precision: 4.00e-02
match ;   Energy      4   ; LINEFIELD(cross_section_tensor, -61, 1) ; 4.0
Precision: 2.21e-07
match ;   Sigma       4   ; LINEFIELD(cross_section_tensor, -61, 2) ; 0.44101223
Precision: 2.21e-07
match ;   Anisotropy  4   ; LINEFIELD(cross_section_tensor, -61, 3) ; 0.44188177
Precision: 5.00e-02
match ;   Energy      5   ; LINEFIELD(cross_section_tensor, -51, 1) ; 5.0
Precision: 2.57e-07
match ;   Sigma       5   ; LINEFIELD(cross_section_tensor, -51, 2) ; 0.51455401
Precision: 2.55e-07
match ;   Anisotropy  5   ; LINEFIELD(cross_section_tensor, -51, 3) ; 0.51083037
Precision: 6.00e-02
match ;   Energy      6   ; LINEFIELD(cross_section_tensor, -41, 1) ; 6.0
Precision: 5.08e-15
match ;   Sigma       6   ; LINEFIELD(cross_section_tensor, -41, 2) ; 0.50765727
Precision: 2.49e-07
match ;   Anisotropy  6   ; LINEFIELD(cross_section_tensor, -41, 3) ; 0.49731765
Precision: 7.00e-02
match ;   Energy      7   ; LINEFIELD(cross_section_tensor, -31, 1) ; 7.0
Precision: 2.13e-06
match ;   Sigma       7   ; LINEFIELD(cross_section_tensor, -31, 2) ; 0.4255441
Precision: 2.04e-07
match ;   Anisotropy  7   ; LINEFIELD(cross_section_tensor, -31, 3) ; 0.40874608
Precision: 8.00e-02
match ;   Energy      8   ; LINEFIELD(cross_section_tensor, -21, 1) ; 8.0
Precision: 1.48e-07
match ;   Sigma       8   ; LINEFIELD(cross_section_tensor, -21, 2) ; 0.29590935
Precision: 1.38e-07
match ;   Anisotropy  8   ; LINEFIELD(cross_section_tensor, -21, 3) ; 0.27562261
Precision: 9.00e-02
match ;   Energy      9   ; LINEFIELD(cross_section_tensor, -11, 1) ; 9.0
Precision: 7.86e-09
match ;   Sigma       9   ; LINEFIELD(cross_section_tensor, -11, 2) ; 0.15719477
Precision: 6.97e-09
match ;   Anisotropy  9   ; LINEFIELD(cross_section_tensor, -11, 3) ; 0.13931501
Precision: 1.00e-01
match ;   Energy     10   ; LINEFIELD(cross_section_tensor, -1, 1) ; 10.0
Precision: 4.44e-16
match ;   Sigma      10   ; LINEFIELD(cross_section_tensor, -1, 2) ; 0.044390835000000003
Precision: 2.17e-08
match ;   Anisotropy 10   ; LINEFIELD(cross_section_tensor, -1, 3) ; 0.043358744000000005