!! Copyright (C) 2024 A. Buccheri
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

! Note, mk_varinfo.pl required modification to check for variable documentation
! outside of src/
module test_parameters_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use namespace_oct_m
  use parser_oct_m


  implicit none
  private
  public :: test_parameters_t

  type test_parameters_t
    integer :: type
    integer :: repetitions
    integer :: min_blocksize
    integer :: max_blocksize
  contains
    procedure :: init_from_file => test_parameters_init_from_file
  end type test_parameters_t

contains

  !> @brief Initialise a test parameters instance from an inp file.
  subroutine test_parameters_init_from_file(this, namespace)
    class(test_parameters_t), intent(out) :: this
    type(namespace_t),        intent(in ) :: namespace

    PUSH_SUB(test_parameters_init_from_file)

    call messages_obsolete_variable(namespace, 'TestDerivatives', 'TestType')
    call messages_obsolete_variable(namespace, 'TestOrthogonalization', 'TestType')

    !%Variable TestType
    !%Type integer
    !%Default all
    !%Section Calculation Modes::Test
    !%Description
    !% Decides on what type of values the test should be performed.
    !%Option real 1
    !% Test for double-precision real functions.
    !%Option complex 2
    !%Option all 3
    !% Tests for double-precision real and complex functions.
    !%End
    call parse_variable(namespace, 'TestType', OPTION__TESTTYPE__ALL, this%type)
    if (this%type < 1 .or. this%type > 5) then
      message(1) = "Invalid option for TestType."
      call messages_fatal(1, only_root_writes = .true., namespace=namespace)
    end if

    !%Variable TestRepetitions
    !%Type integer
    !%Default 1
    !%Section Calculation Modes::Test
    !%Description
    !% This variable controls the behavior of oct-test for performance
    !% benchmarking purposes. It sets the number of times the
    !% computational kernel of a test will be executed, in order to
    !% provide more accurate timings.
    !%
    !% Currently this variable is used by the <tt>hartree_test</tt>,
    !% <tt>derivatives</tt>, and <tt>projector</tt> tests.
    !%End
    call parse_variable(namespace, 'TestRepetitions', 1, this%repetitions)

    !%Variable TestMinBlockSize
    !%Type integer
    !%Default 1
    !%Section Calculation Modes::Test
    !%Description
    !% Some tests can work with multiple blocksizes, in this case of
    !% range of blocksizes will be tested. This variable sets the lower
    !% bound of that range.
    !%
    !% Currently this variable is only used by the derivatives test.
    !%End
    call parse_variable(namespace, 'TestMinBlockSize', 1, this%min_blocksize)

    !%Variable TestMaxBlockSize
    !%Type integer
    !%Default 128
    !%Section Calculation Modes::Test
    !%Description
    !% Some tests can work with multiple blocksizes, in this case of
    !% range of blocksizes will be tested. This variable sets the lower
    !% bound of that range.
    !%
    !% Currently this variable is only used by the derivatives test.
    !%End
    call parse_variable(namespace, 'TestMaxBlockSize', 128, this%max_blocksize)

    POP_SUB(test_parameters_init_from_file)

  end subroutine test_parameters_init_from_file

end module test_parameters_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
