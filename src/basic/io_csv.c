/*
  Copyright (C) 2006 octopus team

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.

*/

/*
The functions in this file read an array from an ascii matrix (csv) file.
Format with values "valueXYZ" as follows

File values.csv:
--------
value111 value211 value311 value411
value121 value221 value321 value421
value131 value231 value331 value431

value112 value212 value312 value412
value122 value222 value322 value422
value132 value232 value332 value432

value113 value213 value313 value413
value123 value223 value323 value423
value133 value233 value333 value433
--------

That is, every line scans the x-coordinate, every XY-plane as a table of values
and all XY-planes separated by an empty row.

The given matrix is interpolated/stretched to fit the calculation
box defined in input file.

Note that there must not be any empty line at the end of the file.

Calculation box shape must be "parallelepiped".

The delimiter can be a tab, a comma or a space.
*/

#include <assert.h>
#include <config.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "string_f.h"

#include "io_binary.h"

static const int size_of[6] = {4, 8, 8, 16, 4, 8};

void read_csv(int64_t *np, void *f, int *output_type,
  int *ierr, char* filename) {
  int i;
  FILE *fd;
  char *buf;
  char *c;
  const char sep[] = "\t\n ,";
  int buf_size = 65536;

  fd = fopen(filename, "r");

  *ierr = 0;
  if (fd == NULL) {
    *ierr = 2;
    return;
  }

  buf = (char *)malloc(buf_size * sizeof(char));
  assert(buf != NULL);

  if ((*output_type) == TYPE_DOUBLE) {
    double d;
    i = 0;
    while (fgets(buf, buf_size * sizeof(char), fd) != NULL) {
      c = strtok(buf, sep);
      while (c != NULL) {
        assert(i / 8 < *np);
        d = strtod(c, (char **)NULL);
        memcpy(f + i, &d, size_of[(*output_type)]);
        c = (char *)strtok((char *)NULL, sep);
        i += size_of[(*output_type)];
      }
    }
  }

  free(buf);
  fclose(fd);
}

void get_info_csv(int64_t *dims, int *ierr, char *filename) {
  char *buf;
  char *c;
  FILE *fd;
  int buf_size = 65536;
  const char sep[] = "\n\t ,";

  int64_t curr_dims[3] = {0, 0, 0};
  int64_t prev_dims[2] = {0, 0};

  fd = fopen(filename, "r");

  *ierr = 0;
  if (fd == NULL) {
    *ierr = 2;
    return;
  }

  buf = (char *)malloc(buf_size * sizeof(char));
  assert(buf != NULL);
  while (fgets(buf, buf_size * sizeof(char), fd) != NULL) {
    c = strtok(buf, sep);

    prev_dims[0] = curr_dims[0];

    curr_dims[0] = 0;

    /** count the number of columns i.e. the size in x-direction**/
    while (c != NULL) {
      curr_dims[0]++;
      c = (char *)strtok((char *)NULL, sep);
    }

    /** The number of columns must be the same on all non-empty rows **/
    /** This only checks that the number of columns is correct
        within each z-block **/
    if (prev_dims[0] > 0 && curr_dims[0] > 0)
      assert(curr_dims[0] == prev_dims[0]);

    /** If the previous line was empty and the current one is non-empty, then it
       signifies the start of a new z block i.e. a new xy-plane **/
    if (prev_dims[0] == 0 && curr_dims[0] != 0) {
      prev_dims[1] = curr_dims[1];
      curr_dims[1] = 0;
      curr_dims[2]++;

      /** The number of rows i.e. the size in y-direction must be the same
       * within all z-blocks **/
      if (prev_dims[1] > 0 && curr_dims[1] > 0)
        assert(prev_dims[1] == curr_dims[1]);
    }

    /** If the current row is non-empty, increase the y-dimension **/

    if (curr_dims[0] > 0)
      curr_dims[1]++;
  }

  dims[0] = curr_dims[0];
  dims[1] = curr_dims[1];
  dims[2] = curr_dims[2];

  if (dims[0]*dims[1]*dims[2] == 0)  *ierr=1;
  

  free(buf);
  fclose(fd);
}
