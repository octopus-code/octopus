!! Copyright (C) 2012 D. Strubbe
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

! -----------------------------------------------------------------------
!> This module contains interfaces for routines in operate.c
! -----------------------------------------------------------------------
module operate_f_oct_m

  use, intrinsic :: iso_fortran_env

  implicit none

  public ! only interfaces in this module

  interface
    subroutine doperate_ri_vec(opn, w, opnri, opri, rimap_inv, rimap_inv_max, fi, ldfp, fo)
      import :: real64
      implicit none
      integer,        intent(in)    :: opn
      real(real64),   intent(in)    :: w
      integer,        intent(in)    :: opnri
      integer,        intent(in)    :: opri
      integer,        intent(in)    :: rimap_inv
      integer,        intent(in)    :: rimap_inv_max
      real(real64),   intent(in)    :: fi
      integer,        intent(in)    :: ldfp
      real(real64),   intent(inout) :: fo
    end subroutine doperate_ri_vec
  end interface

  interface
    subroutine zoperate_ri_vec(opn, w, opnri, opri, rimap_inv, rimap_inv_max, fi, ldfp, fo)
      import :: real64
      implicit none
      integer,         intent(in)    :: opn
      real(real64),    intent(in)    :: w
      integer,         intent(in)    :: opnri
      integer,         intent(in)    :: opri
      integer,         intent(in)    :: rimap_inv
      integer,         intent(in)    :: rimap_inv_max
      complex(real64), intent(in)    :: fi
      integer,         intent(in)    :: ldfp
      complex(real64), intent(inout) :: fo
    end subroutine zoperate_ri_vec
  end interface

  interface
    subroutine dgauss_seidel(opn, w, opnri, opri, rimap_inv, rimap_inv_max, factor, pot, rho)
      import :: real64
      implicit none
      integer,        intent(in)    :: opn
      real(real64),   intent(in)    :: w
      integer,        intent(in)    :: opnri
      integer,        intent(in)    :: opri
      integer,        intent(in)    :: rimap_inv
      integer,        intent(in)    :: rimap_inv_max
      real(real64),   intent(in)    :: factor
      real(real64),   intent(inout) :: pot
      real(real64),   intent(in)    :: rho
    end subroutine dgauss_seidel
  end interface

end module operate_f_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
