!! Copyright (C) 2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

!------------------------------------------------------------
!>@brief Post all isend commands for all batches of a given task
!!
!! Note that we use here a unique request ID that is labelled by the
!! sending task ID and the batch index, to help the MPI library.
subroutine X(states_elec_all_to_all_communications_post_all_mpi_isend)(this, st, np, node_to)
  class(states_elec_all_to_all_communications_t),  intent(inout) :: this
  type(states_elec_t),                             intent(inout) :: st
  integer,                                         intent(in)    :: np
  integer,                                         intent(in)    :: node_to

  integer :: icom, ib_send, ik_send, send_size

  if (node_to == -1) return

  PUSH_SUB(X(states_elec_all_to_all_communications_post_all_mpi_isend))
  call profiling_in("ALL_TO_ALL_COMM")

  SAFE_ALLOCATE(this%send_req(1:this%nbatch_to_send))

  do icom = 1, this%nbatch_to_send

    call this%get_send_indices(st, icom, ib_send, ik_send)

    ! At the moment the MPI code below only works with CPU-packed states
    ! TODO: Ultimately this should be better treated, as the GPU-CPU transfer is expensive
    ! Use GPU-aware MPI is possible
    if (st%group%psib(ib_send, ik_send)%status() == BATCH_DEVICE_PACKED) then
      call batch_read_device_to_packed(st%group%psib(ib_send, ik_send))
    end if

    ! If we want to work with unpacked states, we do not want to transfer np_part points.
    ! The solution would be to use MPI subarray for unpacked batches
    ! see https://events.prace-ri.eu/event/176/contributions/57/attachments/148/296/Advanced_MPI_I.pdf
    ! For packed states, this is also needed of we want to not have to send the paded number of states
    this%send_req(icom) = MPI_REQUEST_NULL

    send_size = np*int(st%group%psib(ib_send, ik_send)%pack_size(1), int32)

    call st%st_kpt_mpi_grp%isend(st%group%psib(ib_send, ik_send)%X(ff_pack), &
      send_size, R_MPITYPE, node_to, this%send_req(icom), &
      tag=icom) !st%st_kpt_mpi_grp%rank*st%group%nblocks + icom)
  end do

  call profiling_out("ALL_TO_ALL_COMM")
  POP_SUB(X(states_elec_all_to_all_communications_post_all_mpi_isend))
end subroutine X(states_elec_all_to_all_communications_post_all_mpi_isend)

!------------------------------------------------------------
!>@brief Allocate a batch and perform the MPI_Recv. On exist, the batch contains the received information
!!
!! Note that we use here a unique request ID that is labelled by the
!! sending task ID and the batch index, to help the MPI library.
subroutine X(states_elec_all_to_all_communications_mpi_recv_batch)(this, st, np, node_fr, icom, psib_receiv)
  class(states_elec_all_to_all_communications_t),  intent(in)    :: this
  type(states_elec_t),                             intent(in)    :: st
  integer,                                         intent(in)    :: np
  integer,                                         intent(in)    :: node_fr
  integer,                                         intent(in)    :: icom
  type(wfs_elec_t),                                intent(inout) :: psib_receiv

  integer :: ib, ik, receiv_size, st_start, st_end

  if (node_fr == -1) return

  PUSH_SUB(X(states_elec_all_to_all_communications_mpi_recv_batch))
  call profiling_in("ALL_TO_ALL_COMM")

  ! Current k-point index we are receiving. Used inside local_contribution
  call this%get_receive_indices(st, icom, ib, ik)
  ! We need to know the start and end state indices, for local_contribution
  st_start =  st%group%block_range(ib, 1)
  st_end = st%group%block_range(ib, 2)

  ! Allocate the receiving batch
  call this%alloc_receive_batch(st, icom, np, psib_receiv)

  receiv_size = np*pad_pow2((st_end-st_start+1)*st%d%dim)

  call st%st_kpt_mpi_grp%recv(psib_receiv%X(ff_pack), receiv_size, R_MPITYPE, node_fr, &
    tag=icom) !node_fr*st%group%nblocks + icom)

  call profiling_out("ALL_TO_ALL_COMM")
  POP_SUB(X(states_elec_all_to_all_communications_mpi_recv_batch))
end subroutine X(states_elec_all_to_all_communications_mpi_recv_batch)


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
