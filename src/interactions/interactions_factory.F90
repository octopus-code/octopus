!! Copyright (C) 2020 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module interactions_factory_oct_m
  use coulomb_force_oct_m
  use current_to_mxll_field_oct_m
  use debug_oct_m
  use global_oct_m
  use gravity_oct_m
  use interaction_oct_m
  use interaction_partner_oct_m
  use interaction_enum_oct_m
  use interactions_factory_abst_oct_m
  use linear_medium_to_em_field_oct_m
  use lennard_jones_oct_m
  use lorentz_force_oct_m
  use messages_oct_m
  use mxll_e_field_to_matter_oct_m
  use mxll_b_field_to_matter_oct_m
  use mxll_vec_pot_to_matter_oct_m
  use namespace_oct_m
  use parser_oct_m
  use varinfo_oct_m
  implicit none

  private
  public :: interactions_factory_t

  type, extends(interactions_factory_abst_t) :: interactions_factory_t
  contains
    procedure :: create => interactions_factory_create              !< @copydoc interactions_factory_create()
    procedure, nopass :: options => interactions_factory_options    !< @copydoc interactions_factory_options()
  end type interactions_factory_t

contains

  ! ---------------------------------------------------------------------------------------
  !> @brief create an interaction of given type with specified partner
  !!
  !! This functions calls the constructor of the specific interaction type and returns
  !! a pointer to this new interaction.
  !
  function interactions_factory_create(this, type, partner) result(interaction)
    class(interactions_factory_t),         intent(in)    :: this
    integer,                               intent(in)    :: type
    class(interaction_partner_t),  target, intent(inout) :: partner
    class(interaction_t),                  pointer       :: interaction

    PUSH_SUB(interactions_factory_create)

    select case (type)
    case (GRAVITY)
      interaction => gravity_t(partner)
    case (COULOMB_FORCE)
      interaction => coulomb_force_t(partner)
    case (LORENTZ_FORCE)
      interaction => lorentz_force_t(partner)
    case (LINEAR_MEDIUM_TO_EM_FIELD)
      interaction => linear_medium_to_em_field_t(partner)
    case (CURRENT_TO_MXLL_FIELD)
      interaction => current_to_mxll_field_t(partner)
    case (MXLL_E_FIELD_TO_MATTER)
      interaction => mxll_e_field_to_matter_t(partner)
    case (MXLL_B_FIELD_TO_MATTER)
      interaction => mxll_b_field_to_matter_t(partner)
    case (MXLL_VEC_POT_TO_MATTER)
      interaction => mxll_vec_pot_to_matter_t(partner)
    case (LENNARD_JONES)
      interaction => lennard_jones_t(partner)
    case default
      message(1) = "Unknown interaction in interactions_factory_create"
      call messages_fatal(1)
    end select

    POP_SUB(interactions_factory_create)
  end function interactions_factory_create

  ! ---------------------------------------------------------------------------------------
  !> @brief return a list of options to be used when creating the interactions
  !!
  !! This functions uses the provided namespace to parse the input options
  !! regarding the interaction creation.
  !
  function interactions_factory_options(namespace, interactions) result(options)
    type(namespace_t),                    intent(in)  :: namespace       !< namespace to use when getting the options.
    integer,                              intent(in)  :: interactions(:) !< interactions for which the options are to be provided.
    type(interactions_factory_options_t) :: options(size(interactions))

    integer :: line, col, i, type, timing
    type(block_t) :: blk

    !%Variable InteractionTiming
    !%Type integer
    !%Default timing_exact
    !%Section Time-Dependent::Propagation
    !%Description
    !% A parameter to determine if interactions should use the quantities
    !% at the exact iteration or if retardation is allowed.
    !%Option timing_exact 1
    !% Only allow interactions at the exact iterations required by the algorithms behing executed
    !%Option timing_retarded 2
    !% Allow retarded interactions
    !%End
    call parse_variable(namespace, 'InteractionTiming', TIMING_EXACT, timing)
    if (.not. varinfo_valid_option('InteractionTiming', timing)) then
      call messages_input_error(namespace, 'InteractionTiming')
    end if
    call messages_print_var_option('InteractionTiming', timing, namespace=namespace)
    options%timing = timing

    ! Default modes for the interactions
    do i = 1, size(interactions)
      select case (interactions(i))
      case (GRAVITY, LENNARD_JONES)
        options(i)%mode = NO_PARTNERS
      case default
        options(i)%mode = ALL_PARTNERS
      end select
    end do

    !%Variable Interactions
    !%Type block
    !%Section System
    !%Description
    !% This input option controls the interactions between systems. It basically
    !% allows to select which systems will interact with another system through
    !% a given interaction type. The format of the block is the following:
    !%
    !%  <br>%<tt>Namespace.Interactions
    !%   <br>&nbsp;&nbsp;interaction_type | interaction_mode | ...
    !%  <br>%</tt>
    !%
    !% Here is an example to better understand how this works:
    !%
    !%  <br>%<tt>SystemA.Interactions
    !%   <br>&nbsp;&nbsp;gravity | all_except | "SystemB"
    !%  <br>%</tt>
    !%
    !% This means that SystemA and all the systems that belong to the same
    !% namespace (i.e., all its subsystems) will interact through gravity with
    !% all interaction partners that are also able to interact through gravity,
    !% except with SystemB. Note that the opposite is not true so, although
    !% clearly unphysical, this will not prevent SystemB from feeling the
    !% gravity from SystemA (in <tt>Octopus</tt> the interactions are always
    !% one-sided).
    !%
    !% NB: Each interaction type should only appear once in the block. Any
    !% further instances beyond the first will be ignored.
    !%
    !% Available modes and interaction types:
    !%Option no_partners -1
    !%  (interaction mode)
    !% Do not interact with any partner.
    !%Option all_partners -2
    !%  (interaction mode)
    !% Interact with all available partners.
    !%Option only_partners -3
    !%  (interaction mode)
    !% Interact only with some specified partners. A list of partner names must
    !% be given.
    !%Option all_except -4
    !%  (interaction mode)
    !% Interact with all available partners except with some specified
    !% partners. A list of partner names to exclude must be given.
    !%Option gravity 1
    !%  (interaction type)
    !% Gravity interaction between two masses.
    !%Option lorentz_force 2
    !%  (interaction type)
    !% Lorentz force resulting from an EM field acting on a moving charge.
    !%Option coulomb_force 3
    !%  (interaction type)
    !% Coulomb force between two charged particles.
    !%Option linear_medium_to_em_field 4
    !%  (interaction type)
    !% Linear medium for propagation of EM fields.
    !%Option current_to_mxll_field 5
    !%  (interaction type)
    !% Drude dispersive linear medium for propagation of EM fields.
    !%Option maxwell_e_field 6
    !%  (interaction type)
    !% Electric field resulting from the Maxwell solver.
    !%Option maxwell_b_field 7
    !%  (interaction type)
    !% Magnetic field resulting from the Maxwell solver.
    !%Option maxwell_vector_potential 8
    !%  (interaction type)
    !% Vector potential resulting from the Maxwell solver.
    !%Option lennard_jones 9
    !%  (interaction type)
    !% Force resulting from a Lennard Jones potential between classical particles.
    !%End
    if (parse_block(namespace, "Interactions", blk) == 0) then
      ! Loop over all interactions specified in the input file
      do line = 0, parse_block_n(blk) - 1
        ! Read the interaction type (first column)
        call parse_block_integer(blk, line, 0, type)

        ! Sanity check: the interaction type must be known and must not be mistaken for an interaction mode
        if (.not. varinfo_valid_option("Interactions", type) .or. &
          any(type == [ALL_PARTNERS, ONLY_PARTNERS, NO_PARTNERS, ALL_EXCEPT])) then
          call messages_input_error(namespace, "Interactions", details="Unknown interaction type", row=line, column=0)
        end if

        ! Is this interaction in the list of provided interactions?
        i = findloc(interactions, type, dim=1)
        if (i == 0) cycle

        ! Read how this interaction should be treated (second column)
        call parse_block_integer(blk, line, 1, options(i)%mode)
        if (all(options(i)%mode /= [ALL_PARTNERS, ONLY_PARTNERS, NO_PARTNERS, ALL_EXCEPT])) then
          call messages_input_error(namespace, "Interactions", "Unknown interaction mode", row=line, column=1)
        end if

        select case (options(i)%mode)
        case (ONLY_PARTNERS, ALL_EXCEPT)
          ! In these two cases we need to read the names of the selected
          ! partners (remaining columns) and handled them appropriately
          allocate(options(i)%partners(parse_block_cols(blk, line) - 2))
          do col = 2, parse_block_cols(blk, line) - 1
            call parse_block_string(blk, line, col, options(i)%partners(col-1))
          end do
        end select
      end do
      call parse_block_end(blk)
    end if

  end function interactions_factory_options

end module interactions_factory_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
