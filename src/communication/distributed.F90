!! Copyright (C) 2008-2016 X. Andrade
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module distributed_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use mpi_oct_m
  use multicomm_oct_m
  use profiling_oct_m

  implicit none

  private

  public ::                      &
    distributed_t,               &
    distributed_nullify,         &
    distributed_init,            &
    distributed_copy,            &
    distributed_end,             &
    distributed_allgather

  ! TODO(Alex) Create an issue for refactoring type distributed_t attributes
  ! `distributed_nullify` is a meaningless routine - look at removing/consolidating
  ! `distributed_allgather` is pointless and should be scrapped
  !   - Only called in xc_vxc_inc.F90

  !> @brief Distribution of N instances over mpi_grp%size processes, for the local rank mpi_grp%rank.
  !! Distribution for all other processes of mpi_grp%comm are stored in node, num and range.
  !!
  !! @TODO
  !!  * node could more meaningfully be named process, and be replaced with a function
  !!  * start, end, nlocal, nglobal and node are all superfluous. One could:
  !!    * Replace nlocal with a simple function
  !!    * Replace nglobal with a simple function
  !!    * Replace num with a simple function
  type distributed_t
    integer              :: start    = 1       !< Start index of distributed quantity on process mpi_grp%rank
    integer              :: end      = 0       !< End index of distributed quantity on process mpi_grp%rank
    integer              :: nlocal   = 0       !< TODO(Alex) Replace with (end - start + 1) OR num(mpi_grp%rank)
    !                                             or (range(2, mpi_grp%rank) - range(1, mpi_grp%rank) + 1)
    integer              :: nglobal  = 0       !< TODO(Alex) Replace with range(2, mpi_grp%size) - range(1, 0) + 1
    logical              :: parallel = .false. !< Is the quantity distributed
    integer, allocatable :: node(:)            !< Map quantity global index to its process, mpi_grp%rank
    integer, allocatable :: range(:, :)        !< Start and end indices associated with each process
    !                                             dim(2, 0:n_processes-1)
    !                                             Defined the same for all processes of mpi_grp
    integer, allocatable :: num(:)             !< Number of distributed quantities on each process of mpi_grp
    !                                             dim(mpi_grp%size)
    type(mpi_grp_t)      :: mpi_grp            !< MPI comm plus basic info
  end type distributed_t

contains

  subroutine distributed_nullify(this, total)
    type(distributed_t), intent(out) :: this
    integer, optional,   intent(in)  :: total

    PUSH_SUB(distributed_nullify)

    if (present(total)) then
      this%end             = total
      this%nlocal          = total
      this%nglobal         = total
    end if

    POP_SUB(distributed_nullify)
  end subroutine distributed_nullify


  !> @brief Distribute N instances across M processes of communicator `comm`
  subroutine distributed_init(this, total, comm, tag, scalapack_compat)
    type(distributed_t),        intent(out) :: this
    integer,                    intent(in)  :: total  !< Size of quantity to distribute
    type(MPI_Comm),             intent(in)  :: comm   !< mpi communicator
    character(len=*), optional, intent(in)  :: tag
    logical,          optional, intent(in)  :: scalapack_compat

    integer :: i

    PUSH_SUB(distributed_init)

    this%nglobal = total
    SAFE_ALLOCATE(this%node(1:this%nglobal))

    call mpi_grp_init(this%mpi_grp, comm)
    SAFE_ALLOCATE(this%num(0:this%mpi_grp%size - 1))
    SAFE_ALLOCATE(this%range(1:2, 0:this%mpi_grp%size - 1))

    ! Defaults
    if (this%mpi_grp%size == 1 .or. this%nglobal == 1) then
      this%node(1:total)   = 0
      this%start           = 1
      this%end             = total
      this%nlocal          = total
      this%parallel        = .false.
      this%range(:, 0)     = [1, total]
      this%num(0)          = total
      call mpi_grp_init(this%mpi_grp, MPI_COMM_UNDEFINED)

    else
      this%parallel = .true.

      call multicomm_divide_range(this%nglobal, this%mpi_grp%size, this%range(1, :), this%range(2, :), &
        lsize = this%num, scalapack_compat = scalapack_compat)

      this%start  = this%range(1, this%mpi_grp%rank)
      this%end    = this%range(2, this%mpi_grp%rank)
      this%nlocal = this%num(this%mpi_grp%rank)

      do i = 0, this%mpi_grp%size - 1
        this%node(this%range(1, i):this%range(2, i)) = i
      end do

      if (present(tag)) then
        message(1) = 'Info: Parallelization in ' // trim(tag)
        call messages_info(1)
        do i = 0, this%mpi_grp%size - 1
          write(message(1),'(a,i4,a,i6,a)') 'Info: Node in group ', i, &
            ' will manage ', this%num(i), ' '//trim(tag)
          if (this%num(i) > 0) then
            write(message(1),'(a,a,i6,a,i6)') trim(message(1)), ':', this%range(1, i), " - ", this%range(2, i)
          end if
          call messages_info(1)
        end do
      end if

    end if

    POP_SUB(distributed_init)
  end subroutine distributed_init


  !> @Brief Create a copy of a distributed instance
  subroutine distributed_copy(in, out)
    type(distributed_t), intent(in)    :: in
    type(distributed_t), intent(inout) :: out

    integer :: size

    PUSH_SUB(distributed_copy)

    call distributed_end(out)

    out%start    = in%start
    out%end      = in%end
    out%nlocal   = in%nlocal
    out%nglobal  = in%nglobal
    out%parallel = in%parallel

    ! TODO(Alex) This is a weird choice, could retreive from size(num), for example
    size = in%mpi_grp%size

    call mpi_grp_init(out%mpi_grp, in%mpi_grp%comm)

    if (allocated(in%node)) then
      SAFE_ALLOCATE(out%node(1:in%nglobal))
      out%node(1:in%nglobal) = in%node(1:in%nglobal)
    end if

    if (allocated(in%range)) then
      SAFE_ALLOCATE(out%range(1:2, 0:size - 1))
      out%range(1:2, 0:size - 1) = in%range(1:2, 0:size - 1)
    end if

    if (allocated(in%num)) then
      SAFE_ALLOCATE(out%num(0:size - 1))
      out%num(0:size - 1) = in%num(0:size - 1)
    end if

    POP_SUB(distributed_copy)
  end subroutine distributed_copy


  subroutine distributed_end(this)
    type(distributed_t), intent(inout) :: this

    PUSH_SUB(distributed_end)

    SAFE_DEALLOCATE_A(this%node)
    SAFE_DEALLOCATE_A(this%range)
    SAFE_DEALLOCATE_A(this%num)

    POP_SUB(distributed_end)
  end subroutine distributed_end


  subroutine distributed_allgather(this, aa)
    type(distributed_t), intent(in)    :: this
    real(real64), contiguous,   intent(inout) :: aa(:)

    integer, allocatable :: displs(:)

    if (.not. this%parallel) return

    PUSH_SUB(distributed_allgather)

    SAFE_ALLOCATE(displs(0:this%mpi_grp%size - 1))

    displs(0:this%mpi_grp%size - 1) = this%range(1, 0:this%mpi_grp%size - 1) - 1

#ifdef HAVE_MPI
    call MPI_Allgatherv(MPI_IN_PLACE, this%nlocal, MPI_DOUBLE_PRECISION, &
      aa(1), this%num, displs, MPI_DOUBLE_PRECISION, this%mpi_grp%comm, mpi_err)
#endif

    SAFE_DEALLOCATE_A(displs)

    POP_SUB(distributed_allgather)
  end subroutine distributed_allgather

end module distributed_oct_m


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
