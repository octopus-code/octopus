!!! Copyright (C) 2008-2009 David Strubbe
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

! ---------------------------------------------------------
!> Computes the effective mass tensor
!!
!! This is obtained using the formula
!! \f[
!! m^-1[ij] = <psi0|H2ij|psi0> + 2*Re<psi0|H'i|psi'j>
!! \f]
!! for each state, spin, and k-point
!! The off-diagonal elements are not correct in a degenerate subspace
subroutine X(calc_eff_mass_inv)(namespace, space, gr, st, hm, lr, pert, eff_mass_inv, degen_thres)
  type(namespace_t),        intent(in)    :: namespace
  class(space_t),           intent(in)    :: space
  type(grid_t),             intent(in)    :: gr
  type(states_elec_t),      intent(in)    :: st
  type(hamiltonian_elec_t), intent(inout) :: hm
  type(lr_t),               intent(in)    :: lr(:,:) !< (1, pdim)
  class(perturbation_t),    intent(inout) :: pert
  real(real64),             intent(out)   :: eff_mass_inv(:,:,:,:) !< (pdim, pdim, nik, nst)
  real(real64),             intent(in)    :: degen_thres

  integer :: ik, ist, ist2, idir1, idir2, pdim
  R_TYPE :: term
  R_TYPE, allocatable   :: psi(:, :)
  R_TYPE, allocatable   :: pertpsi(:,:,:)     ! H`i|psi0>
  R_TYPE, allocatable   :: pertpsi2(:,:)      ! H2i|psi0>
  R_TYPE, allocatable   :: proj_dl_psi(:,:)   ! (1-Pn`)|psi`j>
  logical, allocatable  :: orth_mask(:)

  PUSH_SUB(X(calc_eff_mass_inv))

  pdim = space%periodic_dim

  SAFE_ALLOCATE(psi(1:gr%np_part, 1:hm%d%dim))
  SAFE_ALLOCATE(pertpsi(1:gr%np, 1:hm%d%dim, 1:pdim))
  SAFE_ALLOCATE(pertpsi2(1:gr%np, 1:hm%d%dim))
  SAFE_ALLOCATE(proj_dl_psi(1:gr%np, 1:hm%d%dim))
  SAFE_ALLOCATE(orth_mask(1:st%nst))

  eff_mass_inv(:,:,:,:) = M_ZERO

  do ik = st%d%kpt%start, st%d%kpt%end

    do ist = st%st_start, st%st_end

      call states_elec_get_state(st, gr, ist, ik, psi)

      ! start by computing all the wavefunctions acted on by perturbation
      do idir1 = 1, pdim
        call pert%setup_dir(idir1)
        call pert%X(apply)(namespace, space, gr, hm, ik, psi, pertpsi(:, :, idir1))
      end do

      do idir2 = 1, pdim
        do idir1 = 1, pdim

          if (idir1 < idir2) then
            eff_mass_inv(idir1, idir2, ist, ik) = eff_mass_inv(idir2, idir1, ist, ik)
            ! utilizing symmetry of inverse effective mass tensor
            cycle
          end if

          call lalg_copy(gr%np, hm%d%dim, lr(1, idir2)%X(dl_psi)(:, :,  ist, ik), proj_dl_psi)

          ! project out components of other states in degenerate subspace
          do ist2 = 1, st%nst
!              alternate direct method
!              if (abs(st%eigenval(ist2, ik) - st%eigenval(ist, ik)) < degen_thres) then
!                   proj_dl_psi(1:gr%np) = proj_dl_psi(1:mesh%np) - st%X(psi)(1:gr%np, 1, ist2, ik) * &
!                     X(mf_dotp)(m, st%X(psi)(1:gr%np, 1, ist2, ik), proj_dl_psi(1:gr%np))
            orth_mask(ist2) = .not. (abs(st%eigenval(ist2, ik) - st%eigenval(ist, ik)) < degen_thres)
            ! mask == .false. means do projection; .true. means do not
          end do

!            orth_mask(ist) = .true. ! projection on unperturbed wfn already removed in Sternheimer eqn

          call X(states_elec_orthogonalize_single)(st, gr, st%nst, ik, proj_dl_psi, mask = orth_mask)

          ! contribution from Sternheimer equation
          term = X(mf_dotp)(gr, st%d%dim, proj_dl_psi, pertpsi(:, :, idir1))
          eff_mass_inv(idir1, idir2, ist, ik) = M_TWO * R_REAL(term)

          call pert%setup_dir(idir1, idir2)
          call pert%X(apply_order_2)(namespace, space, gr, hm, ik, psi, pertpsi2)
          eff_mass_inv(idir1, idir2, ist, ik) = &
            eff_mass_inv(idir1, idir2, ist, ik) - R_REAL(X(mf_dotp)(gr, hm%d%dim, psi, pertpsi2))

        end do !idir2
      end do !idir1
    end do !ist
  end do !ik

  if (st%parallel_in_states .or. st%d%kpt%parallel) then
    call comm_allreduce(st%st_kpt_mpi_grp, eff_mass_inv)
  end if

  SAFE_DEALLOCATE_A(psi)
  SAFE_DEALLOCATE_A(pertpsi)
  SAFE_DEALLOCATE_A(pertpsi2)
  SAFE_DEALLOCATE_A(proj_dl_psi)
  SAFE_DEALLOCATE_A(orth_mask)

  POP_SUB(X(calc_eff_mass_inv))
end subroutine X(calc_eff_mass_inv)


! ---------------------------------------------------------
!> add projection onto occupied states, by sum over states
subroutine X(kdotp_add_occ)(namespace, space, gr, st, hm, pert, kdotp_lr, degen_thres)
  type(namespace_t),        intent(in)    :: namespace
  class(space_t),           intent(in)    :: space
  type(grid_t),             intent(in)    :: gr
  type(states_elec_t),      intent(in)    :: st
  type(hamiltonian_elec_t), intent(inout) :: hm
  class(perturbation_t),    intent(in)    :: pert
  type(lr_t),               intent(inout) :: kdotp_lr
  real(real64),             intent(in)    :: degen_thres

  integer :: ist, ist2, ik
  R_TYPE :: mtxel
  R_TYPE, allocatable :: pertpsi(:, :), psi1(:, :), psi2(:, :)

  PUSH_SUB(X(kdotp_add_occ))

  if (st%parallel_in_states) then
    call messages_not_implemented("kdotp_add_occ parallel in states", namespace=namespace)
  end if

  SAFE_ALLOCATE(psi1(1:gr%np_part, 1:st%d%dim))
  SAFE_ALLOCATE(psi2(1:gr%np_part, 1:st%d%dim))
  SAFE_ALLOCATE(pertpsi(1:gr%np, 1:st%d%dim))

  do ik = st%d%kpt%start, st%d%kpt%end
    do ist = 1, st%nst

      call states_elec_get_state(st, gr, ist, ik, psi1)

      call pert%X(apply)(namespace, space, gr, hm, ik, psi1, pertpsi)

      do ist2 = ist + 1, st%nst

        call states_elec_get_state(st, gr, ist2, ik, psi2)

        ! avoid dividing by zero below; these contributions are arbitrary anyway
        if (abs(st%eigenval(ist2, ik) - st%eigenval(ist, ik)) < degen_thres) cycle

        ! the unoccupied subspace was handled by the Sternheimer equation
        if (st%occ(ist2, ik) < M_HALF) cycle

        mtxel = X(mf_dotp)(gr, st%d%dim, psi2, pertpsi(:, :))

        call lalg_axpy(gr%np, st%d%dim, mtxel/(st%eigenval(ist, ik) - st%eigenval(ist2, ik)), &
          psi2, kdotp_lr%X(dl_psi)(:,:, ist, ik))
        ! note: there is a minus sign here, because the perturbation is an anti-Hermitian operator
        call lalg_axpy(gr%np, st%d%dim, R_CONJ(-mtxel)/(st%eigenval(ist2, ik) - st%eigenval(ist, ik)), &
          psi1, kdotp_lr%X(dl_psi)(:, :, ist2, ik))
      end do
    end do
  end do

  SAFE_DEALLOCATE_A(psi1)
  SAFE_DEALLOCATE_A(psi2)
  SAFE_DEALLOCATE_A(pertpsi)

  POP_SUB(X(kdotp_add_occ))
end subroutine X(kdotp_add_occ)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
