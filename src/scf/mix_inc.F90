!! Copyright (C) 2002-2015 M. Marques, A. Castro, A. Rubio, G. Bertsch, X. Andrade
!! Copyright (C) 2021 N. Tancogne-Dejean
!! Copyright (C) 2024 A. Buccheri
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

!> @brief Mix the input and output potentials (or densities) according to some scheme.
!!
!! Whether \f$v\f$ is the potential or the density is determined at run time.
subroutine X(mixing)(namespace, smix, vin, vout, vnew)
  type(namespace_t),   intent(in)    :: namespace      !< Namespace
  type(mix_t), target, intent(inout) :: smix           !< Mixing settings and derivatives
  R_TYPE, contiguous,  intent(in)    :: vin(:, :)      !< Input potential
  R_TYPE, contiguous,  intent(in)    :: vout(:, :)     !< Output potential
  R_TYPE, contiguous,  intent(out)   :: vnew(:, :)     !< Updated potential

  R_TYPE,              allocatable   :: residual(:, :) !< Residual
  integer                            :: d1, d2         !< Potential (and residual) dimensions

  PUSH_SUB(X(mixing))
  ASSERT(associated(smix%der))

  smix%iter = smix%iter + 1

  ! Global variable assignment, required for any scheme using Kerker preconditioning
  if (smix%kerker) mesh_aux => smix%der%mesh

  d1 = smix%mixfield%d1
  d2 = smix%mixfield%d2

  SAFE_ALLOCATE(residual(1:d1, 1:d2))
  call X(compute_residual)(smix, [d1, d2], vin, vout, residual)

  select case (smix%scheme)
  case (OPTION__MIXINGSCHEME__LINEAR)
    call X(mixing_linear)(d1, d2, smix%coeff, vin, residual, vnew)
    call linear_mixing_aux_field(smix)

  case (OPTION__MIXINGSCHEME__BROYDEN)
    call X(mixing_broyden)(namespace, smix, residual, vin, vnew, smix%iter)

  case (OPTION__MIXINGSCHEME__DIIS)
    call X(mixing_diis)(smix, vin, residual, vnew, smix%iter)

  end select

  SAFE_DEALLOCATE_A(residual)

  POP_SUB(X(mixing))
end subroutine X(mixing)


!> @brief Linear mixing of the input and output potentials.
!!
!! Linear mixing is defined as:
!! \f[
!!     V_{\text{new}} = (1 - a) V_{\text{in}} + a V_{\text{out}}.
!! \f]
!! Using the definition of the residual, f$ V_{\text{out}} = V_{\text{in}} + R\f$,
!! the linear mixing expression can be equivalently written as:
!! \f[
!!     V_{\text{new}} = V_{\text{in}} + a R.
!! \f]
subroutine X(mixing_linear)(d1, d2, coeff, vin, residual, vnew)
  integer,      intent(in) :: d1, d2          !< Limits for each dimension of the potential
  real(real64), intent(in) :: coeff(:)        !< Mixing coefficient
  R_TYPE,       intent(in) :: vin(:, :)       !< Input potential
  R_TYPE,       intent(in) :: residual(:, :)  !< Residual
  R_TYPE,       intent(out):: vnew(:, :)      !< Updated potential

  integer :: id1, id2

  PUSH_SUB(X(mixing_linear))

  !$omp parallel private(id1)
  do id2 = 1, d2
    !$omp do simd
    do id1 = 1, d1
      vnew(id1, id2) = vin(id1, id2) + coeff(id2) * residual(id1, id2)
    end do
    !$omp end do simd nowait
  end do
  !$omp end parallel

  POP_SUB(X(mixing_linear))
end subroutine X(mixing_linear)


!> @brief Compute the residual of the potential (or density) for SCF mixing.
!!
!! In the absence of any preconditioning, the residual is defined as \f$ V_\text{out} - V_\text{in}\f$.
!! If preconditioning is used, please refer to the specific scheme for the definition.
!!
!! Note, d1, d2 are passed in because this routine should be compatible with both the potential and
!! and the auxiliary fields (which have differing sizes).
subroutine X(compute_residual)(smix, dims, vin, vout, residual)
  type(mix_t),         intent(in ) :: smix           !< Mixing settings and derivatives
  integer,             intent(in ) :: dims(2)        !< Dimensions of the terms to mix
  R_TYPE,              intent(in ) :: vin(:, :)      !< Input potential
  R_TYPE,              intent(in ) :: vout(:, :)     !< Output potential
  R_TYPE, contiguous,  intent(out) :: residual(:, :) !< Residual

  R_TYPE,              allocatable :: precond_residual(:, :) !< Work array for preconditioned residual

  integer                          :: d1, d2, id1, id2

  PUSH_SUB(X(compute_residual))

  d1 = dims(1)
  d2 = dims(2)

  !$omp parallel private(id1)
  do id2 = 1, d2
    !$omp do simd
    do id1 = 1, d1
      residual(id1, id2) = vout(id1, id2) - vin(id1, id2)
    end do
    !$omp end do simd nowait
  end do
  !$omp end parallel

  if (smix%kerker) then
    ! Must be assigned when performing derivatives
    ASSERT(associated(smix%der))
    ASSERT(associated(mesh_aux))
    SAFE_ALLOCATE(precond_residual(1:d1, 1:d2))

    do id2 = 1, d2
      call X(kerker_preconditioner)(smix%der, smix%kerker_factor, residual(:, id2), &
        precond_residual(:, id2))
    end do

    call lalg_copy(d1, d2, precond_residual, residual)
    SAFE_DEALLOCATE_A(precond_residual)
  endif

  POP_SUB(X(compute_residual))

end subroutine X(compute_residual)


!> @brief Top-level routine for Broyden mixing.
!!
!! Performs updating of `smix%mixfield` attributes.
!! Whether \f$v\f$ is the potential or the density is determined at run time.
subroutine X(mixing_broyden)(namespace, smix, f, vin, vnew, iter)
  type(namespace_t),  intent(in)    :: namespace   !< Namespace
  type(mix_t),        intent(inout) :: smix        !< Mixing settings and derivatives
  R_TYPE, contiguous, intent(in)    :: f(:, :)     !< Residual
  R_TYPE, contiguous, intent(in)    :: vin(:, :)   !< Input potential
  R_TYPE, contiguous, intent(out)   :: vnew(:, :)  !< Updated potential
  integer,            intent(in)    :: iter        !< Current SCF iteration

  integer :: ipos, iter_used
  integer :: d1, d2                               !<  Potential (and residual) dimensions

  PUSH_SUB(X(mixing_broyden))

  d1 = smix%mixfield%d1
  d2 = smix%mixfield%d2

  ! According to Johnson (1988), df and dv should be normalized here.
  ! However, it turns out that this blocks convergence to very high
  ! accuracies. Also, the normalization is not present in the description
  ! by Kresse & Furthmueller (1996). Thus, we do not normalize here.
  if (iter > 1) then
    ! Store df and dv from current iteration
    ipos = mod(smix%last_ipos, smix%ns) + 1
    smix%ipos = ipos

    call lalg_copy(d1, d2, f, smix%mixfield%X(df)(:, :, ipos))
    call lalg_copy(d1, d2, vin, smix%mixfield%X(dv)(:, :, ipos))
    call lalg_axpy(d1, d2, -M_ONE, smix%mixfield%X(f_old),   smix%mixfield%X(df)(:, :, ipos))
    call lalg_axpy(d1, d2, -M_ONE, smix%mixfield%X(vin_old), smix%mixfield%X(dv)(:, :, ipos))

    smix%last_ipos = ipos
  end if

  ! Store residual and vin for next iteration
  call lalg_copy(d1, d2, f,   smix%mixfield%X(f_old))
  call lalg_copy(d1, d2, vin, smix%mixfield%X(vin_old))

  ! extrapolate new vector
  iter_used = min(iter - 1, smix%ns)
  call X(broyden_extrapolation)(smix, namespace, smix%coeff, d1, d2, &
    vin, vnew, iter_used, f, smix%mixfield%X(df), smix%mixfield%X(dv))

  POP_SUB(X(mixing_broyden))
end subroutine X(mixing_broyden)


!> @brief Broyden mixing implementation.
subroutine X(broyden_extrapolation)(this, namespace, coeff, d1, d2, vin, vnew, iter_used, f, df, dv)
  type(mix_t),       intent(inout) :: this                           !< Mixing settings and derivatives
  type(namespace_t), intent(in)    :: namespace                      !< Namespace
  real(real64),      intent(in)    :: coeff(:)                       !< Mixing coefficient
  integer,           intent(in)    :: d1, d2                         !< Dimensions of the potential and residual
  integer,           intent(in)    :: iter_used                      !< Number of iterations to use in the mixing
  R_TYPE,            intent(in)    :: f(:, :), vin(:, :)             !< Residual and potential for current iteration
  R_TYPE,            intent(in)    :: df(:, :, :), dv(:, :, :)       !< Residual and potential from size(df, 4) iterations
  R_TYPE,            intent(out)   :: vnew(:, :)                     !< Updated potential

  ! The parameter ww does not influence the mixing (see description
  ! in Kresse & Furthmueller 1996). Thus we choose it to be one.
  real(real64), parameter :: ww = M_ONE
  integer  :: i, k
  R_TYPE   :: gamma
  R_TYPE, allocatable :: beta(:, :), work(:)

  PUSH_SUB(X(broyden_extrapolation))

  if (iter_used == 0) then
    ! linear mixing first
    call X(mixing_linear)(d1, d2, coeff, vin, f, vnew)

    ! Auxilliary fields
    call this%compute_residuals_aux_field()

    do i = 1, this%nauxmixfield
      if (this%auxmixfield(i)%p%func_type == TYPE_FLOAT) then
        call dbroyden_extrapolation_aux(this, namespace, i, coeff, iter_used)
      else
        call zbroyden_extrapolation_aux(this, namespace, i, coeff, iter_used)
      end if
    end do

    POP_SUB(X(broyden_extrapolation))
    return
  end if

  SAFE_ALLOCATE(beta(1:iter_used, 1:iter_used))
  SAFE_ALLOCATE(work(1:iter_used))

  ! compute matrix beta, Johnson eq. 13a
  call X(mixing_build_matrix)(this, df, iter_used, d2, ww*ww, beta)

  ! invert matrix beta
  call lalg_inverse(iter_used, beta, 'dir')

  do i = 1, iter_used
    work(i) = M_ZERO
    do k = 1, d2
      work(i) = work(i) + X(mix_dotp)(this, df(:, k, i), f(:, k), reduce = .false.)
    end do
    if (this%der%mesh%parallel_in_domains) call this%der%mesh%allreduce(work(i))
  end do

  ! linear mixing term
  call X(mixing_linear)(d1, d2, coeff, vin, f, vnew)

  ! other terms
  do i = 1, iter_used
    gamma = ww*sum(beta(:, i)*work(:))
    do k = 1, d2
      vnew(1:d1, k) = vnew(1:d1, k) - ww*gamma*(coeff(k)*df(1:d1, k, i) + dv(1:d1, k, i))
    end do
  end do

  ! Auxilliary fields
  call this%compute_residuals_aux_field()

  do i = 1, this%nauxmixfield
    if (this%auxmixfield(i)%p%func_type == TYPE_FLOAT) then
      call dbroyden_extrapolation_aux(this, namespace, i, coeff, iter_used, X(beta)=beta, X(work)=work)
    else
      call zbroyden_extrapolation_aux(this, namespace, i, coeff, iter_used, X(beta)=beta, X(work)=work)
    end if
  end do

  SAFE_DEALLOCATE_A(beta)
  SAFE_DEALLOCATE_A(work)

  POP_SUB(X(broyden_extrapolation))

end subroutine X(broyden_extrapolation)


!> @brief Broyden mixing for auxilliary fields.
!!
!! Auxilliary fields "follow" the direction of the mixing, but do not influence the direction.
!! Auxilliary fields are passed as an attribute of a mix_t instance
!!
!! @note
!! The design of this routine would be more consistent if `mf` (better-named `aux_field`) was passed in,
!! then a wrapper method performs the iteration over the N aux fields, like:
!!
!! ```fortran
!! do i = 1, this%nauxmixfield
!!   aux_field => this%auxmixfield(i)%
!!   if (field%func_type == TYPE_FLOAT) then
!!     call dbroyden_extrapolation_aux(this, namespace, aux_field, dbeta=beta, dwork=dwork)
!!   else
!!     call zbroyden_extrapolation_aux(this, namespace, aux_field, zbeta=beta, zwork=dwork)
!!   end if
!! end do
!! rather than passing `ii` in.
!! ```
subroutine X(broyden_extrapolation_aux)(this, namespace, ii, coeff, iter_used, dbeta, dwork, zbeta, zwork)
  type(mix_t),     intent(inout) :: this
  type(namespace_t),  intent(in) :: namespace
  integer,            intent(in) :: ii
  real(real64),       intent(in) :: coeff(:)
  integer,            intent(in) :: iter_used
  real(real64), optional,    intent(in) :: dbeta(:, :), dwork(:)
  complex(real64), optional,    intent(in) :: zbeta(:, :), zwork(:)

  real(real64), parameter :: ww = M_ONE
  integer :: d1, d2, ipos, i, k
  R_TYPE  :: gamma
  type(mixfield_t), pointer :: mf

  PUSH_SUB(X(broyden_extrapolation_aux))

#ifdef R_TREAL
  ! We cannot have a complex auxiliary field being mixed along with a real field
  ASSERT(this%auxmixfield(ii)%p%func_type == TYPE_FLOAT)
#endif

  mf => this%auxmixfield(ii)%p

  d1 = mf%d1
  d2 = mf%d2

  if (this%iter > 1) then
    ! Store df and dv from current iteration
    ipos = this%ipos

    call lalg_copy(d1, d2, mf%X(residual)(:, :), mf%X(df)(:, :, ipos))
    call lalg_copy(d1, d2, mf%X(vin)(:, :), mf%X(dv)(:, :, ipos))
    call lalg_axpy(d1, d2, -M_ONE, mf%X(f_old)(:, :), mf%X(df)(:, :, ipos))
    call lalg_axpy(d1, d2, -M_ONE, mf%X(vin_old)(:, :), mf%X(dv)(:, :, ipos))
  end if

  ! Store vin and residual for next iteration
  call lalg_copy(d1, d2, mf%X(vin),      mf%X(vin_old))
  call lalg_copy(d1, d2, mf%X(residual), mf%X(f_old))

  ! linear mixing term
  call X(mixing_linear)(d1, d2, coeff, mf%X(vin), mf%X(residual), mf%X(vnew))

  if (iter_used == 0) then
    !We stop here
    POP_SUB(X(broyden_extrapolation_aux))
    return
  end if

  ! other terms
  do i = 1, iter_used
    !Here gamma might be of a different type than the main mixfield type, so we convert it to the proper type
    if (present(dbeta) .and. present(dwork)) then
      gamma = ww*sum(dbeta(:, i)*dwork(:))
    else if (present(zbeta) .and. present(zwork)) then
#ifdef R_TCOMPLX
      gamma = ww*sum(zbeta(:, i)*zwork(:))
#else
      ASSERT(.false.)
#endif
    else
      write(message(1), '(a)') 'Internal error in broyden_extrapolation_aux'
      call messages_fatal(1, namespace=namespace)
    end if
    do k = 1, d2
      mf%X(vnew)(1:d1, k) = mf%X(vnew)(1:d1, k) &
        - ww*gamma*(coeff(k)*mf%X(df)(1:d1, k, i) + mf%X(dv)(1:d1, k, i))
    end do
  end do

  nullify(mf)

  POP_SUB(X(broyden_extrapolation_aux))
endsubroutine X(broyden_extrapolation_aux)

!> @brief Direct inversion in the iterative subspace.
!!
!! For implementation details, please refer to:
!! * Pulay. [Convergence acceleration of iterative sequences the case of scf iteration](https://doi.org/10.1016/0009-2614(80)80396-4)
!! * G. Kresse, and J. Furthmueller. [Efficient iterative schemes for ab initio total-energy
!!   calculations using a plane-wave basis set](https://doi.org/10.1103/PhysRevB.54.11169)
subroutine X(mixing_diis)(this, vin, residual, vnew, iter)
  type(mix_t),          intent(inout) :: this
  R_TYPE, contiguous,   intent(in)    :: vin(:, :)
  R_TYPE, contiguous,   intent(in)    :: residual(:, :)
  R_TYPE,               intent(out)   :: vnew(:, :)
  integer,              intent(in)    :: iter

  integer :: size, ii
  integer :: d1, d2
  R_TYPE :: sumalpha
  R_TYPE, allocatable :: aa(:, :), alpha(:), rhs(:)
  real(real64) :: scaling

  PUSH_SUB(X(mixing_diis))

  d1 = this%mixfield%d1
  d2 = this%mixfield%d2

  if (iter <= this%mixfield%d3) then
    size = iter

  else
    size = this%mixfield%d3

    do ii = 2, size
      call lalg_copy(d1, d2, this%mixfield%X(dv)(:, :, ii), this%mixfield%X(dv)(:, :, ii - 1))
      call lalg_copy(d1, d2, this%mixfield%X(df)(:, :, ii), this%mixfield%X(df)(:, :, ii - 1))
    end do

  end if

  call lalg_copy(d1, d2, vin,      this%mixfield%X(dv)(:, :, size))
  call lalg_copy(d1, d2, residual, this%mixfield%X(df)(:, :, size))

  if (iter == 1 .or. mod(iter, this%interval) /= 0) then
    ! Linear mixing
    call X(mixing_linear)(d1, d2, this%coeff, vin, residual, vnew)
    POP_SUB(X(mixing_diis))
    return
  end if

  SAFE_ALLOCATE(aa(1:size + 1, 1:size + 1))
  SAFE_ALLOCATE(alpha(1:size + 1))
  SAFE_ALLOCATE(rhs(1:size + 1))

  call X(mixing_build_matrix)(this, this%mixfield%X(df), size, d2, M_ONE, aa)

  ! Matrix elements of aa can be small. If there are of machine epsilon magnitude,
  ! the code breaks as we insert 1.0 in the matrix.
  ! This is trivially solved by a rescaling of the matrix
  scaling = maxval(abs(aa(1:size, 1:size)))
  aa(1:size, 1:size)  = aa(1:size, 1:size) / scaling

  aa(1:size, size + 1) = -M_ONE
  aa(size + 1, 1:size) = -M_ONE
  aa(size + 1, size + 1) = M_ZERO

  rhs(1:size) = M_ZERO
  rhs(size + 1) = -M_ONE

  call lalg_least_squares(size + 1, aa, rhs, alpha, preserve_mat=.false.)

  alpha = alpha * scaling

  sumalpha = sum(alpha(1:size))
  alpha = alpha/sumalpha

  vnew(1:d1, 1:d2) = M_ZERO

  do ii = 1, size
    vnew(1:d1, 1:d2) = vnew(1:d1, 1:d2) &
      + alpha(ii) * (this%mixfield%X(dv)(1:d1, 1:d2, ii) &
      + this%residual_coeff * this%mixfield%X(df)(1:d1, 1:d2, ii))
  end do

  POP_SUB(X(mixing_diis))
end subroutine X(mixing_diis)


! --------------------------------------------------------------
! In all mixing schemes apart from the linear mixing one, we need to build the same matrix
! This is done in this routine, together with the stabilization scheme proposed by Johnson (1988)
subroutine X(mixing_build_matrix)(this, df, size, d2, ww, beta)
  type(mix_t),        intent(in)  :: this
  R_TYPE,             intent(in)  :: df(:,:,:)
  integer,            intent(in)  :: size, d2
  real(real64),       intent(in)  :: ww
  R_TYPE, contiguous, intent(out) :: beta(:,:)

  integer :: i, j, k
  real(real64) :: w0

  PUSH_SUB(X(mixing_build_matrix))

  beta = M_ZERO
  do i = 1, size
    do j = i, size
      beta(i, j) = M_ZERO
      do k = 1, d2
        beta(i, j) = beta(i, j) + ww*X(mix_dotp)(this, df(:, k, j), df(:, k, i), reduce = .false.)
      end do
    end do
  end do
  call upper_triangular_to_hermitian(size, beta)

  if (this%der%mesh%parallel_in_domains) call this%der%mesh%allreduce(beta)

  ! According to Johnson (1988), w0 is chosen as 0.01. Because we do not
  ! normalize the components, we need to choose w0 differently. Its purpose
  ! is to stabilize the inversion by adding a small number to the diagonal.
  ! Thus we compute w0 as 0.01 of the minimum of the values on the diagonal
  ! to improve the numerical stability. This enables convergence to very
  ! high accuracies.
  !
  ! NTD: I changed the logic to be 0.01^2 times the smallest diagonal value.
  ! Else, for a small value of 1e-10, we were getting w0 to be 1e-24
  ! For 1e-6, we were getting w0=1e-16.
  ! Overall, this was having no practical effect apart for large numbers.
  w0 = M_HUGE
  do i = 1, size
    w0 = min(w0, abs(beta(i, i)))
  end do
  w0 = w0 * 1e-4_real64
  ! safeguard if w0 should be exactly zero or underflow
  if (w0 < 1e-150_real64) then
    w0 = 1e-4_real64
  end if
  do i = 1, size
    beta(i, i) = w0 + beta(i, i)
  end do

  POP_SUB(X(mixing_build_matrix))
end subroutine X(mixing_build_matrix)

! --------------------------------------------------------------

R_TYPE function X(mix_dotp)(this, xx, yy, reduce) result(dotp)
  type(mix_t),       intent(in) :: this
  R_TYPE,            intent(in) :: xx(:)
  R_TYPE,            intent(in) :: yy(:)
  logical, optional, intent(in) :: reduce

  R_TYPE, allocatable :: ff(:), precff(:)
  logical :: reduce_

  PUSH_SUB(X(mix_dotp))

  reduce_ = .true.
  if (present(reduce)) reduce_ = reduce

  if (this%precondition) then

    ASSERT(this%mixfield%d1 == this%der%mesh%np)

    SAFE_ALLOCATE(ff(1:this%der%mesh%np_part))
    SAFE_ALLOCATE(precff(1:this%der%mesh%np))

    ff(1:this%der%mesh%np) = yy(1:this%der%mesh%np)
    call X(derivatives_perform)(this%preconditioner, this%der, ff, precff)
    dotp = X(mf_dotp)(this%der%mesh, xx, precff, reduce = reduce_)

    SAFE_DEALLOCATE_A(precff)
    SAFE_DEALLOCATE_A(ff)

  else
    dotp = X(mf_dotp)(this%der%mesh, xx, yy, reduce = reduce_, np = this%mixfield%d1)
  end if

  POP_SUB(X(mix_dotp))

end function X(mix_dotp)

! --------------------------------------------------------------
subroutine X(mixfield_set_vin)(mixfield, vin)
  type(mixfield_t),   intent(inout) :: mixfield
  R_TYPE, contiguous, intent(in)    :: vin(:, :)

#ifdef R_TREAL
  integer :: ip
#endif

  PUSH_SUB(X(mixfield_set_vin))

#ifdef R_TREAL
  if (mixfield%mix_spin_density_matrix .or. mixfield%d2 == 1) then
#endif
    call lalg_copy(mixfield%d1, mixfield%d2, vin, mixfield%X(vin))
#ifdef R_TREAL
  else
    do ip = 1, mixfield%d1
      mixfield%X(vin)(ip, 1) = vin(ip, 1) + vin(ip, 2)
      mixfield%X(vin)(ip, 2) = vin(ip, 1) - vin(ip, 2)
    end do
    if (mixfield%d2 > 2) then
      do ip = 1, mixfield%d1
        mixfield%X(vin)(ip, 3) = - M_TWO * vin(ip, 3)
        mixfield%X(vin)(ip, 4) =   M_TWO * vin(ip, 4)
      end do
    end if
  end if
#endif

  POP_SUB(X(mixfield_set_vin))
end subroutine X(mixfield_set_vin)

! --------------------------------------------------------------
subroutine X(mixfield_set_vout)(mixfield, vout)
  type(mixfield_t),    intent(inout) :: mixfield
  R_TYPE, contiguous,  intent(in)    :: vout(:, :)

#ifdef R_TREAL
  integer :: ip
#endif

  PUSH_SUB(X(mixfield_set_vout))

#ifdef R_TREAL
  if (mixfield%mix_spin_density_matrix .or. mixfield%d2 == 1) then
#endif
    call lalg_copy(mixfield%d1, mixfield%d2, vout, mixfield%X(vout))
#ifdef R_TREAL
  else
    do ip = 1, mixfield%d1
      mixfield%X(vout)(ip, 1) = vout(ip, 1) + vout(ip, 2)
      mixfield%X(vout)(ip, 2) = vout(ip, 1) - vout(ip, 2)
    end do
    if (mixfield%d2 > 2) then
      do ip = 1, mixfield%d1
        mixfield%X(vout)(ip, 3) = - M_TWO * vout(ip, 3)
        mixfield%X(vout)(ip, 4) =   M_TWO * vout(ip, 4)
      end do
    end if
  end if
#endif

  POP_SUB(X(mixfield_set_vout))
end subroutine X(mixfield_set_vout)

! --------------------------------------------------------------
subroutine X(mixfield_get_vnew)(mixfield, vnew)
  type(mixfield_t),   intent(in)    :: mixfield
  R_TYPE, contiguous, intent(inout) :: vnew(:,:)

#ifdef R_TREAL
  integer :: ip
#endif

  PUSH_SUB(X(mixfield_get_vnew))

#ifdef R_TREAL
  if (mixfield%mix_spin_density_matrix .or. mixfield%d2 == 1) then
#endif
    call lalg_copy(mixfield%d1, mixfield%d2, mixfield%X(vnew), vnew)
#ifdef R_TREAL
  else
    do ip = 1, mixfield%d1
      vnew(ip, 1) = M_HALF * (mixfield%X(vnew)(ip, 1) + mixfield%X(vnew)(ip, 2))
      vnew(ip, 2) = M_HALF * (mixfield%X(vnew)(ip, 1) - mixfield%X(vnew)(ip, 2))
    end do
    if (mixfield%d2 > 2) then
      do ip = 1, mixfield%d1
        vnew(ip, 3) = -M_HALF * mixfield%X(vnew)(ip, 3)
        vnew(ip, 4) =  M_HALF * mixfield%X(vnew)(ip, 4)
      end do
    end if
  end if
#endif

  POP_SUB(X(mixfield_get_vnew))
end subroutine X(mixfield_get_vnew)


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
