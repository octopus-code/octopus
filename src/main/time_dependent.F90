!! Copyright (C) 2019-2020 M. Oliveira, H. Appel, N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module time_dependent_oct_m
  use debug_oct_m
  use electrons_oct_m
  use global_oct_m
  use messages_oct_m
  use multisystem_basic_oct_m
  use multisystem_debug_oct_m
  use namespace_oct_m
  use parser_oct_m
  use profiling_oct_m
  use restart_oct_m
  use system_oct_m
  use td_oct_m
  use walltimer_oct_m

  implicit none

  private
  public :: time_dependent_run

contains

  ! ---------------------------------------------------------
  subroutine time_dependent_run(electrons, from_scratch)
    class(electrons_t), intent(inout) :: electrons
    logical,            intent(inout) :: from_scratch

    PUSH_SUB(time_dependent_run)

    call td_init(electrons%td, electrons%namespace, electrons%space, electrons%gr, electrons%ions, electrons%st, electrons%ks, &
      electrons%hm, electrons%ext_partners, electrons%outp)
    call td_init_run(electrons%td, electrons%namespace, electrons%mc, electrons%gr, electrons%ions, electrons%st, electrons%ks, &
      electrons%hm, electrons%ext_partners, electrons%outp, electrons%space, from_scratch)
    call td_run(electrons%td, electrons%namespace, electrons%mc, electrons%gr, electrons%ions, electrons%st, electrons%ks, &
      electrons%hm, electrons%ext_partners, electrons%outp, electrons%space, from_scratch)
    call td_end_run(electrons%td, electrons%st, electrons%hm)
    call td_end(electrons%td)

    POP_SUB(time_dependent_run)
  end subroutine time_dependent_run

end module time_dependent_oct_m
