!! Copyright (C)  2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module minimizer_static_oct_m
  use algorithm_oct_m
  use debug_oct_m
  use global_oct_m
  use minimizer_algorithm_oct_m
  use interaction_partner_oct_m
  use interaction_oct_m
  use system_oct_m

  implicit none

  private
  public ::                       &
    minimizer_static_t

  !> Implements a static minimizer that keeps the state of the system constant.
  type, extends(minimizer_algorithm_t) :: minimizer_static_t
    private
  contains
    procedure :: finished => minimizer_static_finished
  end type minimizer_static_t

  interface minimizer_static_t
    procedure minimizer_static_constructor
  end interface minimizer_static_t

contains

  ! ---------------------------------------------------------
  function minimizer_static_constructor() result(this)
    type(minimizer_static_t), pointer    :: this

    PUSH_SUB(minimizer_static_constructor)

    allocate(this)

    this%start_operation = OP_SKIP
    this%final_operation = OP_SKIP

    call this%add_operation(OP_UPDATE_COUPLINGS)
    call this%add_operation(OP_UPDATE_INTERACTIONS)
    call this%add_operation(OP_ITERATION_DONE)
    call this%add_operation(OP_REWIND_ALGORITHM)

    this%algo_steps = 1

    POP_SUB(minimizer_static_constructor)
  end function minimizer_static_constructor


  ! ---------------------------------------------------------
  !> @brief indicate whether a static minimizer is finished
  !!
  !! The finish point is found when all the partner systems have finished their algorithm
  logical function minimizer_static_finished(this) result(finished)
    class(minimizer_static_t), intent(in) :: this

    class(interaction_t),              pointer :: interaction
    type(interaction_iterator_t) :: iter

    finished = minimizer_algorithm_finished(this)
    if (finished) return

    finished = .true.
    call iter%start(this%system%interactions)
    do while (iter%has_next())
      interaction => iter%get_next()

      select type (partner => interaction%partner)
      class is (system_t)

        finished = finished .and. partner%algo%finished()
      end select
    end do
  end function minimizer_static_finished


end module minimizer_static_oct_m

!!o, Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
