!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module target_low_oct_m
  use batch_oct_m
  use batch_ops_oct_m
  use debug_oct_m
  use density_oct_m
  use derivatives_oct_m
  use epot_oct_m
  use electron_space_oct_m
  use excited_states_oct_m
  use fft_oct_m
  use global_oct_m
  use grid_oct_m
  use hamiltonian_elec_oct_m
  use interaction_partner_oct_m
  use io_oct_m
  use io_function_oct_m
  use ion_dynamics_oct_m
  use ions_oct_m
  use, intrinsic :: iso_fortran_env
  use kpoints_oct_m
  use lalg_adv_oct_m
  use lalg_basic_oct_m
  use lasers_oct_m
  use loct_oct_m
  use math_oct_m
  use mesh_oct_m
  use mesh_function_oct_m
  use messages_oct_m
  use multicomm_oct_m
  use namespace_oct_m
  use opt_control_global_oct_m
  use opt_control_state_oct_m
  use output_oct_m
  use output_low_oct_m
  use parser_oct_m
  use profiling_oct_m
  use restart_oct_m
  use space_oct_m
  use species_oct_m
  use spectrum_oct_m
  use states_abst_oct_m
  use states_elec_oct_m
  use states_elec_calc_oct_m
  use states_elec_restart_oct_m
  use string_oct_m
  use td_calc_oct_m
  use td_oct_m
  use types_oct_m
  use unit_oct_m
  use unit_system_oct_m
  use varinfo_oct_m

  implicit none

  private
  public ::                  &
    target_t,                &
    target_mode,             &
    target_type,             &
    target_curr_functional,  &
    target_move_ions,        &
    is_spatial_curr_wgt

  type target_t
    integer :: type
    type(states_elec_t) :: st
    type(excited_states_t) :: est
    real(real64), allocatable :: rho(:)
    real(real64), allocatable :: td_fitness(:)
    character(len=200) :: td_local_target
    character(len=80) :: excluded_states_list
    character(len=4096) :: vel_input_string
    character(len=4096) :: classical_input_string
    character(len=1024), allocatable :: vel_der_array(:,:)
    character(len=1024), allocatable :: mom_der_array(:,:)
    character(len=1024), allocatable :: pos_der_array(:,:)
    real(real64), allocatable :: grad_local_pot(:,:,:)
    logical :: move_ions
    integer :: hhg_nks
    integer, allocatable :: hhg_k(:)
    real(real64),   allocatable :: hhg_alpha(:)
    real(real64),   allocatable :: hhg_a(:)
    real(real64)   :: hhg_w0
    real(real64)   :: dt
    integer :: curr_functional
    real(real64)   :: density_weight
    real(real64)   :: curr_weight
    integer :: strt_iter_curr_tg
    real(real64), allocatable :: spatial_curr_wgt(:)
    character(len=1000) :: plateau_string
    complex(real64), allocatable :: acc(:, :)
    complex(real64), allocatable :: vel(:, :)
    complex(real64), allocatable :: gvec(:, :)
    real(real64), allocatable :: alpha(:)
    complex(real64) :: spin_matrix(2, 2)
    type(fft_t) :: fft_handler
  end type target_t

  integer, public, parameter ::       &
    oct_tg_groundstate      = 1,      &
    oct_tg_excited          = 2,      &
    oct_tg_gstransformation = 3,      &
    oct_tg_userdefined      = 4,      &
    oct_tg_jdensity         = 5,      &
    oct_tg_local            = 6,      &
    oct_tg_td_local         = 7,      &
    oct_tg_exclude_state    = 8,      &
    oct_tg_hhg              = 9,      &
    oct_tg_velocity         = 10,     &
    oct_tg_hhgnew           = 12,     &
    oct_tg_classical        = 13,     &
    oct_tg_spin             = 14

  integer, public, parameter ::       &
    oct_targetmode_static = 0,        &
    oct_targetmode_td     = 1

  integer, public, parameter ::       &
    oct_no_curr              = 0,     &
    oct_curr_square          = 1,     &
    oct_max_curr_ring        = 2,     &
    oct_curr_square_td       = 3



contains

  ! ----------------------------------------------------------------------
  integer pure function target_mode(tg)
    type(target_t), intent(in) :: tg

    select case (tg%type)
    case (oct_tg_td_local, oct_tg_hhg, oct_tg_velocity, oct_tg_hhgnew)
      target_mode = oct_targetmode_td
    case default
      target_mode = oct_targetmode_static
    end select

    ! allow specific current functionals to be td
    ! Attention: yet combined with static density target,
    ! the total target is considered td.
    select case (tg%curr_functional)
    case (oct_curr_square_td)
      target_mode = oct_targetmode_td
    end select

  end function target_mode


  ! ----------------------------------------------------------------------
  integer pure function target_type(tg)
    type(target_t), intent(in) :: tg
    target_type = tg%type
  end function target_type
  ! ----------------------------------------------------------------------


  !-----------------------------------------------------------------------
  integer pure function target_curr_functional(tg)
    type(target_t), intent(in) :: tg
    target_curr_functional = tg%curr_functional
  end function target_curr_functional
  !-----------------------------------------------------------------------


  ! ----------------------------------------------------------------------
  logical pure function target_move_ions(tg)
    type(target_t), intent(in) :: tg
    target_move_ions = tg%move_ions
  end function target_move_ions
  ! ----------------------------------------------------------------------

  ! ----------------------------------------------------------------------
  logical pure function is_spatial_curr_wgt(tg)
    type(target_t), intent(in) :: tg

    is_spatial_curr_wgt = allocated(tg%spatial_curr_wgt)

  end function is_spatial_curr_wgt
  ! ----------------------------------------------------------------------


end module target_low_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
