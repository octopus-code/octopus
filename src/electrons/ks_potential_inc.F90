!! Copyright (C) 2024 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

!>@brief Multiply a mesh function by vHxc
subroutine X(ks_potential_mult_vhxc)(this, mf, ispin)
  class(ks_potential_t), intent(in)    :: this
  R_TYPE,                intent(inout) :: mf(:) !< On exit, vHxc has been added

  integer :: ispin, ip

  PUSH_SUB(X(ks_potential_mult_vhxc))

  ASSERT(not_in_openmp())
  ASSERT(size(mf, dim=1) >= this%np)

  !$omp parallel do simd schedule(static)
  do ip = 1, this%np
    mf(ip) = mf(ip) * this%vhxc(ip, ispin)
  end do

  POP_SUB(X(ks_potential_mult_vhxc))
end subroutine X(ks_potential_mult_vhxc)

!>@brief Wrapper to hamiltonian_elec_base_local_sub to hide the data of vtau
subroutine X(ks_potential_apply_vtau_psi)(this, mesh, d, ispin, psib, vpsib)
  class(ks_potential_t),   intent(in)    :: this
  class(mesh_t),           intent(in)    :: mesh
  type(states_elec_dim_t), intent(in)    :: d
  integer,                 intent(in)    :: ispin
  type(wfs_elec_t),        intent(inout) :: psib
  type(wfs_elec_t),        intent(inout) :: vpsib

  PUSH_SUB(X(ks_potential_apply_vtau_psi))

  call X(hamiltonian_elec_base_local_sub)(this%vtau, mesh, d, ispin, psib, vpsib, &
    potential_accel = this%vtau_accel)

  POP_SUB(X(ks_potential_apply_vtau_psi))
end subroutine X(ks_potential_apply_vtau_psi)

!>@brief Nonlocal contribution of vtau for the current
!!
!! A nonlocal contribution from the MGGA potential must be included
!! This must be done first, as this is like a position-dependent mass
!! as a renormalization of the gradient
subroutine X(ks_potential_current_mass_renormalization)(this, gpsi, space_dim, ndim, ispin)
  class(ks_potential_t),   intent(in)    :: this
  R_TYPE,                  intent(inout) :: gpsi(:,:,:) !< Gradient of psi (np, space%dim, ndim)
  integer,                 intent(in)    :: space_dim
  integer,                 intent(in)    :: ndim
  integer,                 intent(in)    :: ispin

  integer :: idim, ip, idir

  PUSH_SUB(X(ks_potential_current_mass_renormalization))

  ASSERT(not_in_openmp())

  if (this%needs_vtau) then
    !$omp parallel private(idir, ip)
    do idim = 1, ndim
      do idir = 1, space_dim
        !$omp do
        do ip = 1, this%np
          gpsi(ip, idir, idim) = (M_ONE + M_TWO*this%vtau(ip, ispin)) * gpsi(ip, idir, idim)
        end do
      end do
    end do
    !$omp end parallel
  end if

  POP_SUB(X(ks_potential_current_mass_renormalization))
end subroutine X(ks_potential_current_mass_renormalization)

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
